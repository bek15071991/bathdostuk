﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using BathDostukDAL.Context;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;

namespace BathDostukDAL.InitializeData
{
    internal class Init : DropCreateDatabaseIfModelChanges<BathDostukContext>
    {
        protected override void Seed(BathDostukContext context)
        {
            base.Seed(context);


            var cassier = new Employee
            {
                Login = "admin",
                Password = "def197051",
                Rule = Rule.Admin,
                Fio = "Накай уулу Бек",
                Address = "ул. Некрасвова № 101",
                Salary = 10000,
                NumberPhone = "+ (996) 700 914422"
            };

            context.Employees.AddOrUpdate(cassier);



            context.SaveChanges();


        }
    }

}
