﻿using System; 
using System.Data.Entity; 
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;
using BathDostukDAL.Config;
using BathDostukDAL.Connection;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People; 
using BathDostukDAL.Migrations;

namespace BathDostukDAL.Context
{
    public class BathDostukContext : DbContext
    {
        public BathDostukContext()
            : base(GetConnectionString())
        {
           //  Database.SetInitializer(new Init());  //Создает новую базу данных
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BathDostukContext,Configuration>());
        }

        private static string GetConnectionString()
        {
            var manager = new ConnectionManager();  
            return manager.GetConnectString();
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<SaleProduct> SaleProducts { get; set; }
        public DbSet<BuyProduct> BuyProducts { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<ReportOnDay> ReportOnDays { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();


            modelBuilder.Properties().Where(pr => pr.Name == "Id").Configure(p => p.IsKey());
            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("nvarchar"));
            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(150));
             

            modelBuilder.Entity<SaleProduct>().Map(o => o.ToTable("SaleProducts"));
            modelBuilder.Entity<Expense>().Map(o => o.ToTable("Expenses"));
            modelBuilder.Entity<BuyProduct>().Map(o => o.ToTable("BuyProducts"));
            modelBuilder.Entity<Ticket>().Map(o => o.ToTable("Tickets"));

            modelBuilder.Configurations.Add(new EmployeeConfig());
            modelBuilder.Configurations.Add(new OrderConfig());
            modelBuilder.Configurations.Add(new ProductConfig());
        }


        public override int SaveChanges()
        {

            foreach (var entry in ChangeTracker.Entries().Where(p => p.Entity.GetType().GetProperty("Date") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("Date").CurrentValue = DateTime.Now;

                }

                if (entry.State == EntityState.Modified)
                    entry.Property("Date").IsModified = false;
            } 
            var result = base.SaveChanges(); 
            return result;
        }
    }
}
