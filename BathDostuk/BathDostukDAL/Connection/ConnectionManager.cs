﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;

namespace BathDostukDAL.Connection
{
    public class ConnectionManager :IDisposable
    {
        private string _dataBase;
        private string _dataSource;
        private bool _integratedSecurity;
        private bool _multipleActiveResultSets;
        private string _app;
        private readonly string _fileName;
        private readonly XDocument _xml;

        public ConnectionManager()
        {
            _fileName = @"Connection\ConnectionString.xml";
            _xml = XDocument.Load(_fileName);
            if (_xml.Root == null) return;

            foreach (var element in _xml.Root.Elements())
            {
                if (element.Name == "DataBase")
                {
                    _dataBase = element.Value.Trim();
                }
                if (element.Name == "DataSource")
                {
                    _dataSource = element.Value.Trim();
                }
                if (element.Name == "IntegratedSecurity")
                {
                    _integratedSecurity = Convert.ToBoolean(element.Value);
                }
                if (element.Name == "MultipleActiveResultSets")
                {
                    _multipleActiveResultSets = Convert.ToBoolean(element.Value);
                }
                if (element.Name == "App")
                {
                    _app = element.Value.Trim();
                }
            }

             
        }


        public string GetConnectString()
        {
            var sqlbuilder = new SqlConnectionStringBuilder
            {
                DataSource = _dataSource,
                InitialCatalog = _dataBase,
                IntegratedSecurity = _integratedSecurity,
                ApplicationName = _app,
                MultipleActiveResultSets = _multipleActiveResultSets
            };

            return sqlbuilder.ToString(); 
        }

        public void SetDataBase(string dataBase)
        {
            foreach (var elem in _xml.Root.Elements().Where(elem => elem.Name == "DataBase"))
            {
                elem.Value = dataBase;
            }
            _dataBase = dataBase;
        }

        public void SetDataSource(string dataSource)
        {
            foreach (var elem in _xml.Root.Elements().Where(elem => elem.Name == "DataSource"))
            {
                elem.Value = dataSource;
            }
            _dataSource = dataSource;
        }

        public string DataBase
        {
            get { return _dataBase; }
            set { _dataBase = value; }
        }

        public string DataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }

        public bool IntegratedSecurity
        {
            get { return _integratedSecurity; }
            set { _integratedSecurity = value; }
        }

        public bool MultipleActiveResultSets
        {
            get { return _multipleActiveResultSets; }
            set { _multipleActiveResultSets = value; }
        }

        public string App
        {
            get { return _app; }
            set { _app = value; }
        }


        public void SaveChanges()
        {
         _xml.Save(_fileName);
        }

        public void Dispose()
        {
            SaveChanges();
        }
    }
}
