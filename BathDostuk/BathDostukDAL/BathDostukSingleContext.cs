﻿using BathDostukDAL.Context;

namespace BathDostukDAL
{
    public class BathDostukSingleContext
    {
        private static BathDostukContext _Context;

        public static BathDostukContext SingleContext
        {
            get { return _Context ?? (_Context = new BathDostukContext()); }
        }
    }
}
