﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public class SaleProductRepository : RepositoryBase<SaleProduct>, ISaleProductRepository
    {
        public IEnumerable<SaleProduct> GetProductsTop100()
        {
            return GetAll().Take(100).OrderByDescending(bp => bp.Date);
        }

        public IEnumerable<SaleProduct> GetOnPeriod(DateTime @from, DateTime to)
        {
           to= to.AddDays(1);
            return GetAll().Where(bp => bp.Date > @from & bp.Date <= to)
                .Include("Employee") 
                .Include("BuyProduct").ToList();
        }

        public IEnumerable<SaleProduct> GetOnPeriodWithUser(DateTime @from, DateTime to, Employee user)
        {
          to=  to.AddDays(1);

            var rr = GetAll().Where(bp => bp.Date > @from & bp.Date <= to & bp.Employee.Id == user.Id)
                .Include("Employee")
                .Include("BuyProduct").ToList();

            return GetAll().Where(bp => bp.Date > @from & bp.Date <= to & bp.Employee.Id == user.Id)
                .Include("Employee") 
                .Include("BuyProduct").ToList();
        }

        public IEnumerable<SaleProduct> GetIsNotReport()
        {
            return GetAll().Where(or => !or.IsReport)
                    .Where(or => or.Employee.Id == User.CurrentUser.Id).Include("BuyProduct").ToList();
        }

        public IEnumerable<SaleProduct> GetProductsWithUserTop100(Employee employee)
        {
            return GetAll().Take(100).Where(bp=>bp.Employee.Id ==employee.Id).OrderByDescending(bp => bp.Date);
        }

        public IEnumerable<SaleProduct> GetAllWithUser(Employee employee)
        { 
            return GetAll().Include("BuyProduct").Where(bp => bp.Employee.Id == employee.Id);
        }
    }
}
