﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public class ExpenseRepository : RepositoryBase<Expense>, IExpenseRepository
    {
        public IQueryable<Expense> GetAllExpense()
        {
            return GetAll().Where(e => e.Employee == null);
        }

        public IQueryable<Expense> GetAllSalary()
        {
            return GetAll().Where(e => e.Employee != null);
        }

        public List<Expense> GetOnPeriodSalary(DateTime dateTime1, DateTime dateTime2)
        {
            dateTime2 = dateTime2.AddDays(1);
            return GetAll().Where(e => e.Date > dateTime1.Date & e.Date < dateTime2.Date).Where(e => e.Employee != null).OrderByDescending(s => s.Date).Include("Employee").ToList();
        }

     /// <summary>
     /// Получить все зарплаты на период по пользователю
     /// </summary>
     /// <param name="dateTime1"></param>
     /// <param name="dateTime2"></param>
     /// <param name="employee"></param>
     /// <returns></returns>
        public List<Expense> GetOnPeriodWithUserSalary(DateTime dateTime1, DateTime dateTime2, Employee employee)
        {
            dateTime2 = dateTime2.AddDays(1);
            return GetAll().Where(e => e.Date > dateTime1.Date & e.Date < dateTime2.Date).
                Where(e => e.Employee.Id == employee.Id).Where(e => e.Employee != null).OrderByDescending(s => s.Date).Include("Employee").ToList();
        }

        public List<Expense> GetOnPeriodExpense(DateTime dateTime1, DateTime dateTime2)
        {
            dateTime2 = dateTime2.AddDays(1);
            return GetAll().Where(e => e.Date > dateTime1.Date & e.Date < dateTime2.Date).Where(e => e.Employee == null).OrderByDescending(s => s.Date).ToList();
        }

        public IEnumerable<Expense> GetIsNotReportExpense()
        {
            return GetAll().Where(s => s.Employee == null)
                .Where(s => s.IsReport == false).OrderByDescending(s => s.Date);
        }

        public IEnumerable<Expense> GetIsNotReportSalaries()
        {
            return GetAll().Where(s => s.Employee != null)
                  .Where(s => s.IsReport == false).OrderByDescending(s => s.Date);
        }

        public List<Expense> GetOnPeriodWithUserExpense(DateTime dateTime1, DateTime dateTime2, Employee employee)
        {
            dateTime2 = dateTime2.AddDays(1);
            return GetAll().Where(e => e.Date > dateTime1.Date & e.Date < dateTime2.Date).Where(e => e.Employee.Id == employee.Id).OrderByDescending(s => s.Date).Where(e => e.Employee == null).Include("Employee").ToList();

        }
    }
}
