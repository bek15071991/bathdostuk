﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BathDostukDAL.Context;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public abstract class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    { 
        protected BathDostukContext Db = BathDostukSingleContext.SingleContext;
  
        public void Add(TEntity obj)
        { 
            Db.Set<TEntity>().Add(obj);
            Db.SaveChanges();
        }

        public TEntity GetById(int id)
        {
            return Db.Set<TEntity>().Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return Db.Set<TEntity>();
        }

        public void Update(TEntity obj)
        { 
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public void Remove(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.Collect();
        }
    }
}
