﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public class BuyProductRepository : RepositoryBase<BuyProduct>, IBuyProductRepository
    {
        public IEnumerable<BuyProduct> GetProductsInStorageAsNoTracking()
        {
            var sql = @" select * from 
						 (
						 select 
                         bp.Id,
                         bp.Price,
                         bp.Cost,
                         bp.IsReport,
                         bp.[Count]-CAST(ISNULL((select SUM( [Count]) from dbo.SaleProducts where  BuyProduct_Id= bp.id),0) as float)   as [Count],
                         bp.Date,
                         bp.Product_Id,
                          bp.SimpleEmployee_Id 
                           from dbo.BuyProducts bp ) as nbp where nbp.Count >0";
             
            return Db.BuyProducts.SqlQuery(sql).AsNoTracking().ToList();
        }

        public IEnumerable<BuyProduct> GetProductsTop100()
        {
            return GetAll().Take(100).OrderByDescending(bp => bp.Date);
        }

        public IEnumerable<BuyProduct> GetOnPeriod(DateTime @from, DateTime to)
        {
            to = to.AddDays(1);
            return GetAll().Where(bp => bp.Date > @from & bp.Date <= to).Include("Employee").Include("Product").ToList();
        }

        public IEnumerable<BuyProduct> GetOnPeriodWithUser(DateTime @from, DateTime to, Employee user)
        {
            to = to.AddDays(1);
            return GetAll().Where(bp => bp.Date > @from & bp.Date <= to).Where(bp => bp.Employee.Id == user.Id).Include("Employee").Include("Product").ToList();
        }

        public bool CanDeleteProduct(Product selectedProduct)
        {
            var rr = GetAll().Any(b => b.Product.Id == selectedProduct.Id);
            return !rr;
        }
    }
}
