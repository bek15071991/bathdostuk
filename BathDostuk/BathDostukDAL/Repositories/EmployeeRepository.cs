﻿using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public class EmployeeRepository : RepositoryBase<Employee>, IEmployeeRepository
    {
        public Employee GetByLoginPassword(string login, string password)
        {
            return
                Db.Employees.FirstOrDefault(
                    emp => emp.Login.Equals(login) && emp.Password.Equals(password) && !emp.Delete);

        }

        public IEnumerable<Employee> GetAllNotDeleted()
        {
            return GetAll().Where(emp => emp.Delete == false);
        }

        public bool LoginEqual(string p)
        {
            return GetAll().FirstOrDefault(us => us.Login.Equals(p)) != null;
        }

        public IEnumerable<Employee> GetWithRole(Rule cassier)
        {
            return GetAll().Where(emp => emp.Rule == cassier);
        }

        public IEnumerable<Employee> GetEmployesNotDeleted()
        {
            return GetAll().Where(emp => emp.Rule != Rule.Admin & emp.Rule != Rule.Director).Where(emp => emp.Delete == false);
        }
    }
}
