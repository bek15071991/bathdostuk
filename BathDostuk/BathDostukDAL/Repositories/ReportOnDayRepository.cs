﻿using System;
using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public class ReportOnDayRepository : RepositoryBase<ReportOnDay>, IRepositoryReportOnDay
    { 

        public int GetWorkedDayOnPeriodWithUser(DateTime dateTime1, DateTime dateTime2, Employee employee)
        {
            dateTime2 = dateTime2.AddDays(1);
             
            return GetAll().Count() != 0 ? GetAll().Count(s =>  s.Date > dateTime1.Date & s.Date < dateTime2.Date  & s.User.Id == employee.Id) : 0;
        }

        public IEnumerable<ReportOnDay> GetOnPeriodWithUser(DateTime dateTime1, DateTime dateTime2, Employee employee)
        {
            dateTime2 = dateTime2.AddDays(1);
            return GetAll().Where(s => s.Date > dateTime1.Date & s.Date < dateTime2.Date & s.User.Id == employee.Id);
        }
    }
}
