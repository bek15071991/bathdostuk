﻿
using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public class TicketRepository : RepositoryBase<Ticket>, ITicketRepository
    {
        public IEnumerable<Ticket> GetNotDeleted()
        {
            return GetAll().Where(t => !t.Delete);
        }
    }
}
