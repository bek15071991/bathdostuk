﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;

namespace BathDostukDAL.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public IEnumerable<Order> GetIsNotReportOrders()
        {
            return GetAll().Where(or => !or.IsReport)
                    .Where(or => or.Cassier.Id == User.CurrentUser.Id).Include("Ticket").ToList();
        }

        public IEnumerable<Order> GetOrdersOnPeriodWithUser(DateTime @from, DateTime to, Employee user)
        {
            to = to.AddDays(1);
            return GetAll().Where(o => o.Date > @from.Date & o.Date < to.Date & o.Cassier.Id == user.Id).Include("Ticket");
        }

        
    }
}
