﻿using System.Data.Entity.ModelConfiguration;
using BathDostukDomain.Entities;

namespace BathDostukDAL.Config
{
    public class ProductConfig : EntityTypeConfiguration<Product>
    {
        public ProductConfig()
        {
            ToTable("Products");
            Property(c => c.BarCode).HasMaxLength(50);
            Property(c => c.ED).HasMaxLength(20).IsRequired();
            Property(c => c.Name).IsRequired();
        }
    }
}
