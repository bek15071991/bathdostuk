﻿using System.Data.Entity.ModelConfiguration;
using BathDostukDomain.Entities;

namespace BathDostukDAL.Config
{
    public class OrderConfig : EntityTypeConfiguration<Order>
    {
        public OrderConfig()
        {
            ToTable("Orders");
            Property(c => c.IsReport).IsRequired();
            Ignore(c => c.DateTostring);
        }

    }
}
