﻿using System.Data.Entity.ModelConfiguration;
using BathDostukDomain.Entities.People;

namespace BathDostukDAL.Config
{
    public class EmployeeConfig : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfig()
        {

            ToTable("Employees");

            Property(c => c.Fio).IsRequired().HasMaxLength(50);

            Property(c => c.Login).IsRequired().HasMaxLength(20);

            Property(c => c.Password).IsRequired().HasMaxLength(20);

            Property(c => c.Salary).IsRequired();

            Property(c => c.Rule).IsRequired();

            Property(c => c.Address).HasMaxLength(50).IsRequired();

            Property(c => c.NumberPhone).HasMaxLength(20).IsRequired();

            //Property(x => x.CountDays).IsOptional();

        }
    }
}
