namespace BathDostukDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            Sql(@"Alter table  dbo.ReportOnDay add    TotalSalary float   null  
                    Alter table dbo.Employees add CountDays int null   
                    GO
                 update dbo.ReportOnDay set TotalSalary =0 where TotalSalary is null 
                 alter table dbo.reportOnDay alter column totalsalary float not null 
                  GO
                  update dbo.ReportOnDay set TotalExpense =0 where TotalExpense is null 
                 alter table dbo.reportOnDay alter column TotalExpense float not null
                 GO
                update dbo.Expenses set IsReport =0 where IsReport is null 
                 alter table dbo.Expenses alter column IsReport bit not null
                 GO
                update dbo.Employees set CountDays =0 where CountDays is null 
                 alter table dbo.Employees alter column CountDays int not null");

  

        }

        public override void Down()
        { 

        }
    }
}
