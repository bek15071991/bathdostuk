using System.Data.Entity.Migrations;
using BathDostukDAL.Context;

namespace BathDostukDAL.Migrations
{
    internal class Configuration : DbMigrationsConfiguration<BathDostukContext>
    {
        public Configuration()
        {
             AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BathDostukContext context)
        {
             

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //


        }
    }
}
