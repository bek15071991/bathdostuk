﻿using System;
using MahApps.Metro.Controls.Dialogs;

namespace BathDostukWPF.Helper
{
    public class EventArguments : EventArgs
    {
        private readonly string _title;
        private readonly string _text;

        public EventArguments(string title, string text)
        {
            _title = title;
            _text = text;
        }

        public string Title { get { return _title; } }
        public string Text { get { return _text; } }

        public MetroDialogSettings Settings
        {
            get;
            set;
        }
    }
}
