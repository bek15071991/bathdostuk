﻿namespace BathDostukWPF.Helper
{
    public class StateBath
    {
        public MessageState MessageState { get; set; }
        public Views Views { get; set; }
        public object Value { get; set; }
    }

    public enum MessageState
    {
        Show,
        ShowDialog,
        Hide,
        Close
    }

    public enum Views
    {
        SaleOnCreditWindow,
        ClientSettingsWindow,
        StoreWindow,
        ProductSettings,
        StorageWindow
    }
}
