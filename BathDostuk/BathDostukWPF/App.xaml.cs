﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using BathDostukDomain.Entities;
using BathDostukWPF.Helper;
using BathDostukWPF.View;
using BathDostukWPF.ViewModel;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;

namespace BathDostukWPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static App()
        {
            DispatcherHelper.Initialize();
        }


        protected override void OnStartup(StartupEventArgs e)
        {
            InitMessenger();
            var dd = new ViewModelLocator();
            var wind = new LoginWindow { DataContext = dd.Login };
            wind.Show();

        }

        private void InitMessenger()
        {
            Messenger.Default.Register<StateBath>(this, new Action<StateBath>(bath =>
            {
                var view = new Window();
                 
                switch (bath.Views)
                {
                    case Views.SaleOnCreditWindow:
                    {
                        view = new SaleOnCreditWindow();
                        var context = new ViewModelLocator().SaleOnCredit;
                        context.Orders = new ObservableCollection<Order>((List<Order>)bath.Value);
                        view.DataContext = context;
                        break;
                    }
                    case Views.ClientSettingsWindow:
                    {
                        view = new ClientSettingsWindow();  
                        break;
                    }

                    case Views.StoreWindow:
                    {
                        view = new StoreWindow();
                        break;
                    }

                    case Views.StorageWindow:
                    {
                        view = new StorageWindow();
                        break;
                    }
                        case Views.ProductSettings:
                    {
                        view = new ProductSettingsWindow();
                        break;
                    }
                }

                if (bath.MessageState == MessageState.Show)
                {
                    view.Show();
                }
                if (bath.MessageState == MessageState.ShowDialog)
                {
                    view.ShowDialog();
                    view.Close();
                    view = null;
                    GC.Collect();
                }
               
                if (bath.MessageState == MessageState.Hide)
                {
                    view.Hide();
                }
            }));
        }


    }
}
