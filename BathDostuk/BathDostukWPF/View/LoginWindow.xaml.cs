﻿using System.Windows;
using BathDostukWPF.ViewModel;
using MahApps.Metro.Controls;

namespace BathDostukWPF.View
{
    /// <summary>
    /// Логика взаимодействия для LoginViewModel.xaml
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        public LoginWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
        
            
        }

        private void ShowUserDetails(object sender, RoutedEventArgs e)
        {
            var fl = Flyouts.Items[0] as Flyout;
            if (fl == null) return;
            fl.IsOpen = true;
        }

         
    }
}
