﻿using System;
using BathDostukDomain.Entities;
using MahApps.Metro.Controls;

namespace BathDostukWPF.View
{
    /// <summary>
    /// Логика взаимодействия для ProductSettingsWindow.xaml
    /// </summary>
    public partial class ProductSettingsWindow : MetroWindow
    {
        public ProductSettingsWindow()
        {
            InitializeComponent();

            _listProducts.SelectionChanged += (sender, args) =>
            {
                var obj = _listProducts.SelectedItem as Product;
                if (obj != null)
                {  
                    BarCodeTextBox.Value = Convert.ToDouble(obj.BarCode);
                    NameTextBox.Text = obj.Name;
                    EdIsTextBox.Text = obj.ED; 
                }

              

            };
        }
    }
}
