﻿using System.ComponentModel;
using System.Windows;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace BathDostukWPF.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += MetroWindow_Closing;
        }

        private void ShowUserDetails(object sender, RoutedEventArgs e)
        {
            FlyoutUser.IsOpen = !FlyoutUser.IsOpen; 
        }

        private void MetroWindow_Closing(object sender, CancelEventArgs e)
        {
            var mySettings = new MetroDialogSettings()
            {
                AffirmativeButtonText = "Quit",
                NegativeButtonText = "Cancel",
                AnimateShow = true,
                AnimateHide = false
            };

            var result = this.ShowMessageAsync("Quit application?",
                "Sure you want to quit application?",
                MessageDialogStyle.AffirmativeAndNegative, mySettings);

            //result.ContinueWith(task =>
            //{
            //    if (result.Result == MessageDialogResult.Affirmative) ;

            //});

                   Application.Current.Shutdown();
        }


    }
}