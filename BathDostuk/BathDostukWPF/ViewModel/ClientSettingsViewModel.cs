﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using BathDostukDomain.Entities.People;
using BathDostukWPF.Model.Interface;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace BathDostukWPF.ViewModel
{
    public class ClientSettingsViewModel : ViewModelBase
    {
        private readonly IAppClientService _appClientService;

        public ClientSettingsViewModel(IAppClientService appClientService)
        {
            _appClientService = appClientService;
            _clients = _appClientService.GetAllClients();
        }

        private string _fio;
        public string Fio
        {
            get { return _fio; }
            set
            {
                _fio = value;
                RaisePropertyChanged(() => Fio);
            }
        }

        private string _address;
        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                RaisePropertyChanged(() => Address);
            }
        }

        private string _numberPhone;
        public string NumberPhone
        {
            get { return _numberPhone; }
            set
            {
                _numberPhone = value;
                RaisePropertyChanged(() => NumberPhone);
            }
        }

        private bool _isDeleted;
        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                _isDeleted = value;
                RaisePropertyChanged(() => IsDeleted);
            }
        }

        private Client _selectedClient;

        public Client SelectedClient
        {
            get { return _selectedClient; }
            set
            {
                if (value == null) return;
                Fio = value.Fio;
                Address = value.Address;
                NumberPhone = value.NumberPhone;
                IsDeleted = value.IsDelete;
                _selectedClient = value;
            }
        }

        private ObservableCollection<Client> _clients;

        public ObservableCollection<Client> Clients
        {
            get { return _clients; }
            set
            {
                _clients = value;
                RaisePropertyChanged(() => Clients);
            }
        }


        #region Add client
        private ICommand _addClient;

        public ICommand AddClient
        {
            get
            {
                return _addClient ?? (new RelayCommand(() =>
                {
                    if (string.IsNullOrEmpty(Fio) || string.IsNullOrEmpty(Address)
                        || MessageBox.Show(String.Format("Сохранить {0} ?", Fio), "Внимание...") == MessageBoxResult.No) return;
                    _appClientService.AddNewClient(new Client
                    {
                        NumberPhone = NumberPhone,
                        Fio = Fio,
                        Address = Address,
                        IsDelete = IsDeleted
                    });
                    SetDefaultValues();

                }));
            }
        }

        private void SetDefaultValues()
        {
            IsDeleted = false;
            Address =
                Fio =
        NumberPhone = string.Empty;
            Clients = _appClientService.GetAllClients();
        }

        #endregion

        #region Update client
        private ICommand _updateClient;

        public ICommand UpdateClient
        {
            get
            {
                return _updateClient ?? (new RelayCommand(() =>
                {
                    if (SelectedClient == null) return;
                    SelectedClient.Address = Address;
                    SelectedClient.Fio = Fio;
                    SelectedClient.NumberPhone = NumberPhone;
                    SelectedClient.IsDelete = IsDeleted;
                    _appClientService.UpdateClient(SelectedClient);
                    SelectedClient = null;
                    SetDefaultValues();
                    MessageBox.Show("Успешно сохранено!", "Внимание...");
                }));
            }
        }


        #endregion

        #region Delete client
        private ICommand _deleteClient;

        public ICommand DeleteClient
        {
            get
            {
                return _deleteClient ?? (new RelayCommand(() =>
                {
                    if (SelectedClient == null || MessageBox.Show(String.Format("Удалить {0} ?", SelectedClient.Fio), "Внимание...") == MessageBoxResult.No) return;
                    _appClientService.RemoveClient(SelectedClient);
                    SelectedClient = null;
                    SetDefaultValues();

                    MessageBox.Show("Успешно удалено!", "Внимание...");
                }));
            }
        }


        #endregion

    }
}
