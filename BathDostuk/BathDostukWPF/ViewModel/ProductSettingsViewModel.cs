﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using BathDostukDomain.Entities;
using BathDostukWPF.Model.Interface;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace BathDostukWPF.ViewModel
{
    public class ProductSettingsViewModel : ViewModelBase
    {
        private readonly IAppProductService _appProductService;

        public ProductSettingsViewModel(IAppProductService service)
        {
            _appProductService = service;
            _listProducts = _appProductService.GetProducts();
        }

        private string _barCode;
        public string BarCode
        {
            get { return _barCode; }
            set
            {
                // Устанавливаем новое значение
                _barCode = value;
                // Сообщаем всем, кто подписан на событие PropertyChanged, что поле изменилось Name
                RaisePropertyChanged("BarCode");
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                // Устанавливаем новое значение
                _name = value;
                // Сообщаем всем, кто подписан на событие PropertyChanged, что поле изменилось Name
                RaisePropertyChanged("Name");
            }
        }

        private string _edIs;
        public string EdIs
        {
            get { return _edIs; }
            set
            {
                // Устанавливаем новое значение
                _edIs = value;
                // Сообщаем всем, кто подписан на событие PropertyChanged, что поле изменилось Name
                RaisePropertyChanged("EdIs");
            }
        }


        public Product SelectedProduct { get; set; }
        private ObservableCollection<Product> _listProducts;

        public ObservableCollection<Product> ListProducts
        {
            get { return _listProducts; }
            private set
            {
                _listProducts = value;
                RaisePropertyChanged(() => ListProducts);
            }
        }




        #region Изменить
        private ICommand _update;

        public ICommand Update
        {
            get
            {
                return _update ?? new RelayCommand(() =>
                {

                    if (SelectedProduct == null ||
                        MessageBox.Show("Изменить данныне ? ", "Внимание...", MessageBoxButton.YesNo) == MessageBoxResult.No) return;

                    SelectedProduct.BarCode = string.IsNullOrEmpty(BarCode) ? SelectedProduct.BarCode : BarCode;
                    SelectedProduct.ED = string.IsNullOrEmpty(EdIs) ? SelectedProduct.ED : EdIs;
                    SelectedProduct.Name = string.IsNullOrEmpty(Name) ? SelectedProduct.Name : Name;

                    _appProductService.UpdateProduct(SelectedProduct);

                    MessageBox.Show("Успешно изменен");
                    SetDefaultValues();
                    ListProducts = _appProductService.GetProducts();
                    SelectedProduct = null;
                });
            }
        }
        #endregion

        #region Добавить
        private ICommand _add;

        public ICommand Add
        {
            get
            {
                return _update ?? new RelayCommand(() =>
                {
                    if (string.IsNullOrEmpty(BarCode) ||
                        string.IsNullOrEmpty(Name) ||
                        string.IsNullOrEmpty(EdIs) ||
                        MessageBox.Show("Добавить данныне ? ", "Внимание...", MessageBoxButton.YesNo) == MessageBoxResult.No) return;

                    _appProductService.AddNewProduct(new Product
                    {
                        BarCode = BarCode,
                        Name = Name,
                        ED = EdIs
                    });
                    SetDefaultValues();
                    MessageBox.Show("Успешно добавлен");
                    ListProducts = _appProductService.GetProducts();
                });
            }
        }

        private void SetDefaultValues()
        {
            BarCode = "0";
            EdIs =
            Name = string.Empty;
        }

        #endregion

        #region Удалить
        private ICommand _remove;

        public ICommand Remove
        {
            get
            {
                return _update ?? new RelayCommand(() =>
                {
                    if (SelectedProduct == null ||
                        MessageBox.Show("Удалить  ? ", "Внимание...", MessageBoxButton.YesNo) == MessageBoxResult.No) return;

                    _appProductService.RemoveProduct(SelectedProduct);

                    MessageBox.Show("Успешно удален");
                    ListProducts = _appProductService.GetProducts();
                    SetDefaultValues();
                });
            }
        }
        #endregion

    }
}
