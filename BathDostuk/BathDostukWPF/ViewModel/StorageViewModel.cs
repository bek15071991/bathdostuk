﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using BathDostukDomain.Entities;
using BathDostukWPF.Helper;
 
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace BathDostukWPF.ViewModel
{
    public class StorageViewModel : ViewModelBase
    { 
        public StorageViewModel( )
        { 
            BuyProducts= new ObservableCollection<BuyProduct>();
        }

        private ObservableCollection<BuyProduct> _buyProducts;

        public ObservableCollection<BuyProduct> BuyProducts
        {
            get { return _buyProducts; }
            set
            {
                _buyProducts = value;
                RaisePropertyChanged(() => BuyProducts);
            }
        }

        public ObservableCollection<BuyProduct> BuyProductsInStorage { get; set; }

        public ObservableCollection<Product> Products { get; set; }

        private Product _selectedProduct;
        public Product SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                RaisePropertyChanged(() => SelectedProduct);
            }
        }

        private BuyProduct _selectedBuyProduct;
        public BuyProduct SelectedBuyProduct
        {
            get { return _selectedBuyProduct; }
            set
            {
                _selectedBuyProduct = value;
                RaisePropertyChanged(() => SelectedBuyProduct);
            }
        }

        private float _cost;
        public float Cost
        {
            get { return _cost; }
            set
            {
                _cost = value;
                RaisePropertyChanged(() => Cost);
            }
        }

        private float _count;
        public float Count
        {
            get { return _count; }
            set
            {
                _count = value;
                RaisePropertyChanged(() => Count);
            }
        }

        private float _payCost;
        public float PayCost
        {
            get { return _payCost; }
            set
            {
                _payCost = value;
                RaisePropertyChanged(() => PayCost);
            }
        }




        #region Delete Byuproduct
        private ICommand _deleteBuyProduct;
        public ICommand DeleteBuyProduct
        {
            get
            {
                return _deleteBuyProduct ?? (new RelayCommand(() =>
                {
                    if (BuyProducts.Count == 0 || SelectedBuyProduct == null) return;
                    BuyProducts.Remove(SelectedBuyProduct);
                    SelectedBuyProduct = null;
                }));
            }
        }

        #endregion

        #region Clean BuyProducts
        private ICommand _cleanBuyProducts;
        public ICommand CleanBuyProduct
        {
            get
            {
                return _cleanBuyProducts ?? (new RelayCommand(() =>
                {
                    if (BuyProducts.Count == 0) return;
                    BuyProducts.Clear();
                }));
            }
        }

        #endregion


        #region Сохранить продукты в базу
        private ICommand _saveBuyProducts;
        public ICommand SaveBuyProduct
        {
            get
            {
                return _saveBuyProducts ?? (new RelayCommand(() =>
                {
                    var total = BuyProducts.Sum(s => s.Total);
                    if (BuyProducts.Count == 0 ||
                        MessageBox.Show("Сохранить данные?" + Environment.NewLine + "Общая сумма покупки " + total + " сом.", "Внимание...", MessageBoxButton.YesNo) == MessageBoxResult.No) return;
                    _appProductService.SaveBuyProducts(BuyProducts.ToList());

                    MessageBox.Show("Данные успешно сохранены!", "Внимание...");
                    BuyProducts.Clear();
                    BuyProductsInStorage = _appProductService.GetBuyProducts();
                }));
            }
        }



        #endregion


        #region Добавить новую номенклатуру-продукт
        private ICommand _addProduct;
        public ICommand AddProduct
        {
            get
            {
                return _addListProduct ?? (new RelayCommand(() =>
                {
                    Messenger.Default.Send<StateBath>(new StateBath
                    {
                        MessageState = MessageState.Show,
                        Views = Views.ProductSettings
                    });
                }));
            }
        }

        #endregion


        #region Добавить продукт
        private ICommand _addListProduct;
        public ICommand AddListProduct
        {
            get
            {
                return _addListProduct ?? (new RelayCommand(() =>
                {
                    if (SelectedProduct == null && !(Cost > 0) && !(Count > 0) && !(PayCost > 0)) return;

                    var buyProduct = new BuyProduct
                    {
                        Product = SelectedProduct,
                        Cost = Cost,
                        Count = Count,
                        Price = PayCost,
                        Date = DateTime.Now
                    };

                    BuyProducts.Add(buyProduct);
                    SetDafaultValue();
                }));
            }
        }

        #endregion

        private void SetDafaultValue()
        {
            SelectedBuyProduct = null;
            SelectedProduct = null;
            Cost =
                PayCost =
                    Count = 0;
        }
    }
}
