﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="clr-namespace:BathDostukWPF.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using BathDostukDAL.Repositories;
using BathDostukDomain.Interfaces.Repositories;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace BathDostukWPF.ViewModel
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic)
            {
                 
            }
            else
            {
                #region Repostiries
                SimpleIoc.Default.Register<IEmployeeRepository, EmployeeRepository>();
                SimpleIoc.Default.Register<IOrderRepository, OrderRepository>();
                SimpleIoc.Default.Register<ITicketRepository, TicketRepository>();
                SimpleIoc.Default.Register<IProductRepository, ProductRepository>();
                SimpleIoc.Default.Register<IBuyProductRepository, BuyProductRepository>();
                SimpleIoc.Default.Register<ISaleProductRepository, SaleProductRepository>();
                SimpleIoc.Default.Register<IClientRepository, ClientRepositroy>();
                SimpleIoc.Default.Register<IDebtRepository, DebtRepository>();
                SimpleIoc.Default.Register<IRepaymentRepository, RepaymentRepository>();
                #endregion

                #region Services
                SimpleIoc.Default.Register<IEmployeeService, EmployeeService>();
                SimpleIoc.Default.Register<IProductService, ProductService>();
                SimpleIoc.Default.Register<IClientService, ClientService>(); 
                SimpleIoc.Default.Register<ITicketService, TicketService>();
                SimpleIoc.Default.Register<IOrderService, OrderService>();

                #endregion

                #region App services
                SimpleIoc.Default.Register<IAppEmployeeService, AppEmployeeService>();
                SimpleIoc.Default.Register<IAppLoginService, AppLoginService>();
                SimpleIoc.Default.Register<IAppClientService, AppClientService>();
                SimpleIoc.Default.Register<IAppProductService, AppProductService>();
                #endregion

            }


            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LoginViewModel>();
            SimpleIoc.Default.Register<SaleOnCreditViewModel>();
            SimpleIoc.Default.Register<ClientSettingsViewModel>();
            SimpleIoc.Default.Register<StorageViewModel>();
            SimpleIoc.Default.Register<ProductSettingsViewModel>();
            SimpleIoc.Default.Register<StoreViewModel>();
        }


        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public LoginViewModel Login
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LoginViewModel>();
            }
        }
         
        public SaleOnCreditViewModel SaleOnCredit
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SaleOnCreditViewModel>();
            }
        }

        public ClientSettingsViewModel ClientSettings
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ClientSettingsViewModel>();
            }
        }

        public StorageViewModel Storage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<StorageViewModel>();
            }
        }

        public ProductSettingsViewModel ProductSettings
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ProductSettingsViewModel>();
            }
        }

        public StoreViewModel Store
        {
            get
            {
                return ServiceLocator.Current.GetInstance<StoreViewModel>();
            }
        }

        public static void Cleanup()
        {

        }
    }
}