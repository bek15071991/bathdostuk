﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace BathDostukWPF.ViewModel
{
    public class StoreViewModel : ViewModelBase
    {
        private readonly IProductService _productService;

        public StoreViewModel(IProductService productService)
        {
            _productService = productService;
            BuyProducts = _productService.GetBuyProductsInStorage();
            _saleProducts = new ObservableCollection<SaleProduct>();
            SaleProductsStories = _productService.GetSaleProducts();
            Count = 1;
        }

        private ObservableCollection<SaleProduct> _saleProductsStories;
        public ObservableCollection<SaleProduct> SaleProductsStories
        {
            get
            {
                return _saleProductsStories;
            }
            set
            {
                _saleProductsStories = value;
                RaisePropertyChanged(() => SaleProductsStories);
            }
        }

        private ObservableCollection<SaleProduct> _saleProducts;
        public ObservableCollection<SaleProduct> SaleProducts
        {
            get { return _saleProducts; }
            set
            {
                _saleProducts = value;
                RaisePropertyChanged(() => SaleProducts);
            }
        }

        private ObservableCollection<BuyProduct> _buyProducts;
        public ObservableCollection<BuyProduct> BuyProducts
        {
            get { return _buyProducts; }
            set
            {
                _buyProducts = value;
                RaisePropertyChanged(() => BuyProducts);
            }
        }

        private BuyProduct _selectedBuyProduct;
        public BuyProduct SelectedBuyProduct
        {
            get { return _selectedBuyProduct; }
            set
            {
                _selectedBuyProduct = value;
                if (value == null) return;
                Price = value.Price;
                Total = value.Price * Count;
                var val = SaleProducts.Where(sp => sp.BuyProduct.Id == value.Id);
                CountInStorage = value.Count - val.Sum(c => c.Count);
            }
        }


        public SaleProduct SelectedSaleProduct { get; set; }

        private int _count;
        public int Count
        {
            get { return _count; }
            set
            {
                _count = value;
                Total = value * Price;
                RaisePropertyChanged(() => Count);
            }
        }

        private float _price;
        public float Price
        {
            get { return _price; }
            set
            {
                _price = value;
                RaisePropertyChanged(() => Price);
            }
        }

        private float _countInStorage;

        public float CountInStorage
        {
            get { return _countInStorage; }
            set
            {
                _countInStorage = value;
                RaisePropertyChanged(() => CountInStorage);
            }
        }

        private float _total;

        public float Total
        {
            get { return _total; }
            set
            {
                _total = value;
                RaisePropertyChanged(() => Total);
            }
        }


        #region добавить

        private ICommand _add;
        public ICommand Add
        {
            get
            {
                return _add ?? new RelayCommand(() =>
                {
                    if (Count > CountInStorage)
                    {
                        MessageBox.Show("Данного товара не осталось!!!", "Внимание...");
                        return;
                    }

                    var saleProduct = new SaleProduct();
                    saleProduct.BuyProduct = SelectedBuyProduct;
                    saleProduct.Cost = SelectedBuyProduct.Price;
                    saleProduct.Count = Count;
                    saleProduct.Date = DateTime.Now;
                    SaleProducts.Add(saleProduct);
                    SetDefaultValue();


                });
            }
        }

        #endregion


        #region удалить выбраный товар

        private ICommand _deleteProduct;
        public ICommand DeleteProduct
        {
            get
            {
                return _deleteProduct ?? new RelayCommand(() =>
                {
                    SaleProducts.Remove(SelectedSaleProduct);
                    SetDefaultValue();
                });
            }
        }

        #endregion


        #region  очистить

        private ICommand _clear;
        public ICommand Clear
        {
            get
            {
                return _clear ?? new RelayCommand(() =>
                {
                    SaleProducts.Clear();
                    SetDefaultValue();
                });
            }
        }

        #endregion

        #region  Сохранить

        private ICommand _save;
        public ICommand Save
        {
            get
            {
                return _save ?? new RelayCommand(() =>
                {
                    if (SaleProducts.Count < 1 ||
                        MessageBox.Show("Сохранить данные?", "Внимание...", MessageBoxButton.YesNo) !=
                        MessageBoxResult.Yes) return;
                    _productService.SaveSaleProduts(SaleProducts.ToList());
                    BuyProducts = _productService.GetBuyProductsInStorage();
                    SaleProducts.Clear();
                    SetDefaultValue();
                    SaleProductsStories = _productService.GetSaleProducts();
                });
            }
        }

        #endregion

        private void SetDefaultValue()
        {
            Count = 1;
            SelectedBuyProduct = null;
            CountInStorage = 0;
            Price = 0;
            Total = 0;
            var buyProducts = BuyProducts;
            BuyProducts = null;
            BuyProducts = buyProducts;
        }



    }

}
