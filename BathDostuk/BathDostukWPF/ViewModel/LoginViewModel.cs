﻿
using System.Windows;
using System.Windows.Input;
using BathDostukWPF.Model.Interface;
using BathDostukWPF.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace BathDostukWPF.ViewModel
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly IAppLoginService _employeeService;

        public LoginViewModel(IAppLoginService service)
        {
            _employeeService = service;

        }
         
        public string Login { get; set; }
        public string Password { get; set; }
        

        #region Логинится
        private ICommand _signIn;

        public ICommand SignIn
        {
            get
            {
                return _signIn ?? new RelayCommand(() =>
                {

                    // if (string.IsNullOrEmpty(Login) && string.IsNullOrEmpty(Password)) return;
                    var user = _employeeService.SignIn(login: Login, password: Password);
                    if (user == null) return;
                    ApplicationUser.SetUser(user);
                    var main = new MainWindow();
                    main.Show();
                    ((App)Application.Current).MainWindow.Close();

                });
            }
        }
        #endregion

 

        public override void Cleanup()
        {
            base.Cleanup();
        }
    }
}
