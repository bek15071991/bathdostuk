﻿using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using BathDostukDomain.Entities;
using BathDostukWPF.Helper;
using BathDostukWPF.Model.Interface;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace BathDostukWPF.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IAppEmployeeService _appEmployeeService;

        public MainViewModel(IAppEmployeeService appEmployeeService)
        {
            _appEmployeeService = appEmployeeService;
            Orders = new ObservableCollection<Order>();
            
            _saledOrders = _appEmployeeService.GetSalledTicketsOnToDay();
        }

        public string FioUser
        {
            get
            {
                return "Ф.И.О. :  " + ApplicationUser.CurrentUser.Fio;
            }
        }

        public BitmapImage Photo
        {
            get
            {
                var phot = GetImage(ApplicationUser.CurrentUser.Photo);
                return phot;
            }
        }

        public string MessageDialogResult { get; set; }

        public ObservableCollection<Order> Orders { get; set; }
         
        private ObservableCollection<Order> _saledOrders; 

        public ObservableCollection<Order> SaledOrders
        {
            get {return _saledOrders; }
            private set
            {
                _saledOrders = value;
                RaisePropertyChanged(() => SaledOrders);
            }
        }

        public Order SelectedOrder { get; set; }

        private string _totalOrders;

        public string TotalOrders
        {
            get { return _totalOrders; }
            private set
            {
                _totalOrders = value;
                RaisePropertyChanged(() => TotalOrders);
            }
        }

        private bool _isCredit; 
        public bool IsCredit
        {
            get { return _isCredit; }
            set
            {
                _isCredit = value;
                RaisePropertyChanged(() => IsCredit);
            }
        }
 


        #region Add man ticket
        private ICommand _addManTicket;

        public ICommand AddManTicket
        {
            get
            {
                return _addManTicket ?? new RelayCommand(() =>
                {
                    var man = _appEmployeeService.GetManTicket();
                    if (man == null)
                    {
                        MessageBox.Show("Настройте билеты");
                        return;
                    }
                    Orders.Add(new Order
                    {
                        Ticket = man.Name,
                        Amount = man.Cost
                    });
                    SetTotalOrders();
                });
            }
        }
        #endregion

        #region Add woman ticket
        private ICommand _addWomanTicket;

        public ICommand AddWomanTicket
        {
            get
            {
                return _addWomanTicket ?? new RelayCommand(() =>
                {
                    var woman = _appEmployeeService.GetWomanTicket();
                    if (woman == null)
                    {
                        MessageBox.Show("Настройте билеты");
                        return;
                    }
                    Orders.Add(new Order
                    {
                        Ticket = woman.Name,
                        Amount = woman.Cost
                    });
                    SetTotalOrders();
                });
            }
        }
        #endregion

        #region Add child ticket
        private ICommand _addChildTicket;

        public ICommand AddChildTicket
        {
            get
            {
                return _addChildTicket ?? new RelayCommand(() =>
                {
                    var child = _appEmployeeService.GetChildTicket();
                    if (child == null)
                    {
                        MessageBox.Show("Настройте билеты");
                        return;
                    }
                    Orders.Add(new Order
                    {
                        Ticket = child.Name,
                        Amount = child.Cost
                    });
                    SetTotalOrders();
                });
            }
        }
        #endregion

        #region Clean orders
        private ICommand _cleanOrders;

        public ICommand CleanOrders
        {
            get
            {
                return _cleanOrders ?? new RelayCommand(() =>
                {
                    Orders.Clear();
                    SetTotalOrders();
                });
            }
        }
        #endregion


        #region Sale tickets

        private ICommand _saleTickets;

        public ICommand SaleTickets
        {
            get
            {
                return _saleTickets ?? new RelayCommand(() =>
                {
                    if (Orders.Count == 0) return;

                    var saledOrders = _appEmployeeService.GetSalledTicketsOnToDay();



                    if (IsCredit)
                    {
                        if (MessageBox.Show("Сохранить в долг?", "Внимание...", MessageBoxButton.YesNo) == MessageBoxResult.No)
                            return;
                        Messenger.Default.Send<StateBath>(new StateBath
                        {
                            MessageState = MessageState.ShowDialog,
                            Views = Views.SaleOnCreditWindow,
                            Value = Orders.ToList()
                        });
                    }
                    else
                    {
                        if (MessageBox.Show("Сохранить ?", "Внимание...", MessageBoxButton.YesNo) == MessageBoxResult.No)
                            return;
                        _appEmployeeService.SaleTickets(Orders);
                    }

                    SaledOrders = _appEmployeeService.GetSalledTicketsOnToDay();

                    if (saledOrders.Count != SaledOrders.Count)
                    {
                        Orders.Clear();
                        SetTotalOrders();
                        IsCredit = false;
                        MessageBox.Show("Данные были сохраненны!", "Внимание...");
                    }


                });
            }
        }

        #endregion


        #region Delete salled ticket

        private ICommand _deleteSalledTicket;

        public ICommand DeleteSalledTicket
        {
            get
            {
                return _deleteSalledTicket ?? new RelayCommand(() =>
                {
                    if (SelectedOrder == null || MessageBox.Show("Удалить ?", "Внимание...", MessageBoxButton.YesNo) == MessageBoxResult.No) return;
                    SelectedOrder.Cassier = null;
                     
                    _appEmployeeService.RemoveOrder(SelectedOrder);
                    SaledOrders.Remove(SelectedOrder);
                });
            }
        }

        #endregion


        #region Show store

        private ICommand _showStore;

        public ICommand ShowStore
        {
            get
            {
                return _showStore ?? new RelayCommand(() =>
                {
                    Messenger.Default.Send<StateBath>(new StateBath
                    {
                        MessageState = MessageState.Show,
                        Views = Views.StoreWindow
                    });
                });
            }
        }

        #endregion

        #region Show storage

        private ICommand _showStorage;

        public ICommand ShowStorage
        {
            get
            {
                return _showStorage ?? new RelayCommand(() =>
                {
                    Messenger.Default.Send<StateBath>(new StateBath
                    {
                        MessageState = MessageState.Show,
                        Views = Views.StorageWindow
                    });
                });
            }
        }

        #endregion

        private void SetTotalOrders()
        {
            TotalOrders = "Итого: " + Orders.Sum(or => or.Amount) + " сом.";
        }

        public override void Cleanup()
        {
            // Clean up if needed

            base.Cleanup();
        }


        private static BitmapImage GetImage(byte[] bytes)
        {
            var memoryStream1 = new MemoryStream(bytes);
            var bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = memoryStream1;
            bi.EndInit();
            return bi;
        }
    }
}