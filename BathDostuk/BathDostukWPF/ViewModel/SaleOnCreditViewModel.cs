﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukWPF.Helper;
using BathDostukWPF.Model.Interface;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace BathDostukWPF.ViewModel
{
    public class SaleOnCreditViewModel : ViewModelBase
    {

        private readonly IAppEmployeeService _appEmployeeService;
        private readonly IAppClientService _appClientService;

        public SaleOnCreditViewModel(IAppEmployeeService appEmployeeService, IAppClientService appClientService)
        {
            _appEmployeeService = appEmployeeService;
            _appClientService = appClientService;
            Clients = _appClientService.GetAllNotDeleted();
        }

        private string _amount;
        public string Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                _amount = value;
                RaisePropertyChanged(() => Amount);
            }
        }

        private float _total;
        public float Total
        {
            get { return _total; }
            set
            {
                _total = value;
                RaisePropertyChanged(() => Total);
            }
        }


        private string _comment;
        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = value;
                RaisePropertyChanged(() => Comment);
            }
        }


        private Client _selectedClient;
        public Client SelectedClient
        {
            get { return _selectedClient; }
            set
            {
                _selectedClient = value;
                RaisePropertyChanged(() => SelectedClient);
            }
        }

        private ObservableCollection<Client> _clients;

        public ObservableCollection<Client> Clients
        {
            get
            {
                return _clients;
            }
            set
            {
                _clients = value;
                RaisePropertyChanged(() => Clients);
            }
        }

        private ObservableCollection<Order> _orders; 
        public ObservableCollection<Order> Orders {
            get {  return _orders; }
            set
            {
                _orders = value;
                _amount = Orders == null ? null : "Всего :  " + Orders.Sum(c => c.Amount) + " сом.";
                Total = Orders == null ? 0 : Orders.Sum(c => c.Amount);
            }
        }

        #region Save

        private ICommand _save;

        public ICommand Save
        {
            get
            {
                return _save ?? (new RelayCommand(() =>
                {
                    if (SelectedClient == null || Total < 1) return;

                    _appEmployeeService.SaleTicketsOnCredit(Orders, SelectedClient, Comment);

                    Orders.Clear();
                    SelectedClient = null;
                    Amount =
                    Comment = string.Empty;
                    Total = 0;
                    MessageBox.Show("Успешно сохранено!", "Внимание...");

                }));
            }
        }

        #endregion

        #region AddClient

        private ICommand _addClient;

        public ICommand AddClient
        {
            get
            {
                return _addClient ?? (_addClient = new RelayCommand(() =>
                    {
                        Messenger.Default.Send<StateBath>(new StateBath
                        {
                            MessageState = MessageState.Show,
                            Views = Views.ClientSettingsWindow
                        });
                    }));
            }
        }

        #endregion

    }
}
