﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class ProductSetting : Form,IViewProductSetting
    {
        public ProductSetting()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            _textBoxBarCode.KeyPress += (sender, args) =>
            {
                if (char.IsDigit(args.KeyChar))
                    args.Handled = false;
                else
                    args.Handled = true;
            };

            _buttonDelete.Click += (sender, args) =>
            {
                Delete(this, EventArgs.Empty);
            };

            _buttonSave.Click += (sender, args) => { Save(this, EventArgs.Empty); };
        }

        public string BarCode
        {
            get { return _textBoxBarCode.Text; }
            set { _textBoxBarCode.Text = value; }
        }

        public string NameProduct
        {
            get { return _textBoxName.Text; }
            set { _textBoxName.Text = value; }
        }

        public string Ed
        {
            get { return _textBoxEd.Text; }
            set { _textBoxEd.Text = value; }
        }

        public event EventHandler<EventArgs> Save;
        public event EventHandler<EventArgs> Delete;

        public List<Product> Products
        {
            get { return (List<Product>)objectListView1.Objects; }
            set { objectListView1.Objects = value; }
        }

        public Product SelectedProduct
        {
            get { return (Product) objectListView1.SelectedItem.RowObject; }
            set { objectListView1.SelectedObject =  value; }
        }
    }
}
