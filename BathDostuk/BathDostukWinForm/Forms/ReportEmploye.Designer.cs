﻿namespace BathDostukWinForm.Forms
{
    partial class ReportEmploye
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.objectListView2 = new BrightIdeasSoftware.ObjectListView();
            this._olvColumnUser = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnBath = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._comboBoxUser = new System.Windows.Forms.ComboBox();
            this._buttonSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._dtpTo = new System.Windows.Forms.DateTimePicker();
            this._dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // objectListView2
            // 
            this.objectListView2.AllColumns.Add(this._olvColumnUser);
            this.objectListView2.AllColumns.Add(this._olvColumnBath);
            this.objectListView2.AllColumns.Add(this.olvColumn2);
            this.objectListView2.AllColumns.Add(this.olvColumn1);
            this.objectListView2.AllColumns.Add(this.olvColumn3);
            this.objectListView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._olvColumnUser,
            this._olvColumnBath,
            this.olvColumn2,
            this.olvColumn1,
            this.olvColumn3});
            this.objectListView2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView2.FullRowSelect = true;
            this.objectListView2.GridLines = true;
            this.objectListView2.Location = new System.Drawing.Point(256, 0);
            this.objectListView2.MultiSelect = false;
            this.objectListView2.Name = "objectListView2";
            this.objectListView2.ShowGroups = false;
            this.objectListView2.Size = new System.Drawing.Size(989, 426);
            this.objectListView2.TabIndex = 19;
            this.objectListView2.UseCompatibleStateImageBehavior = false;
            this.objectListView2.View = System.Windows.Forms.View.Details;
            // 
            // _olvColumnUser
            // 
            this._olvColumnUser.AspectName = "User";
            this._olvColumnUser.CellPadding = null;
            this._olvColumnUser.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._olvColumnUser.Text = "Сотрудники";
            this._olvColumnUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._olvColumnUser.Width = 359;
            // 
            // _olvColumnBath
            // 
            this._olvColumnBath.AspectName = "Days";
            this._olvColumnBath.CellPadding = null;
            this._olvColumnBath.Text = "Кол-во отработанных дней";
            this._olvColumnBath.Width = 155;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Salary";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Выданные зарплаты";
            this.olvColumn2.Width = 124;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._comboBoxUser);
            this.groupBox2.Controls.Add(this._buttonSearch);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._dtpTo);
            this.groupBox2.Controls.Add(this._dtpFrom);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 426);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск";
            // 
            // _comboBoxUser
            // 
            this._comboBoxUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxUser.FormattingEnabled = true;
            this._comboBoxUser.Location = new System.Drawing.Point(26, 140);
            this._comboBoxUser.Name = "_comboBoxUser";
            this._comboBoxUser.Size = new System.Drawing.Size(199, 21);
            this._comboBoxUser.TabIndex = 3;
            // 
            // _buttonSearch
            // 
            this._buttonSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSearch.Location = new System.Drawing.Point(62, 186);
            this._buttonSearch.Name = "_buttonSearch";
            this._buttonSearch.Size = new System.Drawing.Size(117, 32);
            this._buttonSearch.TabIndex = 2;
            this._buttonSearch.Text = "Показать";
            this._buttonSearch.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сотрудник";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "по";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "C";
            // 
            // _dtpTo
            // 
            this._dtpTo.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpTo.Location = new System.Drawing.Point(53, 68);
            this._dtpTo.Name = "_dtpTo";
            this._dtpTo.Size = new System.Drawing.Size(153, 20);
            this._dtpTo.TabIndex = 0;
            // 
            // _dtpFrom
            // 
            this._dtpFrom.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpFrom.Location = new System.Drawing.Point(53, 24);
            this._dtpFrom.Name = "_dtpFrom";
            this._dtpFrom.Size = new System.Drawing.Size(153, 20);
            this._dtpFrom.TabIndex = 0;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "PlanSalary";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.Text = "Зарплата по плану";
            this.olvColumn1.Width = 111;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "StaySalary";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.Text = "Осталось зарплаты";
            this.olvColumn3.Width = 122;
            // 
            // ReportEmploye
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 426);
            this.Controls.Add(this.objectListView2);
            this.Controls.Add(this.groupBox2);
            this.Name = "ReportEmploye";
            this.ShowIcon = false;
            this.Text = "Отчет по сотрудникам";
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView objectListView2;
        private BrightIdeasSoftware.OLVColumn _olvColumnUser;
        private BrightIdeasSoftware.OLVColumn _olvColumnBath;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox _comboBoxUser;
        private System.Windows.Forms.Button _buttonSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker _dtpTo;
        private System.Windows.Forms.DateTimePicker _dtpFrom;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn3;

    }
}