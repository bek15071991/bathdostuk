﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class ReportBath : Form, IViewReportBath
    {
        public ReportBath()
        {
            InitializeComponent();
            _buttonSearch.Click += (sender, args) => { Search(this, EventArgs.Empty); };

            _olvColumnGender.AspectToStringConverter = (ob) =>
            {
                if (ob == null) return string.Empty;
                var gender = (Gender) ob;
                return EnumConvert.ToString(gender);
            }
                ;
        }

        public DateTime From
        {
            get { return _dtpFrom.Value; }
        }

        public DateTime To
        {
            get { return _dtpTo.Value; }
        }

        public void ShowOrders(List<Order> orders)
        {
            objectListView2.Objects = orders; 
            _textBoxTotal2.Text = "Всего билетов: " + orders.Count + "    " + "Итого :   " + orders.Sum(s=>s.Ticket.Cost) + " сом."; ;
        }

        public void ShowUsers(List<Employee> employees)
        {
            var user = new Employee
            {
                Fio = "Все"
            };
            employees.Add(user);
            _comboBoxUser.DataSource = employees;
            _comboBoxUser.DisplayMember = "Fio";
            _comboBoxUser.SelectedItem = user;
        }

       

        public Employee SeletcteEmployee
        {
            get { return _comboBoxUser.SelectedItem as Employee; }
            set
            {
                if (value == null)
                    _comboBoxUser.SelectedIndex = -1;
            }
        }

        public event EventHandler<EventArgs> Search;

        private void objectListView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
