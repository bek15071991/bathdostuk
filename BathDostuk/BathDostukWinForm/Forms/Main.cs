﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;

using System.Windows.Forms;
using BathDostukDAL.Repositories;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukWinForm.Presenters;
using Rule = BathDostukDomain.Entities.Enum.Rule;

namespace BathDostukWinForm.Forms
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            Closing += (sender, args) =>
            {
                if (
                    MessageBox.Show("Вы желаете выйти из программы?", "Выход...", MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Application.Exit();
                    return;
                }
                args.Cancel = true;
            };

            statusStrip1.Items[0].Text = "Пользователь :   " + User.CurrentUser.Fio + new string(' ', 10);
            statusStrip1.Items[1].Text = "Роль :   " + EnumConvert.ToString(User.CurrentUser.Rule) + new string(' ', 10);
            statusStrip1.Items[2].Text = "Время :   " + DateTime.Now.ToLongTimeString() + new string(' ', 10);
            statusStrip1.Items[3].Text = "Дата :   " + DateTime.Now.ToLongDateString();


            switch (User.CurrentUser.Rule)
            {
                case Rule.Cassier:
                    {
                        товарыToolStripMenuItem.Visible =
                        _tsmBath.Visible =
                        _tsmStorage.Visible =
                            _tsmStore.Visible =
                                _tsmReportDay.Visible =
                                расходToolStripMenuItem1.Visible = true;
                        break;
                    }

                case Rule.Barman:
                    {
                        товарыToolStripMenuItem.Visible =
                        _tsmStorage.Visible =
                             _tsmStore.Visible =
                             товарыToolStripMenuItem.Visible =
                                 _tsmReportDay.Visible = true;

                        break;
                    }
                case Rule.Admin:
                    {
                        _tsmBath.Visible =
                        _tsmStorage.Visible =
                            _tsmStore.Visible =
                            _tsmStatistik.Visible =
                            товарыToolStripMenuItem.Visible =
                            _tsmReportDay.Visible =
                            _tsmManagment.Visible =
                                _tsmReportDay.Visible =
                                расходToolStripMenuItem1.Visible = true;
                        break;
                    }
                case Rule.Director:
                    {
                        _tsmStatistik.Visible =
                        товарыToolStripMenuItem.Visible =
                        _tsmManagment.Visible = true;
                        break;
                    }

            }







        }


        private void продуктыToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        public void ShowForm(Form form)
        {
            DeleteChildForms();
            form.MdiParent = this;
            form.Icon = Icon;
            form.WindowState = FormWindowState.Maximized;
            form.Show();
        }


        private void DeleteChildForms()
        {
            foreach (var mdiChild in MdiChildren)
                mdiChild.Close();
        }



        private void еToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new Bath();
            new PresenterBath(form, new OrderRepository(), new TicketRepository());
            ShowForm(form);

        }

        private void sdfdfdfdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new Storage();
            new PresenterStorage(form, new ProductRepository(), new BuyProductRepository());
            ShowForm(form);
        }

        private void sdfsdfToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new Store();
            new PresenterStore(form, new SaleProductRepository(), new BuyProductRepository());
            ShowForm(form);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            statusStrip1.Items[2].Text = "Время :   " + DateTime.Now.ToLongTimeString() + new string(' ', 10);
            statusStrip1.Items[3].Text = "Дата :   " + DateTime.Now.ToLongDateString();
        }

        private void пользователиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new UserSetting();
            new PresenterUserSetting(form, new EmployeeRepository());
            ShowForm(form);
        }

        private void выдачаЗарплатыToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void наТекущийДеньToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ReportOnDay();
            new PresenterReportOnDay(form, new SaleProductRepository(), new OrderRepository(), new ReportOnDayRepository(), new ExpenseRepository(), new EmployeeRepository());
            ShowForm(form);
        }

        private void баняToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var form = new ReportBath();
            new PresenterReportBath(form, new OrderRepository(), new EmployeeRepository());
            ShowForm(form);
        }

        private void складМагазинToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ReportProduct();
            new PresenterReportProduct(form, new BuyProductRepository(), new SaleProductRepository(), new EmployeeRepository());
            ShowForm(form);
        }

        private void зарплатыToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var form = new ReportSalary();
            new PresenterReportSalary(form, new ExpenseRepository(), new EmployeeRepository(), new ReportOnDayRepository());
            ShowForm(form);
        }

        private void общийОтчетToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ReportExpenses();
            new PresenterReportExpense(form, new ExpenseRepository());
            ShowForm(form);
        }

        private void настройкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new TicketSetting();
            new PresenterTicketSetting(new TicketRepository(), form, new OrderRepository());
            ShowForm(form);

        }


        private void общийОтчетToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var form = new ReportEmploye();
            new PresenterReportEmploye(form, new EmployeeRepository(),
                new ReportOnDayRepository(), new ExpenseRepository());
            ShowForm(form);
        }

        private void оСистемеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new About().ShowDialog();
        }

        private void записьРасходаToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var form = new ExpenseForm();
            new PresenterExpense(form, new ExpenseRepository(), new EmployeeRepository());
            ShowForm(form);
        }

        private void товарыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new ProductSetting();
            new PresenterProductSetting(form, new ProductRepository(), new BuyProductRepository());
            ShowForm(form);
        }

        private void выдачаЗарплатыToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var form = new GiveSalary();
            new PresenterGiveSalary(form, new ExpenseRepository(), new EmployeeRepository());
            ShowForm(form);
        }

        private void общийОтчетToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            var form = new GeneralReport();
            new PresenterGeneralReport(form, new EmployeeRepository(),
                new SaleProductRepository(),
                new ReportOnDayRepository(),
                new ExpenseRepository());
            ShowForm(form);
        }

    }
}
