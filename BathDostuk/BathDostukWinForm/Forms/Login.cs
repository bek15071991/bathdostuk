﻿using System;
using System.Windows.Forms;
using BathDostukDAL.Migrations;
using NewBath.Views;

namespace BathDostukWinForm.Forms
{
    public partial class Login : Form,IViewLogin
    {
        public Login()
        {
            InitializeComponent();
            _buttonCancel.Click += (sender, args) => { Application.Exit(); };

            _buttonOk.Click += (sender, args) =>
            {
                LogIn(this, EventArgs.Empty);
            };

            _buttonSettingDataBase.Click += (sender, args) =>
            {
                new AppSetting().ShowDialog();
            };
        }

        public event EventHandler<EventArgs> LogIn;

          string IViewLogin.Login
        {
            get { return _textBoxLogin.Text; }
        }

        public string Password
        {
            get { return _textBoxPassword.Text; }
        }

        public void CloseForm()
        {
            this.Hide();
        }
    }
}
