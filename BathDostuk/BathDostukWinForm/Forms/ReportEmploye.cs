﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class ReportEmploye : Form, IViewReportEmploey
    {
        public ReportEmploye()
        {
            InitializeComponent();
            _buttonSearch.Click += (sender, args) => Search(this, new EventArgs());
        }

        public DateTime From
        {
            get { return _dtpFrom.Value.Date; }
        }

        public DateTime To
        {
            get { return _dtpTo.Value.Date; }
        }

        public void ShowUsers(List<Employee> employees)
        {
            var user = new Employee { Fio = "все" };

            employees.Add(user);
            _comboBoxUser.DataSource = employees;
            _comboBoxUser.DisplayMember = "Fio";
            _comboBoxUser.SelectedItem = user;
        }

        public Employee SeletcteEmployee
        {
            get { return _comboBoxUser.SelectedItem as Employee; }
            set { _comboBoxUser.SelectedItem = value; }
        }

        public event EventHandler<EventArgs> Search;

        public void ShowReport(Employee user, double days, double salary, double planSalary, double staySalary)
        {
            objectListView2.AddObject(new
            {
                Days = days,
                Salary = salary,
                User = user.Fio,
                PlanSalary = planSalary,
                StaySalary = staySalary
            });
        }

        public void ClearList()
        {
            objectListView2.ClearObjects();
        }
    }
}
