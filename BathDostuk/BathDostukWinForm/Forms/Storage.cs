﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class Storage : Form, IViewStorage
    {
        public Storage()
        {
            InitializeComponent();
            _buttonAdd.Click += (sender, args) => { Add(this, EventArgs.Empty); };
            _buttonDelete.Click += (sender, args) =>
            {
                Delete(this, EventArgs.Empty);
            };
            _buttonSave.Click += (sender, args) =>
            {
                Save(this, EventArgs.Empty);
            };
            _buttonSearch.Click += (sender, args) => { Searche(this, EventArgs.Empty); };
            _buttonShowAll.Click += (sender, args) => { ShowAll(this, EventArgs.Empty); };

            _textBoxCount.TextChanged += TextBoxCountOnTextChanged;
            _textBoxCost.TextChanged += TextBoxCountOnTextChanged;

            _textBoxCost.KeyPress += TextBoxCostOnKeyPress;
            _textBoxCount.KeyPress += TextBoxCostOnKeyPress;
            _comboBoxBarCode.KeyPress += TextBoxCostOnKeyPress;
            BuyProducts = new List<BuyProduct>();
            BuyProductsInStorage = new List<BuyProduct>();
        }

        private void TextBoxCostOnKeyPress(object sender, KeyPressEventArgs keyPressEventArgs)
        {
            if (char.IsDigit(keyPressEventArgs.KeyChar) ||
                keyPressEventArgs.KeyChar == (char)8 ||
                keyPressEventArgs.KeyChar == ',')
                keyPressEventArgs.Handled = false;
            else keyPressEventArgs.Handled = true;
        }

        private void TextBoxCountOnTextChanged(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(_textBoxCost.Text) ||
                 string.IsNullOrEmpty(_textBoxCount.Text))
                return;
            var amotun = Convert.ToDouble(_textBoxCost.Text) * Convert.ToDouble(_textBoxCount.Text);
            _textBoxAmount.Text = amotun.ToString();
        }

        public void ShowProducts(List<Product> products)
        {
            _comboBoxProduct.DataSource =
            _comboBoxBarCode.DataSource = products;

            _comboBoxProduct.DisplayMember = "Name";
            _comboBoxBarCode.DisplayMember = "BarCode";
            _comboBoxBarCode.SelectedIndex =
            _comboBoxProduct.SelectedIndex = -1;
        }

        public Product SelectedProduct
        {
            get
            {
                if (_comboBoxProduct.SelectedIndex != -1)
                    return (Product)_comboBoxProduct.SelectedItem;
                else
                    return (Product)_comboBoxBarCode.SelectedItem;
            }
            set { if (value == null) 
                _comboBoxBarCode.SelectedIndex=
                _comboBoxProduct.SelectedIndex = -1; }
        }

        public float Cost
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxCost.Text))
                    return 0;
                return (float)Convert.ToDouble(_textBoxCost.Text);
            }
            set { _textBoxCost.Text = value.ToString(); }
        }

        public float Price
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxPrice.Text))
                    return 0;
                return (float)Convert.ToDouble(_textBoxPrice.Text);
            }
            set { _textBoxPrice.Text = value.ToString(); }
        }

        public float Count
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxCount.Text))
                    return 0;
                return (float)Convert.ToDouble(_textBoxCount.Text);
            }
            set { _textBoxCount.Text = value.ToString(); }
        }

        public List<BuyProduct> BuyProducts
        {
            get { return objectListView1.Objects as List<BuyProduct>; }
            set
            {
                var amount = value == null ? 0 : value.Sum(bp => bp.Total);
                _textBoxTotal.Text = "Итого: " + amount + " сом.";
                objectListView1.Objects = value;
            }
        }

        public BuyProduct SelectedBuyProduct
        {
            get { return objectListView1.SelectedObject as BuyProduct; }
            set { objectListView1.SelectedObject = value; }
        }

        public List<BuyProduct> BuyProductsInStorage
        {
            get { return objectListView2.Objects as List<BuyProduct>; }
            set
            {
                var amount = value == null ? 0 : value.Sum(bp => bp.Total);
                _textBoxTotal2.Text = "Итого: " + amount + " сом.";
                objectListView2.Objects = value;
            }
        }

        public DateTime From
        {
            get { return _dtpFrom.Value; }
            set { _dtpFrom.Value = value; }
        }

        public DateTime To
        {
            get { return _dtpTo.Value; }
            set { _dtpTo.Value = value; }
        }

        public event EventHandler<EventArgs> Add;
        public event EventHandler<EventArgs> Delete;
        public event EventHandler<EventArgs> Save;
        public event EventHandler<EventArgs> Searche;
        public event EventHandler<EventArgs> ShowAll;
    }
}
