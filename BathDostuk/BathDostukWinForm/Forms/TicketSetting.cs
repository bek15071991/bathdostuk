﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class TicketSetting : Form, IViewTicketSetting
    {
        public TicketSetting()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            _olvColumnGender.AspectToStringConverter = (ob) =>
            {
                var gender = (Gender)ob;
                return gender == Gender.Female ? "Женский" : "Мужской";
            };

            _olvColumnIsDelete.AspectToStringConverter = (ob) => (bool) ob ? "Удален" : "Не удален";

            _buttonDelete.Click +=
                (sender, args) =>
                {
                    Delete(this, EventArgs.Empty);
                };

            _buttonSave.Click += (sender, args) =>
            {
                Save(this, EventArgs.Empty);
            };

            _textBoxCost.KeyPress += (sender, args) =>
            {
                if (char.IsDigit(args.KeyChar) ||
              args.KeyChar == (char)8)
                    args.Handled = false;
                else args.Handled = true;
            };
        }

        public int Cost
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxCost.Text)) return 0;
                return Convert.ToInt32(_textBoxCost.Text);
            }
            set { _textBoxCost.Text = value.ToString(); }
        }

        public string TicketName
        {
            get { return _textBoxName.Text; }
            set { _textBoxName.Text = value; }
        }

        public event EventHandler<EventArgs> Save;
        public event EventHandler<EventArgs> Delete;

        public Ticket SelectedTicket
        {
            get { return (Ticket)objectListView1.SelectedItem.RowObject; }
            set { objectListView1.SelectedObject = value; }

        }

        public Gender ViewGender
        {
            get { return (Gender) _comboBoxGender.SelectedIndex; }
            set
            {
                if ((int) value == 2)
                    _comboBoxGender.SelectedIndex = -1;
            }
        }


        public List<Ticket> Tickets
        {
            get { return objectListView1.Objects as List<Ticket>; }
            set { objectListView1.Objects = value; }
        }
    }
}
