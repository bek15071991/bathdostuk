﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class Bath : Form, IViewBath
    {
        public Bath()
        {
            InitializeComponent();

            _olvColumnGender.AspectToStringConverter = (ob) =>
            { 
                if (ob == null) return string.Empty;
                var gender = (Gender) ob;
                return EnumConvert.ToString(gender);
            };

            _olvColumnGender1.AspectToStringConverter = (ob) =>
            { 
                if (ob == null) return string.Empty;
                var gender = (Gender)ob;
                return EnumConvert.ToString(gender);
            };


            _comboBoxTicket.SelectedIndexChanged += (sender, args) =>
            {
                var ticket = _comboBoxTicket.SelectedItem as Ticket;
                if (ticket == null)
                {
                    _textBoxGender.Text =
                    _textBoxCost.Text =   string.Empty;

                    return;
                } 
                _textBoxCost.Text = ticket.Cost.ToString();
                _textBoxGender.Text = EnumConvert.ToString(ticket.Gender);
            };

            _buttonAdd.Click += (sender, args) =>
            {
                Add(this, EventArgs.Empty);
            };

            _buttonDelete.Click += (sender, args) =>
            {
                Delete(this, EventArgs.Empty);
            };

            _buttonSave.Click += (sender, args) =>
            {
                Save(this, EventArgs.Empty);
            };
        }

       

        public List<Ticket> Tickets
        {
            get { return _comboBoxTicket.Items.Cast<Ticket>().ToList(); }
            set
            {
                _comboBoxTicket.DataSource = value;
                _comboBoxTicket.DisplayMember = "Name";
                _comboBoxTicket.SelectedIndex = -1;
                _textBoxGender.Text = 
                _textBoxCost.Text = string.Empty;
                
            }
        }

        public Ticket SelectedTicket
        {
            get { return _comboBoxTicket.SelectedItem as Ticket; }
            set { _comboBoxTicket.SelectedItem = value; }
        }

        public Order SelectedOrder
        {
            get { return objectListView1.SelectedObject as Order; }
            set { objectListView1.SelectedObject = value; }
        }

      
        public List<Order> Orders
        {
            get { return objectListView1.Objects as List<Order>; }
            set
            {
                var sum = value == null ? 0 : value.Sum(r => r.Ticket.Cost);
                _textBoxTotal.Text = "Итого: " + sum;
                objectListView1.Objects = value;
            }
        }

        public List<Order> PaydOrders
        {
            get { return objectListView2.Objects as List<Order>; }
            set
            {
                _textBoxTotal2.Text = "Итого: " + value.Sum(r => r.Ticket.Cost);
                objectListView2.Objects = value;
            }
        }

        public event EventHandler<EventArgs> Add;
        public event EventHandler<EventArgs> Save;
        public event EventHandler<EventArgs> Delete;
    }
}
