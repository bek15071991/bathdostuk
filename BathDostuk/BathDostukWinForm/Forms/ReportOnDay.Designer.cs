﻿namespace BathDostukWinForm.Forms
{
    partial class ReportOnDay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._buttonSave = new System.Windows.Forms.Button();
            this._textBoxTotal = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this._listOrders = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnGender = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxBathTotal = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this._listSaleProducts = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn10 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn11 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxStoreTotal = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this._listExpenses = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn7 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxTotalExpense = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this._listSalaries = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn12 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn8 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn9 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxTotalSalaries = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this._listUsers = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn13 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn15 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnRule = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._comboBoxUsers = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this._buttonAdd = new System.Windows.Forms.Button();
            this._buttonDelete = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listOrders)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listSaleProducts)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listExpenses)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listSalaries)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listUsers)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._buttonSave);
            this.groupBox1.Controls.Add(this._textBoxTotal);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 429);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1115, 37);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // _buttonSave
            // 
            this._buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSave.Location = new System.Drawing.Point(721, 6);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(137, 28);
            this._buttonSave.TabIndex = 12;
            this._buttonSave.Text = "Сдать деньги";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // _textBoxTotal
            // 
            this._textBoxTotal.Location = new System.Drawing.Point(389, 11);
            this._textBoxTotal.Name = "_textBoxTotal";
            this._textBoxTotal.ReadOnly = true;
            this._textBoxTotal.Size = new System.Drawing.Size(326, 20);
            this._textBoxTotal.TabIndex = 11;
            this._textBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1115, 429);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._listOrders);
            this.tabPage1.Controls.Add(this._textBoxBathTotal);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(854, 392);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Баня";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // _listOrders
            // 
            this._listOrders.AllColumns.Add(this.olvColumn3);
            this._listOrders.AllColumns.Add(this.olvColumn5);
            this._listOrders.AllColumns.Add(this._olvColumnGender);
            this._listOrders.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn3,
            this.olvColumn5,
            this._olvColumnGender});
            this._listOrders.Cursor = System.Windows.Forms.Cursors.Hand;
            this._listOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listOrders.FullRowSelect = true;
            this._listOrders.GridLines = true;
            this._listOrders.Location = new System.Drawing.Point(3, 3);
            this._listOrders.MultiSelect = false;
            this._listOrders.Name = "_listOrders";
            this._listOrders.ShowGroups = false;
            this._listOrders.Size = new System.Drawing.Size(848, 366);
            this._listOrders.TabIndex = 11;
            this._listOrders.UseCompatibleStateImageBehavior = false;
            this._listOrders.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "Ticket.Name";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn3.Text = "Билет";
            this.olvColumn3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn3.Width = 253;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "Ticket.Cost";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.Text = "Цена за час";
            this.olvColumn5.Width = 118;
            // 
            // _olvColumnGender
            // 
            this._olvColumnGender.AspectName = "Ticket.Gender";
            this._olvColumnGender.CellPadding = null;
            this._olvColumnGender.Text = "Отделение";
            this._olvColumnGender.Width = 102;
            // 
            // _textBoxBathTotal
            // 
            this._textBoxBathTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxBathTotal.Location = new System.Drawing.Point(3, 369);
            this._textBoxBathTotal.Name = "_textBoxBathTotal";
            this._textBoxBathTotal.ReadOnly = true;
            this._textBoxBathTotal.Size = new System.Drawing.Size(848, 20);
            this._textBoxBathTotal.TabIndex = 10;
            this._textBoxBathTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._listSaleProducts);
            this.tabPage2.Controls.Add(this._textBoxStoreTotal);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(854, 392);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Магазин";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // _listSaleProducts
            // 
            this._listSaleProducts.AllColumns.Add(this.olvColumn1);
            this._listSaleProducts.AllColumns.Add(this.olvColumn2);
            this._listSaleProducts.AllColumns.Add(this.olvColumn4);
            this._listSaleProducts.AllColumns.Add(this.olvColumn10);
            this._listSaleProducts.AllColumns.Add(this.olvColumn11);
            this._listSaleProducts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn4,
            this.olvColumn10,
            this.olvColumn11});
            this._listSaleProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this._listSaleProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listSaleProducts.FullRowSelect = true;
            this._listSaleProducts.GridLines = true;
            this._listSaleProducts.Location = new System.Drawing.Point(3, 3);
            this._listSaleProducts.MultiSelect = false;
            this._listSaleProducts.Name = "_listSaleProducts";
            this._listSaleProducts.ShowGroups = false;
            this._listSaleProducts.Size = new System.Drawing.Size(848, 366);
            this._listSaleProducts.TabIndex = 11;
            this._listSaleProducts.UseCompatibleStateImageBehavior = false;
            this._listSaleProducts.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "BuyProduct.Product.Name";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Товар";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 253;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "BuyProduct.Product.BarCode";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Баркод";
            this.olvColumn2.Width = 123;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "BuyProduct.Price";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.Text = "Цена на продажу";
            this.olvColumn4.Width = 146;
            // 
            // olvColumn10
            // 
            this.olvColumn10.AspectName = "Count";
            this.olvColumn10.CellPadding = null;
            this.olvColumn10.Text = "Кол-во";
            this.olvColumn10.Width = 88;
            // 
            // olvColumn11
            // 
            this.olvColumn11.AspectName = "Total";
            this.olvColumn11.CellPadding = null;
            this.olvColumn11.Text = "Сумма";
            this.olvColumn11.Width = 85;
            // 
            // _textBoxStoreTotal
            // 
            this._textBoxStoreTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxStoreTotal.Location = new System.Drawing.Point(3, 369);
            this._textBoxStoreTotal.Name = "_textBoxStoreTotal";
            this._textBoxStoreTotal.ReadOnly = true;
            this._textBoxStoreTotal.Size = new System.Drawing.Size(848, 20);
            this._textBoxStoreTotal.TabIndex = 10;
            this._textBoxStoreTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this._listExpenses);
            this.tabPage3.Controls.Add(this._textBoxTotalExpense);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1107, 403);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Расходы";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // _listExpenses
            // 
            this._listExpenses.AllColumns.Add(this.olvColumn6);
            this._listExpenses.AllColumns.Add(this.olvColumn7);
            this._listExpenses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn6,
            this.olvColumn7});
            this._listExpenses.Cursor = System.Windows.Forms.Cursors.Hand;
            this._listExpenses.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listExpenses.FullRowSelect = true;
            this._listExpenses.GridLines = true;
            this._listExpenses.Location = new System.Drawing.Point(3, 3);
            this._listExpenses.MultiSelect = false;
            this._listExpenses.Name = "_listExpenses";
            this._listExpenses.ShowGroups = false;
            this._listExpenses.Size = new System.Drawing.Size(1101, 377);
            this._listExpenses.TabIndex = 6;
            this._listExpenses.UseCompatibleStateImageBehavior = false;
            this._listExpenses.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "Salary";
            this.olvColumn6.CellPadding = null;
            this.olvColumn6.Text = "Кол-во";
            this.olvColumn6.Width = 114;
            // 
            // olvColumn7
            // 
            this.olvColumn7.AspectName = "Comment";
            this.olvColumn7.CellPadding = null;
            this.olvColumn7.Text = "Комментарий";
            this.olvColumn7.Width = 337;
            // 
            // _textBoxTotalExpense
            // 
            this._textBoxTotalExpense.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotalExpense.Location = new System.Drawing.Point(3, 380);
            this._textBoxTotalExpense.Name = "_textBoxTotalExpense";
            this._textBoxTotalExpense.ReadOnly = true;
            this._textBoxTotalExpense.Size = new System.Drawing.Size(1101, 20);
            this._textBoxTotalExpense.TabIndex = 11;
            this._textBoxTotalExpense.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this._listSalaries);
            this.tabPage4.Controls.Add(this._textBoxTotalSalaries);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1107, 403);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Зарплаты";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // _listSalaries
            // 
            this._listSalaries.AllColumns.Add(this.olvColumn12);
            this._listSalaries.AllColumns.Add(this.olvColumn8);
            this._listSalaries.AllColumns.Add(this.olvColumn9);
            this._listSalaries.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn12,
            this.olvColumn8,
            this.olvColumn9});
            this._listSalaries.Cursor = System.Windows.Forms.Cursors.Hand;
            this._listSalaries.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listSalaries.FullRowSelect = true;
            this._listSalaries.GridLines = true;
            this._listSalaries.Location = new System.Drawing.Point(3, 3);
            this._listSalaries.MultiSelect = false;
            this._listSalaries.Name = "_listSalaries";
            this._listSalaries.ShowGroups = false;
            this._listSalaries.Size = new System.Drawing.Size(1101, 377);
            this._listSalaries.TabIndex = 12;
            this._listSalaries.UseCompatibleStateImageBehavior = false;
            this._listSalaries.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn12
            // 
            this.olvColumn12.AspectName = "Employee.Fio";
            this.olvColumn12.CellPadding = null;
            this.olvColumn12.Text = "Сотрудник";
            this.olvColumn12.Width = 158;
            // 
            // olvColumn8
            // 
            this.olvColumn8.AspectName = "Salary";
            this.olvColumn8.CellPadding = null;
            this.olvColumn8.Text = "Кол-во";
            this.olvColumn8.Width = 128;
            // 
            // olvColumn9
            // 
            this.olvColumn9.AspectName = "Comment";
            this.olvColumn9.CellPadding = null;
            this.olvColumn9.Text = "Комментарий";
            this.olvColumn9.Width = 337;
            // 
            // _textBoxTotalSalaries
            // 
            this._textBoxTotalSalaries.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotalSalaries.Location = new System.Drawing.Point(3, 380);
            this._textBoxTotalSalaries.Name = "_textBoxTotalSalaries";
            this._textBoxTotalSalaries.ReadOnly = true;
            this._textBoxTotalSalaries.Size = new System.Drawing.Size(1101, 20);
            this._textBoxTotalSalaries.TabIndex = 13;
            this._textBoxTotalSalaries.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this._listUsers);
            this.tabPage5.Controls.Add(this.groupBox2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1107, 403);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Список сотрудников";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // _listUsers
            // 
            this._listUsers.AllColumns.Add(this.olvColumn13);
            this._listUsers.AllColumns.Add(this.olvColumn15);
            this._listUsers.AllColumns.Add(this._olvColumnRule);
            this._listUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn13,
            this.olvColumn15,
            this._olvColumnRule});
            this._listUsers.Cursor = System.Windows.Forms.Cursors.Hand;
            this._listUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listUsers.FullRowSelect = true;
            this._listUsers.GridLines = true;
            this._listUsers.Location = new System.Drawing.Point(292, 3);
            this._listUsers.MultiSelect = false;
            this._listUsers.Name = "_listUsers";
            this._listUsers.ShowGroups = false;
            this._listUsers.Size = new System.Drawing.Size(812, 397);
            this._listUsers.TabIndex = 15;
            this._listUsers.UseCompatibleStateImageBehavior = false;
            this._listUsers.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn13
            // 
            this.olvColumn13.AspectName = "Fio";
            this.olvColumn13.CellPadding = null;
            this.olvColumn13.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn13.Text = "Ф.И.О.";
            this.olvColumn13.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn13.Width = 196;
            // 
            // olvColumn15
            // 
            this.olvColumn15.AspectName = "NumberPhone";
            this.olvColumn15.CellPadding = null;
            this.olvColumn15.Text = "Телефон";
            this.olvColumn15.Width = 119;
            // 
            // _olvColumnRule
            // 
            this._olvColumnRule.AspectName = "Rule";
            this._olvColumnRule.CellPadding = null;
            this._olvColumnRule.Text = "Позиция";
            this._olvColumnRule.Width = 202;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._comboBoxUsers);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._buttonDelete);
            this.groupBox2.Controls.Add(this._buttonAdd);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(289, 397);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            // 
            // _comboBoxUsers
            // 
            this._comboBoxUsers.Cursor = System.Windows.Forms.Cursors.Hand;
            this._comboBoxUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxUsers.FormattingEnabled = true;
            this._comboBoxUsers.Location = new System.Drawing.Point(8, 35);
            this._comboBoxUsers.Name = "_comboBoxUsers";
            this._comboBoxUsers.Size = new System.Drawing.Size(267, 21);
            this._comboBoxUsers.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Сотрудники";
            // 
            // _buttonAdd
            // 
            this._buttonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonAdd.Location = new System.Drawing.Point(22, 80);
            this._buttonAdd.Name = "_buttonAdd";
            this._buttonAdd.Size = new System.Drawing.Size(115, 48);
            this._buttonAdd.TabIndex = 9;
            this._buttonAdd.Text = "Добавить";
            this._buttonAdd.UseVisualStyleBackColor = true;
            // 
            // _buttonDelete
            // 
            this._buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonDelete.Location = new System.Drawing.Point(159, 80);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(99, 48);
            this._buttonDelete.TabIndex = 11;
            this._buttonDelete.Text = "Удалить";
            this._buttonDelete.UseVisualStyleBackColor = true;
            // 
            // ReportOnDay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 466);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "ReportOnDay";
            this.ShowIcon = false;
            this.Text = "Отчет на текущий день";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listOrders)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listSaleProducts)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listExpenses)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listSalaries)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._listUsers)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private BrightIdeasSoftware.ObjectListView _listSaleProducts;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn10;
        private BrightIdeasSoftware.OLVColumn olvColumn11;
        private System.Windows.Forms.TextBox _textBoxStoreTotal;
        private BrightIdeasSoftware.ObjectListView _listOrders;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private System.Windows.Forms.TextBox _textBoxBathTotal;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.TextBox _textBoxTotal;
        private BrightIdeasSoftware.OLVColumn _olvColumnGender;
        private System.Windows.Forms.TabPage tabPage3;
        private BrightIdeasSoftware.ObjectListView _listExpenses;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private BrightIdeasSoftware.OLVColumn olvColumn7;
        private System.Windows.Forms.TextBox _textBoxTotalExpense;
        private System.Windows.Forms.TabPage tabPage4;
        private BrightIdeasSoftware.ObjectListView _listSalaries;
        private BrightIdeasSoftware.OLVColumn olvColumn12;
        private BrightIdeasSoftware.OLVColumn olvColumn8;
        private BrightIdeasSoftware.OLVColumn olvColumn9;
        private System.Windows.Forms.TextBox _textBoxTotalSalaries;
        private System.Windows.Forms.TabPage tabPage5;
        private BrightIdeasSoftware.ObjectListView _listUsers;
        private BrightIdeasSoftware.OLVColumn olvColumn13;
        private BrightIdeasSoftware.OLVColumn olvColumn15;
        private BrightIdeasSoftware.OLVColumn _olvColumnRule;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox _comboBoxUsers;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.Button _buttonAdd;
    }
}