﻿namespace BathDostukWinForm.Forms
{
    partial class ProductSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this._textBoxEd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._textBoxName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._textBoxBarCode = new System.Windows.Forms.TextBox();
            this._buttonDelete = new System.Windows.Forms.Button();
            this._buttonSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn1);
            this.objectListView1.AllColumns.Add(this.olvColumn2);
            this.objectListView1.AllColumns.Add(this.olvColumn3);
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3});
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(327, 0);
            this.objectListView1.MultiSelect = false;
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(648, 459);
            this.objectListView1.TabIndex = 6;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "BarCode";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Баркод";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 160;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Name";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Наименование";
            this.olvColumn2.Width = 290;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "ED";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.Text = "Ед. измерения";
            this.olvColumn3.Width = 119;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._textBoxEd);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._textBoxName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._textBoxBarCode);
            this.groupBox1.Controls.Add(this._buttonDelete);
            this.groupBox1.Controls.Add(this._buttonSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 459);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ед. измерения";
            // 
            // _textBoxEd
            // 
            this._textBoxEd.Location = new System.Drawing.Point(4, 170);
            this._textBoxEd.Name = "_textBoxEd";
            this._textBoxEd.Size = new System.Drawing.Size(317, 20);
            this._textBoxEd.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Наименование товара";
            // 
            // _textBoxName
            // 
            this._textBoxName.Location = new System.Drawing.Point(4, 112);
            this._textBoxName.Name = "_textBoxName";
            this._textBoxName.Size = new System.Drawing.Size(317, 20);
            this._textBoxName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Баркод";
            // 
            // _textBoxBarCode
            // 
            this._textBoxBarCode.Location = new System.Drawing.Point(4, 49);
            this._textBoxBarCode.Name = "_textBoxBarCode";
            this._textBoxBarCode.Size = new System.Drawing.Size(317, 20);
            this._textBoxBarCode.TabIndex = 1;
            // 
            // _buttonDelete
            // 
            this._buttonDelete.Location = new System.Drawing.Point(175, 229);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(99, 48);
            this._buttonDelete.TabIndex = 5;
            this._buttonDelete.Text = "Удалить";
            this._buttonDelete.UseVisualStyleBackColor = true;
            // 
            // _buttonSave
            // 
            this._buttonSave.Location = new System.Drawing.Point(35, 229);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(99, 48);
            this._buttonSave.TabIndex = 4;
            this._buttonSave.Text = "Добавить";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // ProductSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 459);
            this.Controls.Add(this.objectListView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "ProductSetting";
            this.Text = "Настройка товаров";
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _textBoxBarCode;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _textBoxEd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _textBoxName;
    }
}