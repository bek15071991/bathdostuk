﻿namespace BathDostukWinForm.Forms
{
    partial class Bath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bath));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnGender = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxTotal = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this._textBoxGender = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._textBoxCost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._comboBoxTicket = new System.Windows.Forms.ComboBox();
            this._buttonDelete = new System.Windows.Forms.Button();
            this._buttonAdd = new System.Windows.Forms.Button();
            this._buttonSave = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.objectListView2 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnGender1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn9 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxTotal2 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(993, 490);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(985, 457);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Продажа билетов";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.objectListView1);
            this.panel1.Controls.Add(this._textBoxTotal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(307, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(675, 451);
            this.panel1.TabIndex = 6;
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn1);
            this.objectListView1.AllColumns.Add(this.olvColumn2);
            this.objectListView1.AllColumns.Add(this._olvColumnGender);
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this._olvColumnGender});
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(0, 0);
            this.objectListView1.MultiSelect = false;
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(675, 425);
            this.objectListView1.TabIndex = 9;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Ticket.Name";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Билет";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 253;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Ticket.Cost";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Цена за час";
            this.olvColumn2.Width = 118;
            // 
            // _olvColumnGender
            // 
            this._olvColumnGender.AspectName = "Ticket.Gender";
            this._olvColumnGender.CellPadding = null;
            this._olvColumnGender.Text = "Отделение";
            this._olvColumnGender.Width = 167;
            // 
            // _textBoxTotal
            // 
            this._textBoxTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotal.Location = new System.Drawing.Point(0, 425);
            this._textBoxTotal.Name = "_textBoxTotal";
            this._textBoxTotal.ReadOnly = true;
            this._textBoxTotal.Size = new System.Drawing.Size(675, 26);
            this._textBoxTotal.TabIndex = 7;
            this._textBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._textBoxGender);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._textBoxCost);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._comboBoxTicket);
            this.groupBox1.Controls.Add(this._buttonDelete);
            this.groupBox1.Controls.Add(this._buttonAdd);
            this.groupBox1.Controls.Add(this._buttonSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 451);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Отделение";
            // 
            // _textBoxGender
            // 
            this._textBoxGender.Location = new System.Drawing.Point(16, 184);
            this._textBoxGender.Name = "_textBoxGender";
            this._textBoxGender.ReadOnly = true;
            this._textBoxGender.Size = new System.Drawing.Size(260, 26);
            this._textBoxGender.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Цена за час";
            // 
            // _textBoxCost
            // 
            this._textBoxCost.Location = new System.Drawing.Point(16, 114);
            this._textBoxCost.Name = "_textBoxCost";
            this._textBoxCost.ReadOnly = true;
            this._textBoxCost.Size = new System.Drawing.Size(260, 26);
            this._textBoxCost.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Тип билета";
            // 
            // _comboBoxTicket
            // 
            this._comboBoxTicket.Cursor = System.Windows.Forms.Cursors.Hand;
            this._comboBoxTicket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxTicket.FormattingEnabled = true;
            this._comboBoxTicket.Location = new System.Drawing.Point(16, 46);
            this._comboBoxTicket.Name = "_comboBoxTicket";
            this._comboBoxTicket.Size = new System.Drawing.Size(260, 28);
            this._comboBoxTicket.TabIndex = 2;
            // 
            // _buttonDelete
            // 
            this._buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonDelete.Location = new System.Drawing.Point(153, 244);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(105, 48);
            this._buttonDelete.TabIndex = 4;
            this._buttonDelete.Text = "Удалить";
            this._buttonDelete.UseVisualStyleBackColor = true;
            // 
            // _buttonAdd
            // 
            this._buttonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonAdd.Location = new System.Drawing.Point(22, 244);
            this._buttonAdd.Name = "_buttonAdd";
            this._buttonAdd.Size = new System.Drawing.Size(105, 48);
            this._buttonAdd.TabIndex = 5;
            this._buttonAdd.Text = "Добавить";
            this._buttonAdd.UseVisualStyleBackColor = true;
            // 
            // _buttonSave
            // 
            this._buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSave.Location = new System.Drawing.Point(80, 307);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(120, 48);
            this._buttonSave.TabIndex = 5;
            this._buttonSave.Text = "Сохранить";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.objectListView2);
            this.tabPage2.Controls.Add(this._textBoxTotal2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(985, 457);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Проданные билеты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // objectListView2
            // 
            this.objectListView2.AllColumns.Add(this.olvColumn5);
            this.objectListView2.AllColumns.Add(this.olvColumn6);
            this.objectListView2.AllColumns.Add(this._olvColumnGender1);
            this.objectListView2.AllColumns.Add(this.olvColumn9);
            this.objectListView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn5,
            this.olvColumn6,
            this._olvColumnGender1,
            this.olvColumn9});
            this.objectListView2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView2.FullRowSelect = true;
            this.objectListView2.GridLines = true;
            this.objectListView2.Location = new System.Drawing.Point(3, 3);
            this.objectListView2.MultiSelect = false;
            this.objectListView2.Name = "objectListView2";
            this.objectListView2.ShowGroups = false;
            this.objectListView2.Size = new System.Drawing.Size(979, 425);
            this.objectListView2.TabIndex = 5;
            this.objectListView2.UseCompatibleStateImageBehavior = false;
            this.objectListView2.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "Ticket.Name";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Text = "Билет";
            this.olvColumn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Width = 253;
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "Ticket.Cost";
            this.olvColumn6.CellPadding = null;
            this.olvColumn6.Text = "Цена за час";
            this.olvColumn6.Width = 118;
            // 
            // _olvColumnGender1
            // 
            this._olvColumnGender1.AspectName = "Ticket.Gender";
            this._olvColumnGender1.CellPadding = null;
            this._olvColumnGender1.Text = "Отделение";
            this._olvColumnGender1.Width = 155;
            // 
            // olvColumn9
            // 
            this.olvColumn9.AspectName = "DateTostring";
            this.olvColumn9.CellPadding = null;
            this.olvColumn9.Text = "Дата";
            this.olvColumn9.Width = 216;
            // 
            // _textBoxTotal2
            // 
            this._textBoxTotal2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotal2.Location = new System.Drawing.Point(3, 428);
            this._textBoxTotal2.Name = "_textBoxTotal2";
            this._textBoxTotal2.ReadOnly = true;
            this._textBoxTotal2.Size = new System.Drawing.Size(979, 26);
            this._textBoxTotal2.TabIndex = 8;
            this._textBoxTotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Bath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 490);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Bath";
            this.Text = "Баня";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox _comboBoxTicket;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.Button _buttonAdd;
        private System.Windows.Forms.Panel panel1;
        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private System.Windows.Forms.TextBox _textBoxTotal;
        private BrightIdeasSoftware.ObjectListView objectListView2;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private BrightIdeasSoftware.OLVColumn olvColumn9;
        private System.Windows.Forms.TextBox _textBoxTotal2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _textBoxCost;
        private BrightIdeasSoftware.OLVColumn _olvColumnGender;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _textBoxGender;
        private BrightIdeasSoftware.OLVColumn _olvColumnGender1;
    }
}