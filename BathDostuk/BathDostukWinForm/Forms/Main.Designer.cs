﻿namespace BathDostukWinForm.Forms
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this._tsmBath = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmStorage = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmStore = new System.Windows.Forms.ToolStripMenuItem();
            this.расходToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.записьРасходаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.выдачаЗарплатыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmReportDay = new System.Windows.Forms.ToolStripMenuItem();
            this.наТекущийДеньToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmSettingsAdmin = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.товарыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmManagment = new System.Windows.Forms.ToolStripMenuItem();
            this.пользователиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._tsmStatistik = new System.Windows.Forms.ToolStripMenuItem();
            this.баняToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.складМагазинToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.зарплатыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.общийОтчетToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.общийОтчетToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.оСистемеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.общийОтчетToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsmBath,
            this._tsmStorage,
            this._tsmStore,
            this.расходToolStripMenuItem1,
            this._tsmReportDay,
            this._tsmSettingsAdmin,
            this._tsmManagment,
            this._tsmStatistik,
            this.оСистемеToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(891, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // _tsmBath
            // 
            this._tsmBath.Image = ((System.Drawing.Image)(resources.GetObject("_tsmBath.Image")));
            this._tsmBath.Name = "_tsmBath";
            this._tsmBath.Size = new System.Drawing.Size(61, 20);
            this._tsmBath.Text = "Баня";
            this._tsmBath.Visible = false;
            this._tsmBath.Click += new System.EventHandler(this.еToolStripMenuItem_Click);
            // 
            // _tsmStorage
            // 
            this._tsmStorage.Image = ((System.Drawing.Image)(resources.GetObject("_tsmStorage.Image")));
            this._tsmStorage.Name = "_tsmStorage";
            this._tsmStorage.Size = new System.Drawing.Size(68, 20);
            this._tsmStorage.Text = "Склад";
            this._tsmStorage.Visible = false;
            this._tsmStorage.Click += new System.EventHandler(this.sdfdfdfdfToolStripMenuItem_Click);
            // 
            // _tsmStore
            // 
            this._tsmStore.Image = ((System.Drawing.Image)(resources.GetObject("_tsmStore.Image")));
            this._tsmStore.Name = "_tsmStore";
            this._tsmStore.Size = new System.Drawing.Size(82, 20);
            this._tsmStore.Text = "Магазин";
            this._tsmStore.Visible = false;
            this._tsmStore.Click += new System.EventHandler(this.sdfsdfToolStripMenuItem_Click);
            // 
            // расходToolStripMenuItem1
            // 
            this.расходToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.записьРасходаToolStripMenuItem1,
            this.выдачаЗарплатыToolStripMenuItem1});
            this.расходToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("расходToolStripMenuItem1.Image")));
            this.расходToolStripMenuItem1.Name = "расходToolStripMenuItem1";
            this.расходToolStripMenuItem1.Size = new System.Drawing.Size(72, 20);
            this.расходToolStripMenuItem1.Text = "Расход";
            this.расходToolStripMenuItem1.Visible = false;
            // 
            // записьРасходаToolStripMenuItem1
            // 
            this.записьРасходаToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("записьРасходаToolStripMenuItem1.Image")));
            this.записьРасходаToolStripMenuItem1.Name = "записьРасходаToolStripMenuItem1";
            this.записьРасходаToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.записьРасходаToolStripMenuItem1.Text = "Запись расхода";
            this.записьРасходаToolStripMenuItem1.Click += new System.EventHandler(this.записьРасходаToolStripMenuItem1_Click);
            // 
            // выдачаЗарплатыToolStripMenuItem1
            // 
            this.выдачаЗарплатыToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("выдачаЗарплатыToolStripMenuItem1.Image")));
            this.выдачаЗарплатыToolStripMenuItem1.Name = "выдачаЗарплатыToolStripMenuItem1";
            this.выдачаЗарплатыToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.выдачаЗарплатыToolStripMenuItem1.Text = "Выдача зарплаты";
            this.выдачаЗарплатыToolStripMenuItem1.Click += new System.EventHandler(this.выдачаЗарплатыToolStripMenuItem1_Click);
            // 
            // _tsmReportDay
            // 
            this._tsmReportDay.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.наТекущийДеньToolStripMenuItem});
            this._tsmReportDay.Image = ((System.Drawing.Image)(resources.GetObject("_tsmReportDay.Image")));
            this._tsmReportDay.Name = "_tsmReportDay";
            this._tsmReportDay.Size = new System.Drawing.Size(67, 20);
            this._tsmReportDay.Text = "Отчет";
            this._tsmReportDay.Visible = false;
            // 
            // наТекущийДеньToolStripMenuItem
            // 
            this.наТекущийДеньToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("наТекущийДеньToolStripMenuItem.Image")));
            this.наТекущийДеньToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.наТекущийДеньToolStripMenuItem.Name = "наТекущийДеньToolStripMenuItem";
            this.наТекущийДеньToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.наТекущийДеньToolStripMenuItem.Text = "На текущий день";
            this.наТекущийДеньToolStripMenuItem.Click += new System.EventHandler(this.наТекущийДеньToolStripMenuItem_Click);
            // 
            // _tsmSettingsAdmin
            // 
            this._tsmSettingsAdmin.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.настройкаToolStripMenuItem,
            this.товарыToolStripMenuItem});
            this._tsmSettingsAdmin.Image = ((System.Drawing.Image)(resources.GetObject("_tsmSettingsAdmin.Image")));
            this._tsmSettingsAdmin.Name = "_tsmSettingsAdmin";
            this._tsmSettingsAdmin.Size = new System.Drawing.Size(94, 20);
            this._tsmSettingsAdmin.Text = "Настройка";
            // 
            // настройкаToolStripMenuItem
            // 
            this.настройкаToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("настройкаToolStripMenuItem.Image")));
            this.настройкаToolStripMenuItem.Name = "настройкаToolStripMenuItem";
            this.настройкаToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.настройкаToolStripMenuItem.Text = "Билеты";
            this.настройкаToolStripMenuItem.Visible = false;
            this.настройкаToolStripMenuItem.Click += new System.EventHandler(this.настройкаToolStripMenuItem_Click);
            // 
            // товарыToolStripMenuItem
            // 
            this.товарыToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("товарыToolStripMenuItem.Image")));
            this.товарыToolStripMenuItem.Name = "товарыToolStripMenuItem";
            this.товарыToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.товарыToolStripMenuItem.Text = "Товары";
            this.товарыToolStripMenuItem.Click += new System.EventHandler(this.товарыToolStripMenuItem_Click);
            // 
            // _tsmManagment
            // 
            this._tsmManagment.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.пользователиToolStripMenuItem});
            this._tsmManagment.Image = ((System.Drawing.Image)(resources.GetObject("_tsmManagment.Image")));
            this._tsmManagment.Name = "_tsmManagment";
            this._tsmManagment.Size = new System.Drawing.Size(101, 20);
            this._tsmManagment.Text = "Управление";
            this._tsmManagment.Visible = false;
            // 
            // пользователиToolStripMenuItem
            // 
            this.пользователиToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("пользователиToolStripMenuItem.Image")));
            this.пользователиToolStripMenuItem.Name = "пользователиToolStripMenuItem";
            this.пользователиToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.пользователиToolStripMenuItem.Text = "Пользователи";
            this.пользователиToolStripMenuItem.Click += new System.EventHandler(this.пользователиToolStripMenuItem_Click);
            // 
            // _tsmStatistik
            // 
            this._tsmStatistik.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.баняToolStripMenuItem1,
            this.складМагазинToolStripMenuItem,
            this.зарплатыToolStripMenuItem1,
            this.общийОтчетToolStripMenuItem,
            this.общийОтчетToolStripMenuItem1,
            this.общийОтчетToolStripMenuItem2});
            this._tsmStatistik.Image = ((System.Drawing.Image)(resources.GetObject("_tsmStatistik.Image")));
            this._tsmStatistik.Name = "_tsmStatistik";
            this._tsmStatistik.Size = new System.Drawing.Size(96, 20);
            this._tsmStatistik.Text = "Статистика";
            this._tsmStatistik.Visible = false;
            // 
            // баняToolStripMenuItem1
            // 
            this.баняToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("баняToolStripMenuItem1.Image")));
            this.баняToolStripMenuItem1.Name = "баняToolStripMenuItem1";
            this.баняToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.баняToolStripMenuItem1.Text = "Баня";
            this.баняToolStripMenuItem1.Click += new System.EventHandler(this.баняToolStripMenuItem1_Click);
            // 
            // складМагазинToolStripMenuItem
            // 
            this.складМагазинToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("складМагазинToolStripMenuItem.Image")));
            this.складМагазинToolStripMenuItem.Name = "складМагазинToolStripMenuItem";
            this.складМагазинToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.складМагазинToolStripMenuItem.Text = "Склад/Магазин";
            this.складМагазинToolStripMenuItem.Click += new System.EventHandler(this.складМагазинToolStripMenuItem_Click);
            // 
            // зарплатыToolStripMenuItem1
            // 
            this.зарплатыToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("зарплатыToolStripMenuItem1.Image")));
            this.зарплатыToolStripMenuItem1.Name = "зарплатыToolStripMenuItem1";
            this.зарплатыToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
            this.зарплатыToolStripMenuItem1.Text = "Зарплаты";
            this.зарплатыToolStripMenuItem1.Click += new System.EventHandler(this.зарплатыToolStripMenuItem1_Click);
            // 
            // общийОтчетToolStripMenuItem
            // 
            this.общийОтчетToolStripMenuItem.Name = "общийОтчетToolStripMenuItem";
            this.общийОтчетToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.общийОтчетToolStripMenuItem.Text = "Расходы";
            this.общийОтчетToolStripMenuItem.Click += new System.EventHandler(this.общийОтчетToolStripMenuItem_Click);
            // 
            // общийОтчетToolStripMenuItem1
            // 
            this.общийОтчетToolStripMenuItem1.Name = "общийОтчетToolStripMenuItem1";
            this.общийОтчетToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.общийОтчетToolStripMenuItem1.Text = "Отчет по сотрудникам";
            this.общийОтчетToolStripMenuItem1.Click += new System.EventHandler(this.общийОтчетToolStripMenuItem1_Click);
            // 
            // оСистемеToolStripMenuItem
            // 
            this.оСистемеToolStripMenuItem.Name = "оСистемеToolStripMenuItem";
            this.оСистемеToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.оСистемеToolStripMenuItem.Text = "О системе";
            this.оСистемеToolStripMenuItem.Click += new System.EventHandler(this.оСистемеToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel4});
            this.statusStrip1.Location = new System.Drawing.Point(0, 460);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(891, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel2.Text = "toolStripStatusLabel2";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel3.Text = "toolStripStatusLabel3";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel4.Text = "toolStripStatusLabel4";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // общийОтчетToolStripMenuItem2
            // 
            this.общийОтчетToolStripMenuItem2.Name = "общийОтчетToolStripMenuItem2";
            this.общийОтчетToolStripMenuItem2.Size = new System.Drawing.Size(159, 22);
            this.общийОтчетToolStripMenuItem2.Text = "Общий отчет";
            this.общийОтчетToolStripMenuItem2.Click += new System.EventHandler(this.общийОтчетToolStripMenuItem2_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(891, 482);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Баня    \"Достук\"";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem _tsmBath;
        private System.Windows.Forms.ToolStripMenuItem _tsmStorage;
        private System.Windows.Forms.ToolStripMenuItem _tsmStore;
        private System.Windows.Forms.ToolStripMenuItem _tsmReportDay;
        private System.Windows.Forms.ToolStripMenuItem _tsmSettingsAdmin;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem _tsmManagment;
        private System.Windows.Forms.ToolStripMenuItem пользователиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem наТекущийДеньToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripMenuItem _tsmStatistik;
        private System.Windows.Forms.ToolStripMenuItem баняToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem складМагазинToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem зарплатыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStripMenuItem общийОтчетToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem общийОтчетToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem оСистемеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem расходToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem записьРасходаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem товарыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выдачаЗарплатыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem общийОтчетToolStripMenuItem2;
    }
}