﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class ReportProduct : Form, IViewReportProduct
    {
        public ReportProduct()
        {
            InitializeComponent();
            _buttonSearchStorage.Click += (sender, args) =>
            {
                SearchStorage(this, EventArgs.Empty);
            };

            _buttonSearchStore.Click += (sender, args) =>
            {
                SearchStore(this, EventArgs.Empty);
            };
        }

        public DateTime StoreFrom
        {
            get { return _dtpStoreFrom.Value.Date; }
        }

        public DateTime StoreTo
        {
            get { return _dtpStoreTo.Value.Date; }
        }

        public DateTime StorageFrom
        {
            get { return _dtpFrom.Value.Date; }
        }

        public DateTime StorageTo
        {
            get { return _dtpTo.Value.Date; }
        }

        public Employee SelectedStoreEmployee
        {
            get { return _comboBoxUserStore.SelectedItem as Employee; }
        }

        public Employee SelectedStoreageEmployee
        {
            get { return _comboBoxUserStorage.SelectedItem as Employee; }
        }

        public void ShowStorage(List<BuyProduct> buyProducts)
        {
            _listBuyProducts.SetObjects(buyProducts);
        }

        public void ShowStore(List<SaleProduct> saleProducts)
        {
            _listSaleProduct.SetObjects(saleProducts);
        }

        public void ShowUsers(List<Employee> employees)
        {
            var user = new Employee { Fio = "(Все)" };
            employees.Add(user);
            _comboBoxUserStore.DataSource =
                _comboBoxUserStorage.DataSource = employees;
            _comboBoxUserStore.DisplayMember =
                _comboBoxUserStorage.DisplayMember = "Fio";
            _comboBoxUserStore.SelectedItem =
                _comboBoxUserStorage.SelectedItem = user;
        }

        public event EventHandler<EventArgs> SearchStore;
        public event EventHandler<EventArgs> SearchStorage;
        public void TotalStore(double t, double pribal)
        {
            _textBoxTotalStore.Text = string.Format("Итого :   {0} сом.            Прибыль :   {1} сом.", t, pribal);
        }

        public void TotalStorage(double t)
        {
            _textBoxTotalStorage.Text = string.Format("Итого :   {0} сом.", t);
        }
    }
}
