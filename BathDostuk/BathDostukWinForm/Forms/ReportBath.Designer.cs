﻿namespace BathDostukWinForm.Forms
{
    partial class ReportBath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._comboBoxUser = new System.Windows.Forms.ComboBox();
            this._buttonSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._dtpTo = new System.Windows.Forms.DateTimePicker();
            this._dtpFrom = new System.Windows.Forms.DateTimePicker();
            this._textBoxTotal2 = new System.Windows.Forms.TextBox();
            this.objectListView2 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn9 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnGender = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._comboBoxUser);
            this.groupBox2.Controls.Add(this._buttonSearch);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._dtpTo);
            this.groupBox2.Controls.Add(this._dtpFrom);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 483);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск";
            // 
            // _comboBoxUser
            // 
            this._comboBoxUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxUser.FormattingEnabled = true;
            this._comboBoxUser.Location = new System.Drawing.Point(26, 140);
            this._comboBoxUser.Name = "_comboBoxUser";
            this._comboBoxUser.Size = new System.Drawing.Size(199, 21);
            this._comboBoxUser.TabIndex = 3;
            // 
            // _buttonSearch
            // 
            this._buttonSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSearch.Location = new System.Drawing.Point(62, 186);
            this._buttonSearch.Name = "_buttonSearch";
            this._buttonSearch.Size = new System.Drawing.Size(117, 32);
            this._buttonSearch.TabIndex = 2;
            this._buttonSearch.Text = "Поиск";
            this._buttonSearch.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Сотрудник";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "по";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "C";
            // 
            // _dtpTo
            // 
            this._dtpTo.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpTo.Location = new System.Drawing.Point(53, 68);
            this._dtpTo.Name = "_dtpTo";
            this._dtpTo.Size = new System.Drawing.Size(153, 20);
            this._dtpTo.TabIndex = 0;
            // 
            // _dtpFrom
            // 
            this._dtpFrom.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpFrom.Location = new System.Drawing.Point(53, 24);
            this._dtpFrom.Name = "_dtpFrom";
            this._dtpFrom.Size = new System.Drawing.Size(153, 20);
            this._dtpFrom.TabIndex = 0;
            // 
            // _textBoxTotal2
            // 
            this._textBoxTotal2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotal2.Location = new System.Drawing.Point(256, 463);
            this._textBoxTotal2.Name = "_textBoxTotal2";
            this._textBoxTotal2.ReadOnly = true;
            this._textBoxTotal2.Size = new System.Drawing.Size(1011, 20);
            this._textBoxTotal2.TabIndex = 12;
            this._textBoxTotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // objectListView2
            // 
            this.objectListView2.AllColumns.Add(this.olvColumn5);
            this.objectListView2.AllColumns.Add(this.olvColumn6);
            this.objectListView2.AllColumns.Add(this.olvColumn9);
            this.objectListView2.AllColumns.Add(this.olvColumn1);
            this.objectListView2.AllColumns.Add(this._olvColumnGender);
            this.objectListView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn5,
            this.olvColumn6,
            this.olvColumn9,
            this.olvColumn1,
            this._olvColumnGender});
            this.objectListView2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView2.FullRowSelect = true;
            this.objectListView2.GridLines = true;
            this.objectListView2.Location = new System.Drawing.Point(256, 0);
            this.objectListView2.MultiSelect = false;
            this.objectListView2.Name = "objectListView2";
            this.objectListView2.ShowGroups = false;
            this.objectListView2.Size = new System.Drawing.Size(1011, 463);
            this.objectListView2.TabIndex = 15;
            this.objectListView2.UseCompatibleStateImageBehavior = false;
            this.objectListView2.View = System.Windows.Forms.View.Details;
            this.objectListView2.SelectedIndexChanged += new System.EventHandler(this.objectListView2_SelectedIndexChanged);
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "Ticket.Name";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Text = "Билет";
            this.olvColumn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Width = 265;
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "Ticket.Cost";
            this.olvColumn6.CellPadding = null;
            this.olvColumn6.Text = "Цена за час";
            this.olvColumn6.Width = 104;
            // 
            // olvColumn9
            // 
            this.olvColumn9.AspectName = "DateTostring";
            this.olvColumn9.CellPadding = null;
            this.olvColumn9.Text = "Дата";
            this.olvColumn9.Width = 166;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Cassier.Fio";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.Text = "Сотрудник";
            this.olvColumn1.Width = 179;
            // 
            // _olvColumnGender
            // 
            this._olvColumnGender.AspectName = "Ticket.Gender";
            this._olvColumnGender.CellPadding = null;
            this._olvColumnGender.Text = "Отделение";
            // 
            // ReportBath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 483);
            this.Controls.Add(this.objectListView2);
            this.Controls.Add(this._textBoxTotal2);
            this.Controls.Add(this.groupBox2);
            this.Name = "ReportBath";
            this.Text = "Отчет по проданным билетам";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _buttonSearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker _dtpTo;
        private System.Windows.Forms.DateTimePicker _dtpFrom;
        private System.Windows.Forms.TextBox _textBoxTotal2;
        private System.Windows.Forms.ComboBox _comboBoxUser;
        private System.Windows.Forms.Label label1;
        private BrightIdeasSoftware.ObjectListView objectListView2;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private BrightIdeasSoftware.OLVColumn olvColumn9;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn _olvColumnGender;
    }
}