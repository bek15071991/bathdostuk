﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class ReportExpenses : Form, IViewReportExpenses
    {
        public ReportExpenses()
        {
            InitializeComponent(); 
            _buttonSearch.Click += (sender, args) =>
            {
                Search(this, EventArgs.Empty);
            };
        }

        public DateTime From
        {
            get { return _dtpFrom.Value.Date; }
        }

        public DateTime To
        {
            get { return _dtpTo.Value.Date; }
        }
       

        public void ShowTotal(double total)
        {
            _textBoxTotal2.Text = string.Format("Итого :   {0} сом.", total);
        }

        public event EventHandler<EventArgs> Search;
        public void ShowExpense(IEnumerable<Expense> list)
        {
            objectListView1.SetObjects(list);
        }
    }
}
