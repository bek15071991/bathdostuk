﻿using System.Windows.Forms;

namespace BathDostukWinForm.Forms
{
    public partial class Security : Form
    {
        public Security()
        {
            InitializeComponent();
            _buttonOk.Click += (sender, args) =>
            {
                if (string.IsNullOrEmpty(_textBoxPassword.Text)) return;
                 DialogResult = DialogResult.Yes; 
            };
        }



        public string Password
        {
            get { return _textBoxPassword.Text; }
        }
    }
}
