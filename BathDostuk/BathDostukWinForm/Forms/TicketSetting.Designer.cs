﻿namespace BathDostukWinForm.Forms
{
    partial class TicketSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnGender = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnIsDelete = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this._comboBoxGender = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this._textBoxCost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._textBoxName = new System.Windows.Forms.TextBox();
            this._buttonDelete = new System.Windows.Forms.Button();
            this._buttonSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn1);
            this.objectListView1.AllColumns.Add(this.olvColumn2);
            this.objectListView1.AllColumns.Add(this._olvColumnGender);
            this.objectListView1.AllColumns.Add(this._olvColumnIsDelete);
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this._olvColumnGender,
            this._olvColumnIsDelete});
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(268, 0);
            this.objectListView1.MultiSelect = false;
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(608, 479);
            this.objectListView1.TabIndex = 5;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Name";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Тип билета";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 366;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Cost";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Стоимость";
            this.olvColumn2.Width = 93;
            // 
            // _olvColumnGender
            // 
            this._olvColumnGender.AspectName = "Gender";
            this._olvColumnGender.CellPadding = null;
            this._olvColumnGender.Text = "Отделение";
            this._olvColumnGender.Width = 80;
            // 
            // _olvColumnIsDelete
            // 
            this._olvColumnIsDelete.AspectName = "Delete";
            this._olvColumnIsDelete.CellPadding = null;
            this._olvColumnIsDelete.Text = "Состояние";
            this._olvColumnIsDelete.Width = 92;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._comboBoxGender);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._textBoxCost);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._textBoxName);
            this.groupBox1.Controls.Add(this._buttonDelete);
            this.groupBox1.Controls.Add(this._buttonSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 479);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(96, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Отделение";
            // 
            // _comboBoxGender
            // 
            this._comboBoxGender.Cursor = System.Windows.Forms.Cursors.Hand;
            this._comboBoxGender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxGender.FormattingEnabled = true;
            this._comboBoxGender.Items.AddRange(new object[] {
            "Мужское",
            "Женское",
            "Семейнное"});
            this._comboBoxGender.Location = new System.Drawing.Point(56, 165);
            this._comboBoxGender.Name = "_comboBoxGender";
            this._comboBoxGender.Size = new System.Drawing.Size(149, 21);
            this._comboBoxGender.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Стоимость за час";
            // 
            // _textBoxCost
            // 
            this._textBoxCost.Location = new System.Drawing.Point(4, 112);
            this._textBoxCost.Name = "_textBoxCost";
            this._textBoxCost.Size = new System.Drawing.Size(258, 20);
            this._textBoxCost.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Тип билета";
            // 
            // _textBoxName
            // 
            this._textBoxName.Location = new System.Drawing.Point(4, 49);
            this._textBoxName.Name = "_textBoxName";
            this._textBoxName.Size = new System.Drawing.Size(258, 20);
            this._textBoxName.TabIndex = 1;
            // 
            // _buttonDelete
            // 
            this._buttonDelete.Location = new System.Drawing.Point(142, 215);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(99, 48);
            this._buttonDelete.TabIndex = 5;
            this._buttonDelete.Text = "Удалить";
            this._buttonDelete.UseVisualStyleBackColor = true;
            // 
            // _buttonSave
            // 
            this._buttonSave.Location = new System.Drawing.Point(19, 215);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(99, 48);
            this._buttonSave.TabIndex = 4;
            this._buttonSave.Text = "Сохранить";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // TicketSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 479);
            this.Controls.Add(this.objectListView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "TicketSetting";
            this.Text = "Настройка билетов";
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn _olvColumnIsDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _textBoxCost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _textBoxName;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox _comboBoxGender;
        private BrightIdeasSoftware.OLVColumn _olvColumnGender;
    }
}