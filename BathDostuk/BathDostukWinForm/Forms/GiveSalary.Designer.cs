﻿namespace BathDostukWinForm.Forms
{
    partial class GiveSalary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._textBoxComment = new System.Windows.Forms.TextBox();
            this._buttonDelete = new System.Windows.Forms.Button();
            this._buttonSave = new System.Windows.Forms.Button();
            this._textBoxSalary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._comboBoxEmploye = new System.Windows.Forms.ComboBox();
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn11 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._textBoxComment);
            this.groupBox1.Controls.Add(this._buttonDelete);
            this.groupBox1.Controls.Add(this._buttonSave);
            this.groupBox1.Controls.Add(this._textBoxSalary);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._comboBoxEmploye);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 487);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // _textBoxComment
            // 
            this._textBoxComment.Location = new System.Drawing.Point(23, 156);
            this._textBoxComment.Multiline = true;
            this._textBoxComment.Name = "_textBoxComment";
            this._textBoxComment.Size = new System.Drawing.Size(304, 136);
            this._textBoxComment.TabIndex = 3;
            // 
            // _buttonDelete
            // 
            this._buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonDelete.Location = new System.Drawing.Point(176, 315);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(112, 41);
            this._buttonDelete.TabIndex = 5;
            this._buttonDelete.Text = "Удалить";
            this._buttonDelete.UseVisualStyleBackColor = true;
            // 
            // _buttonSave
            // 
            this._buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSave.Location = new System.Drawing.Point(53, 315);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(112, 41);
            this._buttonSave.TabIndex = 4;
            this._buttonSave.Text = "Созранить";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // _textBoxSalary
            // 
            this._textBoxSalary.Location = new System.Drawing.Point(23, 101);
            this._textBoxSalary.Name = "_textBoxSalary";
            this._textBoxSalary.Size = new System.Drawing.Size(304, 20);
            this._textBoxSalary.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Комментарий";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Зарплата";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Сотрудник";
            // 
            // _comboBoxEmploye
            // 
            this._comboBoxEmploye.Cursor = System.Windows.Forms.Cursors.Hand;
            this._comboBoxEmploye.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxEmploye.FormattingEnabled = true;
            this._comboBoxEmploye.Location = new System.Drawing.Point(23, 43);
            this._comboBoxEmploye.Name = "_comboBoxEmploye";
            this._comboBoxEmploye.Size = new System.Drawing.Size(304, 21);
            this._comboBoxEmploye.TabIndex = 1;
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn1);
            this.objectListView1.AllColumns.Add(this.olvColumn2);
            this.objectListView1.AllColumns.Add(this.olvColumn3);
            this.objectListView1.AllColumns.Add(this.olvColumn11);
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn11});
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(350, 0);
            this.objectListView1.MultiSelect = false;
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(777, 487);
            this.objectListView1.TabIndex = 6;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Employee.Fio";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Сотрудник";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 253;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Salary";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Выдано зарплаты";
            this.olvColumn2.Width = 107;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "Comment";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.Text = "Комментарий";
            this.olvColumn3.Width = 232;
            // 
            // olvColumn11
            // 
            this.olvColumn11.AspectName = "Date";
            this.olvColumn11.CellPadding = null;
            this.olvColumn11.Text = "Дата";
            this.olvColumn11.Width = 153;
            // 
            // GiveSalary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 487);
            this.Controls.Add(this.objectListView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "GiveSalary";
            this.Text = "Выдача зарплаты";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.TextBox _textBoxSalary;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _comboBoxEmploye;
        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn11;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private System.Windows.Forms.TextBox _textBoxComment;
        private System.Windows.Forms.Label label3;
    }
}