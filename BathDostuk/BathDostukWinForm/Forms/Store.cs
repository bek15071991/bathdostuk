﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class Store : Form, IViewStore
    {
        private List<BuyProduct> _buyProducts;


        public Store()
        {
            InitializeComponent();
            _buttonAdd.Click += (sender, args) => { Add(this, EventArgs.Empty); };
            _buttonDelete.Click += (sender, args) =>
            {
                Delete(this, EventArgs.Empty);
            };
            _buttonSave.Click += (sender, args) =>
            {
                Save(this, EventArgs.Empty);
            };
            _buttonSearch.Click += (sender, args) => { Searche(this, EventArgs.Empty); };
            _buttonShowAll.Click += (sender, args) => { ShowAll(this, EventArgs.Empty); };

            _textBoxCount.TextChanged += TextBoxCountOnTextChanged;

            _textBoxCount.KeyPress += TextBoxCostOnKeyPress;
            _comboBoxBarCode.KeyPress += TextBoxCostOnKeyPress;
            SaleProducts = new List<SaleProduct>();
            SaleProductsInStorage = new List<SaleProduct>();

             
            _comboBoxProduct.SelectedIndexChanged += (sender, args) =>
            {
                if (_comboBoxProduct.SelectedIndex == -1 || _comboBoxProduct.SelectedValue == null ||
                  !(_comboBoxProduct.SelectedValue is int)) return;

                var id = _comboBoxProduct.SelectedValue;

                var buyProduct = _buyProducts.FirstOrDefault(bp => bp.Id == Convert.ToInt32(id));

                InStorageBuyProduct = buyProduct;


                if (GetInStorage != null) GetInStorage(this, EventArgs.Empty);

            };
        }


        private void TextBoxCostOnKeyPress(object sender, KeyPressEventArgs keyPressEventArgs)
        {
            if (char.IsDigit(keyPressEventArgs.KeyChar) ||
                keyPressEventArgs.KeyChar == (char)8 ||
                keyPressEventArgs.KeyChar == ',')
                keyPressEventArgs.Handled = false;
            else keyPressEventArgs.Handled = true;
        }

        private void TextBoxCountOnTextChanged(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(_textBoxPrice.Text) ||
                 string.IsNullOrEmpty(_textBoxCount.Text))
                return;
            var amotun = Convert.ToDouble(_textBoxPrice.Text) * Convert.ToDouble(_textBoxCount.Text);
            _textBoxAmount.Text = amotun.ToString();
        }




        public void ShowBuyProducts(List<BuyProduct> byProducts)
        { 
            _buyProducts = byProducts;

            var list = from bp in byProducts
                       select new
                       {
                           bp.Id,
                           bp.Product.Name,
                           bp.Product.BarCode
                       };


            _comboBoxProduct.DataSource =
            _comboBoxBarCode.DataSource = list.ToList(); ;

            _comboBoxProduct.DisplayMember = "Name";
            _comboBoxBarCode.DisplayMember = "BarCode";

            _comboBoxBarCode.ValueMember =
            _comboBoxProduct.ValueMember = "Id";

            _comboBoxBarCode.SelectedIndex =
            _comboBoxProduct.SelectedIndex = -1;
        }

        public BuyProduct SelectedBuyProduct
        {
            get
            {
                if (_comboBoxProduct.SelectedIndex != -1)
                {
                    var id = _comboBoxProduct.SelectedValue;
                    return _buyProducts.FirstOrDefault(bp => bp.Id == Convert.ToInt32(id));
                }
                return null;

            }
            set
            {
                if (value == null)
                    Price =
                        CountInStorage=
                        Price = 0;
                _textBoxAmount.Text = "0";
                _comboBoxBarCode.SelectedIndex =
                _comboBoxProduct.SelectedIndex = -1;
            }
        }

        public BuyProduct InStorageBuyProduct { get; set; }

        public float CountInStorage
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxInStorage.Text))
                    return 0;
                return (float)Convert.ToDouble(_textBoxInStorage.Text);
            }
            set { _textBoxInStorage.Text = value.ToString(); }
        }

        public float Price
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxPrice.Text))
                    return 0;
                return (float)Convert.ToDouble(_textBoxPrice.Text);
            }
            set { _textBoxPrice.Text = value.ToString(); }
        }

        public float Count
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxCount.Text))
                    return 0;
                return (float)Convert.ToDouble(_textBoxCount.Text);
            }
            set { _textBoxCount.Text = value.ToString(); }
        }

        public List<SaleProduct> SaleProducts
        {
            get { return objectListView1.Objects as List<SaleProduct>; }
            set
            {
                var amount = value == null ? 0 : value.Sum(bp => bp.Total);
                _textBoxTotal.Text = "Итого: " + amount + " сом.";
                objectListView1.Objects = value;
            }
        }

        public SaleProduct SelectedSaleProduct
        {
            get { return objectListView1.SelectedObject as SaleProduct; }
            set { objectListView1.SelectedObject = value; }
        }

        public List<SaleProduct> SaleProductsInStorage
        {
            get { return objectListView2.Objects as List<SaleProduct>; }
            set
            {
                var amount = value == null ? 0 : value.Sum(bp => bp.Total);
                _textBoxTotal2.Text = "Итого: " + amount + " сом.";
                objectListView2.Objects = value;
            }
        }

        public DateTime From
        {
            get { return _dtpFrom.Value; }
            set { _dtpFrom.Value = value; }
        }

        public DateTime To
        {
            get { return _dtpTo.Value; }
            set { _dtpTo.Value = value; }
        }

        public event EventHandler<EventArgs> Add;
        public event EventHandler<EventArgs> Delete;
        public event EventHandler<EventArgs> Save;
        public event EventHandler<EventArgs> Searche;
        public event EventHandler<EventArgs> ShowAll;
        public event EventHandler<EventArgs> GetInStorage;
    }
}
