﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class GeneralReport : Form, IViewGeneralReport
    { 

        public GeneralReport()
        {
            InitializeComponent();
            _buttonSearch.Click += (sender, args) =>
            {
                Search(this, EventArgs.Empty);
            };
        }

        public DateTime From
        {
            get { return _dtpFrom.Value.Date; }
        }

        public DateTime To
        {
            get { return _dtpTo.Value.Date; }
        }

        public void ShowUsers(List<Employee> employees)
        {
            var user = new Employee
            {
                Fio = "(все)"
            };
            employees.Add(user);
            _comboBoxUser.DataSource = employees;
            _comboBoxUser.DisplayMember = "Fio";
            _comboBoxUser.SelectedItem = user;
        }



        public Employee SeletcteEmployee
        {
            get { return _comboBoxUser.SelectedItem as Employee; }
            set
            {
                _comboBoxUser.SelectedItem = value;
            }
        }

        public event EventHandler<EventArgs> Search;

        public void ShowReport(Employee user, double bath, double storeProfit, double store,    double profit, double? expense,double total)
        {

            objectListView2.AddObject(new
            {
                Use = user.Fio,
                BathCount = bath,
                StoreProfitCount = storeProfit,
                StoreCount = store,  
                Profit = profit,
                Expense = expense,
                Total = total
            });
        }

        public void ClearList()
        {
            objectListView2.ClearObjects();
        }
    }
}
