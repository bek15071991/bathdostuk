﻿namespace BathDostukWinForm.Forms
{
    partial class Store
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn10 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn11 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxTotal = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._comboBoxProduct = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._textBoxAmount = new System.Windows.Forms.TextBox();
            this._textBoxInStorage = new System.Windows.Forms.TextBox();
            this._textBoxCount = new System.Windows.Forms.TextBox();
            this._textBoxPrice = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._comboBoxBarCode = new System.Windows.Forms.ComboBox();
            this._buttonDelete = new System.Windows.Forms.Button();
            this._buttonAdd = new System.Windows.Forms.Button();
            this._buttonSave = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.objectListView2 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn8 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn9 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn12 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn13 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._buttonShowAll = new System.Windows.Forms.Button();
            this._buttonSearch = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._dtpTo = new System.Windows.Forms.DateTimePicker();
            this._dtpFrom = new System.Windows.Forms.DateTimePicker();
            this._textBoxTotal2 = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1114, 591);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1106, 558);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Продажа товаров";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.objectListView1);
            this.panel1.Controls.Add(this._textBoxTotal);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(357, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(746, 552);
            this.panel1.TabIndex = 6;
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn1);
            this.objectListView1.AllColumns.Add(this.olvColumn2);
            this.objectListView1.AllColumns.Add(this.olvColumn4);
            this.objectListView1.AllColumns.Add(this.olvColumn10);
            this.objectListView1.AllColumns.Add(this.olvColumn11);
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn4,
            this.olvColumn10,
            this.olvColumn11});
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(0, 0);
            this.objectListView1.MultiSelect = false;
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(746, 526);
            this.objectListView1.TabIndex = 9;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "BuyProduct.Product.Name";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Товар";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 253;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "BuyProduct.Product.BarCode";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Баркод";
            this.olvColumn2.Width = 123;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "BuyProduct.Price";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.Text = "Цена на продажу";
            this.olvColumn4.Width = 146;
            // 
            // olvColumn10
            // 
            this.olvColumn10.AspectName = "Count";
            this.olvColumn10.CellPadding = null;
            this.olvColumn10.Text = "Кол-во";
            this.olvColumn10.Width = 88;
            // 
            // olvColumn11
            // 
            this.olvColumn11.AspectName = "Total";
            this.olvColumn11.CellPadding = null;
            this.olvColumn11.Text = "Сумма";
            this.olvColumn11.Width = 85;
            // 
            // _textBoxTotal
            // 
            this._textBoxTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotal.Location = new System.Drawing.Point(0, 526);
            this._textBoxTotal.Name = "_textBoxTotal";
            this._textBoxTotal.ReadOnly = true;
            this._textBoxTotal.Size = new System.Drawing.Size(746, 26);
            this._textBoxTotal.TabIndex = 7;
            this._textBoxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._comboBoxProduct);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._textBoxAmount);
            this.groupBox1.Controls.Add(this._textBoxInStorage);
            this.groupBox1.Controls.Add(this._textBoxCount);
            this.groupBox1.Controls.Add(this._textBoxPrice);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._comboBoxBarCode);
            this.groupBox1.Controls.Add(this._buttonDelete);
            this.groupBox1.Controls.Add(this._buttonAdd);
            this.groupBox1.Controls.Add(this._buttonSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 552);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 351);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Сумма";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 285);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Количество";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Товар";
            // 
            // _comboBoxProduct
            // 
            this._comboBoxProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this._comboBoxProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxProduct.FormattingEnabled = true;
            this._comboBoxProduct.IntegralHeight = false;
            this._comboBoxProduct.Location = new System.Drawing.Point(16, 45);
            this._comboBoxProduct.Name = "_comboBoxProduct";
            this._comboBoxProduct.Size = new System.Drawing.Size(320, 28);
            this._comboBoxProduct.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Осталось в складе";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Цена ";
            // 
            // _textBoxAmount
            // 
            this._textBoxAmount.Location = new System.Drawing.Point(16, 374);
            this._textBoxAmount.Name = "_textBoxAmount";
            this._textBoxAmount.ReadOnly = true;
            this._textBoxAmount.Size = new System.Drawing.Size(320, 26);
            this._textBoxAmount.TabIndex = 6;
            // 
            // _textBoxInStorage
            // 
            this._textBoxInStorage.Location = new System.Drawing.Point(16, 180);
            this._textBoxInStorage.Name = "_textBoxInStorage";
            this._textBoxInStorage.ReadOnly = true;
            this._textBoxInStorage.Size = new System.Drawing.Size(320, 26);
            this._textBoxInStorage.TabIndex = 4;
            // 
            // _textBoxCount
            // 
            this._textBoxCount.Location = new System.Drawing.Point(16, 308);
            this._textBoxCount.Name = "_textBoxCount";
            this._textBoxCount.Size = new System.Drawing.Size(320, 26);
            this._textBoxCount.TabIndex = 5;
            // 
            // _textBoxPrice
            // 
            this._textBoxPrice.Location = new System.Drawing.Point(16, 241);
            this._textBoxPrice.Name = "_textBoxPrice";
            this._textBoxPrice.ReadOnly = true;
            this._textBoxPrice.Size = new System.Drawing.Size(320, 26);
            this._textBoxPrice.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Баркод";
            // 
            // _comboBoxBarCode
            // 
            this._comboBoxBarCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this._comboBoxBarCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this._comboBoxBarCode.Cursor = System.Windows.Forms.Cursors.Hand;
            this._comboBoxBarCode.DropDownHeight = 50;
            this._comboBoxBarCode.Enabled = false;
            this._comboBoxBarCode.FormattingEnabled = true;
            this._comboBoxBarCode.IntegralHeight = false;
            this._comboBoxBarCode.Location = new System.Drawing.Point(16, 109);
            this._comboBoxBarCode.Name = "_comboBoxBarCode";
            this._comboBoxBarCode.Size = new System.Drawing.Size(320, 28);
            this._comboBoxBarCode.TabIndex = 2;
            // 
            // _buttonDelete
            // 
            this._buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonDelete.Location = new System.Drawing.Point(186, 430);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(135, 39);
            this._buttonDelete.TabIndex = 8;
            this._buttonDelete.Text = "Удалить";
            this._buttonDelete.UseVisualStyleBackColor = true;
            // 
            // _buttonAdd
            // 
            this._buttonAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonAdd.Location = new System.Drawing.Point(28, 430);
            this._buttonAdd.Name = "_buttonAdd";
            this._buttonAdd.Size = new System.Drawing.Size(135, 39);
            this._buttonAdd.TabIndex = 7;
            this._buttonAdd.Text = "Добавить";
            this._buttonAdd.UseVisualStyleBackColor = true;
            // 
            // _buttonSave
            // 
            this._buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSave.Location = new System.Drawing.Point(85, 486);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(170, 39);
            this._buttonSave.TabIndex = 9;
            this._buttonSave.Text = "Сохранить";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.objectListView2);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this._textBoxTotal2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1106, 558);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Проданные товары";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // objectListView2
            // 
            this.objectListView2.AllColumns.Add(this.olvColumn5);
            this.objectListView2.AllColumns.Add(this.olvColumn6);
            this.objectListView2.AllColumns.Add(this.olvColumn8);
            this.objectListView2.AllColumns.Add(this.olvColumn9);
            this.objectListView2.AllColumns.Add(this.olvColumn12);
            this.objectListView2.AllColumns.Add(this.olvColumn13);
            this.objectListView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn5,
            this.olvColumn6,
            this.olvColumn8,
            this.olvColumn9,
            this.olvColumn12,
            this.olvColumn13});
            this.objectListView2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView2.FullRowSelect = true;
            this.objectListView2.GridLines = true;
            this.objectListView2.Location = new System.Drawing.Point(3, 60);
            this.objectListView2.MultiSelect = false;
            this.objectListView2.Name = "objectListView2";
            this.objectListView2.ShowGroups = false;
            this.objectListView2.Size = new System.Drawing.Size(1100, 469);
            this.objectListView2.TabIndex = 11;
            this.objectListView2.UseCompatibleStateImageBehavior = false;
            this.objectListView2.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "BuyProduct.Product.Name";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Text = "Товар";
            this.olvColumn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Width = 301;
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "BuyProduct.Product.BarCode";
            this.olvColumn6.CellPadding = null;
            this.olvColumn6.Text = "Баркод";
            this.olvColumn6.Width = 139;
            // 
            // olvColumn8
            // 
            this.olvColumn8.AspectName = "BuyProduct.Price";
            this.olvColumn8.CellPadding = null;
            this.olvColumn8.Text = "Цена на продажу";
            this.olvColumn8.Width = 156;
            // 
            // olvColumn9
            // 
            this.olvColumn9.AspectName = "Count";
            this.olvColumn9.CellPadding = null;
            this.olvColumn9.Text = "Кол-во";
            this.olvColumn9.Width = 97;
            // 
            // olvColumn12
            // 
            this.olvColumn12.AspectName = "Total";
            this.olvColumn12.CellPadding = null;
            this.olvColumn12.Text = "Сумма";
            this.olvColumn12.Width = 98;
            // 
            // olvColumn13
            // 
            this.olvColumn13.AspectName = "Date";
            this.olvColumn13.CellPadding = null;
            this.olvColumn13.Text = "Дата";
            this.olvColumn13.Width = 213;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._buttonShowAll);
            this.groupBox2.Controls.Add(this._buttonSearch);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._dtpTo);
            this.groupBox2.Controls.Add(this._dtpFrom);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1100, 57);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск";
            // 
            // _buttonShowAll
            // 
            this._buttonShowAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonShowAll.Location = new System.Drawing.Point(860, 16);
            this._buttonShowAll.Name = "_buttonShowAll";
            this._buttonShowAll.Size = new System.Drawing.Size(222, 32);
            this._buttonShowAll.TabIndex = 2;
            this._buttonShowAll.Text = "Показать все";
            this._buttonShowAll.UseVisualStyleBackColor = true;
            // 
            // _buttonSearch
            // 
            this._buttonSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSearch.Location = new System.Drawing.Point(661, 16);
            this._buttonSearch.Name = "_buttonSearch";
            this._buttonSearch.Size = new System.Drawing.Size(117, 32);
            this._buttonSearch.TabIndex = 2;
            this._buttonSearch.Text = "Поиск";
            this._buttonSearch.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(365, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 20);
            this.label8.TabIndex = 1;
            this.label8.Text = "по";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(99, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "C";
            // 
            // _dtpTo
            // 
            this._dtpTo.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpTo.Location = new System.Drawing.Point(403, 17);
            this._dtpTo.Name = "_dtpTo";
            this._dtpTo.Size = new System.Drawing.Size(200, 26);
            this._dtpTo.TabIndex = 0;
            // 
            // _dtpFrom
            // 
            this._dtpFrom.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpFrom.Location = new System.Drawing.Point(137, 17);
            this._dtpFrom.Name = "_dtpFrom";
            this._dtpFrom.Size = new System.Drawing.Size(200, 26);
            this._dtpFrom.TabIndex = 0;
            // 
            // _textBoxTotal2
            // 
            this._textBoxTotal2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotal2.Location = new System.Drawing.Point(3, 529);
            this._textBoxTotal2.Name = "_textBoxTotal2";
            this._textBoxTotal2.ReadOnly = true;
            this._textBoxTotal2.Size = new System.Drawing.Size(1100, 26);
            this._textBoxTotal2.TabIndex = 8;
            this._textBoxTotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Store
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1114, 591);
            this.Controls.Add(this.tabControl1);
            this.Name = "Store";
            this.Text = "Магазин";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView2)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn10;
        private BrightIdeasSoftware.OLVColumn olvColumn11;
        private System.Windows.Forms.TextBox _textBoxTotal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _textBoxAmount;
        private System.Windows.Forms.TextBox _textBoxCount;
        private System.Windows.Forms.TextBox _textBoxPrice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox _comboBoxBarCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox _comboBoxProduct;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.Button _buttonAdd;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.TabPage tabPage2;
        private BrightIdeasSoftware.ObjectListView objectListView2;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private BrightIdeasSoftware.OLVColumn olvColumn8;
        private BrightIdeasSoftware.OLVColumn olvColumn9;
        private BrightIdeasSoftware.OLVColumn olvColumn12;
        private BrightIdeasSoftware.OLVColumn olvColumn13;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _buttonShowAll;
        private System.Windows.Forms.Button _buttonSearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker _dtpTo;
        private System.Windows.Forms.DateTimePicker _dtpFrom;
        private System.Windows.Forms.TextBox _textBoxTotal2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _textBoxInStorage;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}