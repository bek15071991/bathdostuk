﻿namespace BathDostukWinForm.Forms
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this._buttonOk = new System.Windows.Forms.Button();
            this._textBoxPassword = new System.Windows.Forms.TextBox();
            this._textBoxLogin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this._buttonCancel = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this._buttonSettingDataBase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _buttonOk
            // 
            this._buttonOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonOk.Location = new System.Drawing.Point(230, 104);
            this._buttonOk.Name = "_buttonOk";
            this._buttonOk.Size = new System.Drawing.Size(78, 33);
            this._buttonOk.TabIndex = 3;
            this._buttonOk.Text = "вход";
            this._buttonOk.UseVisualStyleBackColor = true;
            // 
            // _textBoxPassword
            // 
            this._textBoxPassword.Cursor = System.Windows.Forms.Cursors.Default;
            this._textBoxPassword.Location = new System.Drawing.Point(248, 64);
            this._textBoxPassword.Name = "_textBoxPassword";
            this._textBoxPassword.PasswordChar = ' ';
            this._textBoxPassword.Size = new System.Drawing.Size(132, 20);
            this._textBoxPassword.TabIndex = 2;
            // 
            // _textBoxLogin
            // 
            this._textBoxLogin.Cursor = System.Windows.Forms.Cursors.Default;
            this._textBoxLogin.Location = new System.Drawing.Point(248, 20);
            this._textBoxLogin.Name = "_textBoxLogin";
            this._textBoxLogin.Size = new System.Drawing.Size(132, 20);
            this._textBoxLogin.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(197, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Логин";
            this.label1.UseWaitCursor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(197, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Пароль";
            this.label2.UseWaitCursor = true;
            // 
            // _buttonCancel
            // 
            this._buttonCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonCancel.Location = new System.Drawing.Point(323, 104);
            this._buttonCancel.Name = "_buttonCancel";
            this._buttonCancel.Size = new System.Drawing.Size(67, 32);
            this._buttonCancel.TabIndex = 4;
            this._buttonCancel.Text = "отмена";
            this._buttonCancel.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Location = new System.Drawing.Point(38, 20);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(129, 125);
            this.panel1.TabIndex = 5;
            // 
            // _buttonSettingDataBase
            // 
            this._buttonSettingDataBase.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSettingDataBase.Location = new System.Drawing.Point(230, 143);
            this._buttonSettingDataBase.Name = "_buttonSettingDataBase";
            this._buttonSettingDataBase.Size = new System.Drawing.Size(160, 32);
            this._buttonSettingDataBase.TabIndex = 4;
            this._buttonSettingDataBase.Text = "настройка подключения";
            this._buttonSettingDataBase.UseVisualStyleBackColor = true;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(417, 188);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._textBoxLogin);
            this.Controls.Add(this._textBoxPassword);
            this.Controls.Add(this._buttonSettingDataBase);
            this.Controls.Add(this._buttonCancel);
            this.Controls.Add(this._buttonOk);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _buttonOk;
        private System.Windows.Forms.TextBox _textBoxPassword;
        private System.Windows.Forms.TextBox _textBoxLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button _buttonCancel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button _buttonSettingDataBase;
    }
}