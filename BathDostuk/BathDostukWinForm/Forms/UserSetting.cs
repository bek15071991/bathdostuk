﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;
using Rule = BathDostukDomain.Entities.Enum.Rule;

namespace BathDostukWinForm.Forms
{
    public partial class UserSetting : Form, IViewUserSetting
    {
        public UserSetting()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            _olvColumnIsDelete.AspectToStringConverter = value => (bool)value == false ? "не удален" : "удален";
            _olvColumnRule.AspectToStringConverter = val =>
            {
                var rul = (Rule)val;
                return EnumConvert.ToString(rul);
            };

            _buttonDelete.Click += (sender, args) =>
            {
                Delete(this, EventArgs.Empty);
                _comboBoxRule.Enabled = true;
            };

            button1.Click += (sender, args) =>
            {
                Login =
                    Fio =
                        Address =
                            NumberPhone =
                                 Password = string.Empty;
                Salary = 0;
                IsDelete = false;
                SelectedUser = null;
                Rule = (Rule)10;
                _comboBoxRule.Enabled = true;
            };

            _buttonSave.Click += (sender, args) => { Save(this, EventArgs.Empty); };



            _comboBoxRule.Items.Add(EnumConvert.ToString(Rule.Admin));
            _comboBoxRule.Items.Add(EnumConvert.ToString(Rule.Director));
            _comboBoxRule.Items.Add(EnumConvert.ToString(Rule.Cassier));
            _comboBoxRule.Items.Add(EnumConvert.ToString(Rule.Barman));
            _comboBoxRule.Items.Add(EnumConvert.ToString(Rule.Attendant));
            _comboBoxRule.Items.Add(EnumConvert.ToString(Rule.Fireman));
            _comboBoxRule.Items.Add(EnumConvert.ToString(Rule.Other));

            _comboBoxRule.SelectedIndex = -1;

            objectListView1.SelectedIndexChanged += (sender, args) =>
            {

                var user = objectListView1.SelectedObject as Employee;

                if (user != null)
                {
                    Fio = user.Fio;
                    Address = user.Address;
                    NumberPhone = user.NumberPhone;
                    Salary = Convert.ToInt32(user.Salary);
                    Login = user.Login;
                    Password = user.Password;
                    Rule = (Rule)10;
                    IsDelete = user.Delete;
                    _comboBoxRule.Enabled = false;
                    CountDays = user.CountDays;
                }
            };


            _textBoxNumber.KeyPress += TextBoxNumberOnKeyPress;

            _textBoxSalary.KeyPress += TextBoxNumberOnKeyPress;

        }


        private void TextBoxNumberOnKeyPress(object sender, KeyPressEventArgs keyPressEventArgs)
        {
            if (char.IsDigit(keyPressEventArgs.KeyChar) ||
                keyPressEventArgs.KeyChar == (char)8)
                keyPressEventArgs.Handled = false;
            else keyPressEventArgs.Handled = true;
        }

        public string Fio
        {
            get { return _textBoxFIO.Text; }
            set { _textBoxFIO.Text = value; }
        }

        public string Address
        {
            get { return _textBoxAddress.Text; }
            set { _textBoxAddress.Text = value; }
        }

        public string NumberPhone
        {
            get { return _textBoxNumber.Text; }
            set { _textBoxNumber.Text = value; }
        }

        public int Salary
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxSalary.Text))
                    return 0;
                return Convert.ToInt32(_textBoxSalary.Text);

            }
            set { _textBoxSalary.Text = value.ToString(); }
        }


        public string Login
        {
            get { return _textBoxLogin.Text; }
            set { _textBoxLogin.Text = value; }
        }

        public string Password
        {
            get { return _textBoxPassword.Text; }
            set { _textBoxPassword.Text = value; }
        }

        public Rule Rule
        {
            get { return (Rule)_comboBoxRule.SelectedIndex; }
            set
            {
                if (value == (Rule)10)
                {
                    _comboBoxRule.SelectedIndex = -1;
                    if (_comboBoxRule.Enabled == false)
                        _comboBoxRule.Enabled = true;
                }
                else _comboBoxRule.SelectedIndex = (int)value;

            }
        }

        public bool IsDelete
        {
            get { return _checkBoxDelete.Checked; }
            set { _checkBoxDelete.Checked = value; }
        }

        public int CountDays
        {
            get { return Convert.ToInt32(_textBoxDays.Value); }
            set { _textBoxDays.Value = value; }
        }

        public List<Employee> Users
        {
            get { return (List<Employee>)objectListView1.Objects; }
            set { objectListView1.Objects = value; }
        }

        public Employee SelectedUser
        {
            get { return (Employee)objectListView1.SelectedObject; }
            set { objectListView1.SelectedObject = value; }
        }

        public event EventHandler<EventArgs> Save;
        public event EventHandler<EventArgs> Delete;
    }
}
