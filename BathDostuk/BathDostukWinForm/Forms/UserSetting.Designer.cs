﻿namespace BathDostukWinForm.Forms
{
    partial class UserSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn7 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnRule = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._olvColumnIsDelete = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._textBoxDays = new System.Windows.Forms.NumericUpDown();
            this._checkBoxDelete = new System.Windows.Forms.CheckBox();
            this._comboBoxRule = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this._textBoxPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._textBoxLogin = new System.Windows.Forms.TextBox();
            this._textBoxNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._textBoxSalary = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._textBoxAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._textBoxFIO = new System.Windows.Forms.TextBox();
            this._buttonDelete = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this._buttonSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxDays)).BeginInit();
            this.SuspendLayout();
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn1);
            this.objectListView1.AllColumns.Add(this.olvColumn2);
            this.objectListView1.AllColumns.Add(this.olvColumn3);
            this.objectListView1.AllColumns.Add(this.olvColumn4);
            this.objectListView1.AllColumns.Add(this.olvColumn7);
            this.objectListView1.AllColumns.Add(this.olvColumn5);
            this.objectListView1.AllColumns.Add(this.olvColumn6);
            this.objectListView1.AllColumns.Add(this._olvColumnRule);
            this.objectListView1.AllColumns.Add(this._olvColumnIsDelete);
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn7,
            this.olvColumn5,
            this.olvColumn6,
            this._olvColumnRule,
            this._olvColumnIsDelete});
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(327, 0);
            this.objectListView1.MultiSelect = false;
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(876, 538);
            this.objectListView1.TabIndex = 13;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Fio";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Ф.И.О.";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 189;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "Address";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.Text = "Адрес";
            this.olvColumn2.Width = 192;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "NumberPhone";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.Text = "Телефон";
            this.olvColumn3.Width = 119;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "Salary";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.Text = "Зарплата";
            this.olvColumn4.Width = 70;
            // 
            // olvColumn7
            // 
            this.olvColumn7.AspectName = "CountDays";
            this.olvColumn7.CellPadding = null;
            this.olvColumn7.Text = "Кол-во дней";
            this.olvColumn7.Width = 79;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "Login";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.Text = "Логин";
            this.olvColumn5.Width = 71;
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "Password";
            this.olvColumn6.CellPadding = null;
            this.olvColumn6.Text = "Пароль";
            this.olvColumn6.Width = 72;
            // 
            // _olvColumnRule
            // 
            this._olvColumnRule.AspectName = "Rule";
            this._olvColumnRule.CellPadding = null;
            this._olvColumnRule.Text = "Позиция";
            this._olvColumnRule.Width = 69;
            // 
            // _olvColumnIsDelete
            // 
            this._olvColumnIsDelete.AspectName = "Delete";
            this._olvColumnIsDelete.CellPadding = null;
            this._olvColumnIsDelete.Text = "Состояние";
            this._olvColumnIsDelete.Width = 69;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._textBoxDays);
            this.groupBox1.Controls.Add(this._checkBoxDelete);
            this.groupBox1.Controls.Add(this._comboBoxRule);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this._textBoxPassword);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this._textBoxLogin);
            this.groupBox1.Controls.Add(this._textBoxNumber);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._textBoxSalary);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this._textBoxAddress);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._textBoxFIO);
            this.groupBox1.Controls.Add(this._buttonDelete);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this._buttonSave);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 538);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // _textBoxDays
            // 
            this._textBoxDays.Location = new System.Drawing.Point(6, 232);
            this._textBoxDays.Name = "_textBoxDays";
            this._textBoxDays.Size = new System.Drawing.Size(315, 20);
            this._textBoxDays.TabIndex = 5;
            // 
            // _checkBoxDelete
            // 
            this._checkBoxDelete.AutoSize = true;
            this._checkBoxDelete.Location = new System.Drawing.Point(12, 427);
            this._checkBoxDelete.Name = "_checkBoxDelete";
            this._checkBoxDelete.Size = new System.Drawing.Size(130, 17);
            this._checkBoxDelete.TabIndex = 9;
            this._checkBoxDelete.Text = "Состояние удаления";
            this._checkBoxDelete.UseVisualStyleBackColor = true;
            // 
            // _comboBoxRule
            // 
            this._comboBoxRule.Cursor = System.Windows.Forms.Cursors.Hand;
            this._comboBoxRule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxRule.FormattingEnabled = true;
            this._comboBoxRule.Location = new System.Drawing.Point(4, 384);
            this._comboBoxRule.Name = "_comboBoxRule";
            this._comboBoxRule.Size = new System.Drawing.Size(317, 21);
            this._comboBoxRule.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 368);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Позиция";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 316);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Пароль";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 266);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Логин";
            // 
            // _textBoxPassword
            // 
            this._textBoxPassword.Location = new System.Drawing.Point(4, 332);
            this._textBoxPassword.Name = "_textBoxPassword";
            this._textBoxPassword.Size = new System.Drawing.Size(317, 20);
            this._textBoxPassword.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Телефон";
            // 
            // _textBoxLogin
            // 
            this._textBoxLogin.Location = new System.Drawing.Point(4, 282);
            this._textBoxLogin.Name = "_textBoxLogin";
            this._textBoxLogin.Size = new System.Drawing.Size(317, 20);
            this._textBoxLogin.TabIndex = 6;
            // 
            // _textBoxNumber
            // 
            this._textBoxNumber.Location = new System.Drawing.Point(4, 130);
            this._textBoxNumber.Name = "_textBoxNumber";
            this._textBoxNumber.Size = new System.Drawing.Size(317, 20);
            this._textBoxNumber.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(180, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Количество рабочих дней в месяц";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Зарплата";
            // 
            // _textBoxSalary
            // 
            this._textBoxSalary.Location = new System.Drawing.Point(4, 181);
            this._textBoxSalary.Name = "_textBoxSalary";
            this._textBoxSalary.Size = new System.Drawing.Size(317, 20);
            this._textBoxSalary.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Адрес";
            // 
            // _textBoxAddress
            // 
            this._textBoxAddress.Location = new System.Drawing.Point(4, 82);
            this._textBoxAddress.Name = "_textBoxAddress";
            this._textBoxAddress.Size = new System.Drawing.Size(317, 20);
            this._textBoxAddress.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Ф.И.О.";
            // 
            // _textBoxFIO
            // 
            this._textBoxFIO.Location = new System.Drawing.Point(4, 32);
            this._textBoxFIO.Name = "_textBoxFIO";
            this._textBoxFIO.Size = new System.Drawing.Size(317, 20);
            this._textBoxFIO.TabIndex = 1;
            // 
            // _buttonDelete
            // 
            this._buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonDelete.Location = new System.Drawing.Point(172, 464);
            this._buttonDelete.Name = "_buttonDelete";
            this._buttonDelete.Size = new System.Drawing.Size(99, 48);
            this._buttonDelete.TabIndex = 11;
            this._buttonDelete.Text = "Удалить";
            this._buttonDelete.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(43, 464);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 48);
            this.button1.TabIndex = 10;
            this.button1.Text = "Очистить";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // _buttonSave
            // 
            this._buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSave.Location = new System.Drawing.Point(96, 518);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(129, 48);
            this._buttonSave.TabIndex = 12;
            this._buttonSave.Text = "Изменить/Сохранить";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // UserSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 538);
            this.Controls.Add(this.objectListView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "UserSetting";
            this.Text = "Настройки пользователей";
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._textBoxDays)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox _textBoxNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _textBoxAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _textBoxFIO;
        private System.Windows.Forms.Button _buttonDelete;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox _textBoxLogin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox _textBoxSalary;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox _textBoxPassword;
        private System.Windows.Forms.ComboBox _comboBoxRule;
        private System.Windows.Forms.Label label7;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private BrightIdeasSoftware.OLVColumn _olvColumnRule;
        private BrightIdeasSoftware.OLVColumn _olvColumnIsDelete;
        private System.Windows.Forms.CheckBox _checkBoxDelete;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown _textBoxDays;
        private BrightIdeasSoftware.OLVColumn olvColumn7;
    }
}