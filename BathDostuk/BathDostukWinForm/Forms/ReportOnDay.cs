﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class ReportOnDay : Form, IViewReportOnDay
    {
        private List<Employee> _users; 

        public ReportOnDay()
        {
            InitializeComponent();

            if (User.CurrentUser.Rule == Rule.Barman)
            {
                tabControl1.TabPages.Remove(tabPage1);
                tabControl1.TabPages.Remove(tabPage3);
            }

            _olvColumnGender.AspectToStringConverter = (ob) =>
            {
                if (ob == null) return string.Empty;
                var gender = (Gender)ob;
                return EnumConvert.ToString(gender);
            }
               ;


            _buttonSave.Click += (sender, args) =>
            {
                Save(this, EventArgs.Empty);
            };

            Shown += (sender, args) =>
            {
                _textBoxBathTotal.Text = Orders != null
                    ? "Всего билетов: " + Orders.Count + "    Итого:   " + TotalBath + " сом."
                    : string.Empty;
                _textBoxTotal.Text = string.Format("Всего :   {0} сом.", Total);
                _textBoxTotalExpense.Text = string.Format("Всего расходов: {0} сом.", TotalExpense);
                _textBoxTotalSalaries.Text = string.Format("Всего выданных зарплат: {0} сом.", TotalExpense);
            };

            _buttonAdd.Click += (sender, args) =>
            {
                if (_comboBoxUsers.SelectedIndex == -1) return;
                var user = _comboBoxUsers.SelectedItem as Employee;
                _listUsers.AddObject(user);
                _users.Remove(user);
                _comboBoxUsers.DataSource = null;
                _comboBoxUsers.SelectedItem = null;
                _comboBoxUsers.DataSource = _users;
                _comboBoxUsers.DisplayMember = "Fio";
                _comboBoxUsers.SelectedIndex = -1;
            };
            _olvColumnRule.AspectToStringConverter = val =>
            {
                var rul = (Rule)val;
                return EnumConvert.ToString(rul);
            };

            _buttonDelete.Click += (sender, args) =>
            {
                var user = _listUsers.SelectedObject as Employee;
                if (user.Id == User.CurrentUser.Id)
                {
                    MessageBox.Show("Нельзя удалить данного пользователя!!!", "Внимание...", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
                _users.Add(user);
                _comboBoxUsers.DataSource = null;
                _comboBoxUsers.DataSource = _users;
                _comboBoxUsers.DisplayMember = "Fio";
                _comboBoxUsers.SelectedIndex = -1;
                _listUsers.RemoveObject(user);
            };

        }


        public List<Order> Orders
        {
            get { return _listOrders.Objects as List<Order>; }
            set
            { _listOrders.Objects = value;
            }
        }

        public List<SaleProduct> SaleProducts
        {
            get { return _listSaleProducts.Objects.Cast<SaleProduct>().ToList(); }
            set
            {
                _textBoxStoreTotal.Text = " Итого:   " + TotalStore + " сом.";
                _listSaleProducts.Objects = value;
            }
        }

        public List<Expense> Expenses
        {
            get { return _listExpenses.Objects.Cast<Expense>().ToList(); }
            set { _listExpenses.SetObjects(value); }
        }

        public List<Expense> Salaries
        {
            get { return _listSalaries.Objects.Cast<Expense>().ToList(); }
            set { _listSalaries.SetObjects(value); }
        }

        public List<Employee> Users
        {
            get { return _listUsers.Objects.Cast<Employee>().ToList(); }
            set { _listUsers.SetObjects(value); }
        }


        public void ShowUsers(List<Employee> users)
        {
            _users = users;
            _comboBoxUsers.DataSource = users;
            _comboBoxUsers.DisplayMember = "Fio";
            _comboBoxUsers.SelectedIndex = -1;
        }

        public double TotalExpense { get; set; }

        public double TotalSalary
        {
            get;
            set;
        }

        public double TotalBath
        {
            get;
            set;
        }

        public double TotalStore
        {
            get;
            set;
        }

        public double Total
        {
            get;
            set;
        }

        public event EventHandler<EventArgs> Save; 

        
    }
}
