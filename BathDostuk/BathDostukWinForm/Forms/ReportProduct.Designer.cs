﻿namespace BathDostukWinForm.Forms
{
    partial class ReportProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._comboBoxUserStorage = new System.Windows.Forms.ComboBox();
            this._buttonSearchStorage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._dtpTo = new System.Windows.Forms.DateTimePicker();
            this._dtpFrom = new System.Windows.Forms.DateTimePicker();
            this._textBoxTotalStorage = new System.Windows.Forms.TextBox();
            this._listBuyProducts = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn7 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn8 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn9 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn12 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn13 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._comboBoxUserStore = new System.Windows.Forms.ComboBox();
            this._buttonSearchStore = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this._dtpStoreTo = new System.Windows.Forms.DateTimePicker();
            this._dtpStoreFrom = new System.Windows.Forms.DateTimePicker();
            this._listSaleProduct = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn10 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn11 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn14 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxTotalStore = new System.Windows.Forms.TextBox();
            this.olvColumn15 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listBuyProducts)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listSaleProduct)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1252, 507);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this._listBuyProducts);
            this.tabPage1.Controls.Add(this._textBoxTotalStorage);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1244, 474);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Склад";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this._listSaleProduct);
            this.tabPage2.Controls.Add(this._textBoxTotalStore);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1244, 474);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Магазин";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._comboBoxUserStorage);
            this.groupBox2.Controls.Add(this._buttonSearchStorage);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._dtpTo);
            this.groupBox2.Controls.Add(this._dtpFrom);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(257, 468);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск";
            // 
            // _comboBoxUserStorage
            // 
            this._comboBoxUserStorage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxUserStorage.FormattingEnabled = true;
            this._comboBoxUserStorage.Location = new System.Drawing.Point(25, 147);
            this._comboBoxUserStorage.Name = "_comboBoxUserStorage";
            this._comboBoxUserStorage.Size = new System.Drawing.Size(199, 24);
            this._comboBoxUserStorage.TabIndex = 10;
            // 
            // _buttonSearchStorage
            // 
            this._buttonSearchStorage.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSearchStorage.Location = new System.Drawing.Point(61, 193);
            this._buttonSearchStorage.Name = "_buttonSearchStorage";
            this._buttonSearchStorage.Size = new System.Drawing.Size(117, 32);
            this._buttonSearchStorage.TabIndex = 9;
            this._buttonSearchStorage.Text = "Поиск";
            this._buttonSearchStorage.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Сотрудник";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "по";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "C";
            // 
            // _dtpTo
            // 
            this._dtpTo.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpTo.Location = new System.Drawing.Point(59, 75);
            this._dtpTo.Name = "_dtpTo";
            this._dtpTo.Size = new System.Drawing.Size(153, 23);
            this._dtpTo.TabIndex = 4;
            // 
            // _dtpFrom
            // 
            this._dtpFrom.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpFrom.Location = new System.Drawing.Point(59, 31);
            this._dtpFrom.Name = "_dtpFrom";
            this._dtpFrom.Size = new System.Drawing.Size(153, 23);
            this._dtpFrom.TabIndex = 5;
            // 
            // _textBoxTotalStorage
            // 
            this._textBoxTotalStorage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotalStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._textBoxTotalStorage.Location = new System.Drawing.Point(260, 448);
            this._textBoxTotalStorage.Name = "_textBoxTotalStorage";
            this._textBoxTotalStorage.ReadOnly = true;
            this._textBoxTotalStorage.Size = new System.Drawing.Size(981, 23);
            this._textBoxTotalStorage.TabIndex = 11;
            this._textBoxTotalStorage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // _listBuyProducts
            // 
            this._listBuyProducts.AllColumns.Add(this.olvColumn5);
            this._listBuyProducts.AllColumns.Add(this.olvColumn6);
            this._listBuyProducts.AllColumns.Add(this.olvColumn7);
            this._listBuyProducts.AllColumns.Add(this.olvColumn8);
            this._listBuyProducts.AllColumns.Add(this.olvColumn9);
            this._listBuyProducts.AllColumns.Add(this.olvColumn12);
            this._listBuyProducts.AllColumns.Add(this.olvColumn13);
            this._listBuyProducts.AllColumns.Add(this.olvColumn1);
            this._listBuyProducts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn5,
            this.olvColumn6,
            this.olvColumn7,
            this.olvColumn8,
            this.olvColumn9,
            this.olvColumn12,
            this.olvColumn13,
            this.olvColumn1});
            this._listBuyProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this._listBuyProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listBuyProducts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._listBuyProducts.FullRowSelect = true;
            this._listBuyProducts.GridLines = true;
            this._listBuyProducts.Location = new System.Drawing.Point(260, 3);
            this._listBuyProducts.MultiSelect = false;
            this._listBuyProducts.Name = "_listBuyProducts";
            this._listBuyProducts.ShowGroups = false;
            this._listBuyProducts.Size = new System.Drawing.Size(981, 445);
            this._listBuyProducts.TabIndex = 12;
            this._listBuyProducts.UseCompatibleStateImageBehavior = false;
            this._listBuyProducts.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "Product.Name";
            this.olvColumn5.CellPadding = null;
            this.olvColumn5.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Text = "Товар";
            this.olvColumn5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn5.Width = 269;
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "Product.BarCode";
            this.olvColumn6.CellPadding = null;
            this.olvColumn6.Text = "Баркод";
            this.olvColumn6.Width = 87;
            // 
            // olvColumn7
            // 
            this.olvColumn7.AspectName = "Cost";
            this.olvColumn7.CellPadding = null;
            this.olvColumn7.Text = "Стоимость";
            this.olvColumn7.Width = 118;
            // 
            // olvColumn8
            // 
            this.olvColumn8.AspectName = "Price";
            this.olvColumn8.CellPadding = null;
            this.olvColumn8.Text = "Цена на продажу";
            this.olvColumn8.Width = 115;
            // 
            // olvColumn9
            // 
            this.olvColumn9.AspectName = "Count";
            this.olvColumn9.CellPadding = null;
            this.olvColumn9.Text = "Кол-во";
            this.olvColumn9.Width = 69;
            // 
            // olvColumn12
            // 
            this.olvColumn12.AspectName = "Total";
            this.olvColumn12.CellPadding = null;
            this.olvColumn12.Text = "Сумма";
            this.olvColumn12.Width = 98;
            // 
            // olvColumn13
            // 
            this.olvColumn13.AspectName = "Date";
            this.olvColumn13.CellPadding = null;
            this.olvColumn13.Text = "Дата";
            this.olvColumn13.Width = 137;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Employee.Fio";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.Text = "Сотрудник";
            this.olvColumn1.Width = 94;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._comboBoxUserStore);
            this.groupBox1.Controls.Add(this._buttonSearchStore);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this._dtpStoreTo);
            this.groupBox1.Controls.Add(this._dtpStoreFrom);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(984, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 468);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Поиск";
            // 
            // _comboBoxUserStore
            // 
            this._comboBoxUserStore.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._comboBoxUserStore.FormattingEnabled = true;
            this._comboBoxUserStore.Location = new System.Drawing.Point(25, 147);
            this._comboBoxUserStore.Name = "_comboBoxUserStore";
            this._comboBoxUserStore.Size = new System.Drawing.Size(199, 24);
            this._comboBoxUserStore.TabIndex = 10;
            // 
            // _buttonSearchStore
            // 
            this._buttonSearchStore.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSearchStore.Location = new System.Drawing.Point(61, 193);
            this._buttonSearchStore.Name = "_buttonSearchStore";
            this._buttonSearchStore.Size = new System.Drawing.Size(117, 32);
            this._buttonSearchStore.TabIndex = 9;
            this._buttonSearchStore.Text = "Поиск";
            this._buttonSearchStore.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Сотрудник";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "по";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "C";
            // 
            // _dtpStoreTo
            // 
            this._dtpStoreTo.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpStoreTo.Location = new System.Drawing.Point(59, 75);
            this._dtpStoreTo.Name = "_dtpStoreTo";
            this._dtpStoreTo.Size = new System.Drawing.Size(153, 23);
            this._dtpStoreTo.TabIndex = 4;
            // 
            // _dtpStoreFrom
            // 
            this._dtpStoreFrom.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpStoreFrom.Location = new System.Drawing.Point(59, 31);
            this._dtpStoreFrom.Name = "_dtpStoreFrom";
            this._dtpStoreFrom.Size = new System.Drawing.Size(153, 23);
            this._dtpStoreFrom.TabIndex = 5;
            // 
            // _listSaleProduct
            // 
            this._listSaleProduct.AllColumns.Add(this.olvColumn2);
            this._listSaleProduct.AllColumns.Add(this.olvColumn3);
            this._listSaleProduct.AllColumns.Add(this.olvColumn4);
            this._listSaleProduct.AllColumns.Add(this.olvColumn10);
            this._listSaleProduct.AllColumns.Add(this.olvColumn11);
            this._listSaleProduct.AllColumns.Add(this.olvColumn14);
            this._listSaleProduct.AllColumns.Add(this.olvColumn15);
            this._listSaleProduct.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn10,
            this.olvColumn11,
            this.olvColumn14,
            this.olvColumn15});
            this._listSaleProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this._listSaleProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listSaleProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._listSaleProduct.FullRowSelect = true;
            this._listSaleProduct.GridLines = true;
            this._listSaleProduct.Location = new System.Drawing.Point(3, 3);
            this._listSaleProduct.MultiSelect = false;
            this._listSaleProduct.Name = "_listSaleProduct";
            this._listSaleProduct.ShowGroups = false;
            this._listSaleProduct.Size = new System.Drawing.Size(981, 447);
            this._listSaleProduct.TabIndex = 13;
            this._listSaleProduct.UseCompatibleStateImageBehavior = false;
            this._listSaleProduct.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "BuyProduct.Product.Name";
            this.olvColumn2.CellPadding = null;
            this.olvColumn2.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn2.Text = "Товар";
            this.olvColumn2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn2.Width = 301;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "BuyProduct.Product.BarCode";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.Text = "Баркод";
            this.olvColumn3.Width = 111;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "BuyProduct.Price";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.Text = "Цена на продажу";
            this.olvColumn4.Width = 113;
            // 
            // olvColumn10
            // 
            this.olvColumn10.AspectName = "Count";
            this.olvColumn10.CellPadding = null;
            this.olvColumn10.Text = "Кол-во";
            this.olvColumn10.Width = 62;
            // 
            // olvColumn11
            // 
            this.olvColumn11.AspectName = "Total";
            this.olvColumn11.CellPadding = null;
            this.olvColumn11.Text = "Сумма";
            this.olvColumn11.Width = 101;
            // 
            // olvColumn14
            // 
            this.olvColumn14.AspectName = "Date";
            this.olvColumn14.CellPadding = null;
            this.olvColumn14.Text = "Дата";
            this.olvColumn14.Width = 152;
            // 
            // _textBoxTotalStore
            // 
            this._textBoxTotalStore.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotalStore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._textBoxTotalStore.Location = new System.Drawing.Point(3, 450);
            this._textBoxTotalStore.Name = "_textBoxTotalStore";
            this._textBoxTotalStore.ReadOnly = true;
            this._textBoxTotalStore.Size = new System.Drawing.Size(981, 21);
            this._textBoxTotalStore.TabIndex = 12;
            this._textBoxTotalStore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // olvColumn15
            // 
            this.olvColumn15.AspectName = "Employee.Fio";
            this.olvColumn15.CellPadding = null;
            this.olvColumn15.Text = "Сотрудник";
            this.olvColumn15.Width = 134;
            // 
            // ReportProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1252, 507);
            this.Controls.Add(this.tabControl1);
            this.Name = "ReportProduct";
            this.Text = "Отчет по товарам";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listBuyProducts)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._listSaleProduct)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox _comboBoxUserStorage;
        private System.Windows.Forms.Button _buttonSearchStorage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker _dtpTo;
        private System.Windows.Forms.DateTimePicker _dtpFrom;
        private System.Windows.Forms.TextBox _textBoxTotalStorage;
        private BrightIdeasSoftware.ObjectListView _listBuyProducts;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private BrightIdeasSoftware.OLVColumn olvColumn7;
        private BrightIdeasSoftware.OLVColumn olvColumn8;
        private BrightIdeasSoftware.OLVColumn olvColumn9;
        private BrightIdeasSoftware.OLVColumn olvColumn12;
        private BrightIdeasSoftware.OLVColumn olvColumn13;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox _comboBoxUserStore;
        private System.Windows.Forms.Button _buttonSearchStore;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker _dtpStoreTo;
        private System.Windows.Forms.DateTimePicker _dtpStoreFrom;
        private BrightIdeasSoftware.ObjectListView _listSaleProduct;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn10;
        private BrightIdeasSoftware.OLVColumn olvColumn11;
        private BrightIdeasSoftware.OLVColumn olvColumn14;
        private BrightIdeasSoftware.OLVColumn olvColumn15;
        private System.Windows.Forms.TextBox _textBoxTotalStore;
    }
}