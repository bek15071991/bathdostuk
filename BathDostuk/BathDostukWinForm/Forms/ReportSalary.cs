﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class ReportSalary : Form, IViewReportSalary
    {
        public ReportSalary()
        {
            InitializeComponent();
            _buttonSearch.Click += (sender, args) =>
            {
                Search(this, EventArgs.Empty);
            };
        }

        public DateTime From
        {
            get { return _dtpFrom.Value.Date; }
        }

        public DateTime To
        {
            get { return _dtpTo.Value.Date; }
        }

        public void ShowExpenses(List<Expense> expenses)
        {
            objectListView1.SetObjects(expenses);
        }

        

        public void ShowUsers(List<Employee> employees)
        {
            _comboBoxUser.DataSource = employees;
            _comboBoxUser.DisplayMember = "Fio";
            _comboBoxUser.SelectedIndex = -1;
        }

        public void ShowSalaryDay(double salary, double day)
        {
            _textBoxTotal2.Text = string.Format("Итого :   {0} сом.       Отработанных дней :   {1}", salary, day);
        }

        public Employee SeletcteEmployee
        {
            get { return _comboBoxUser.SelectedItem as Employee; }
            set
            {
                if (value == null)
                    _comboBoxUser.SelectedIndex = -1;
            }
        }

        public event EventHandler<EventArgs> Search;
    }
}
