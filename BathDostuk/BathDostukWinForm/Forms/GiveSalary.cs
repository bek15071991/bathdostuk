﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Forms
{
    public partial class GiveSalary : Form, IViewGiveSalary
    {
        public GiveSalary()
        {
            InitializeComponent();
            _buttonDelete.Click += (sender, args) => { Delete(this, EventArgs.Empty); };
            _buttonSave.Click += (sender, args) => { Save(this, EventArgs.Empty); };

            _textBoxSalary.KeyPress += (sender, args) =>
            {
                if (char.IsDigit(args.KeyChar) ||
                    args.KeyChar == (char)8)
                    args.Handled = false;
                else args.Handled = true;
            };
        }

        public double Count
        {
            get
            {
                if (string.IsNullOrEmpty(_textBoxSalary.Text)) return 0;
                return Convert.ToDouble(_textBoxSalary.Text);
            }
            set { _textBoxSalary.Text = value.ToString(); }
        }


        public void ShowEmployee(List<Employee> employees)
        {
            _comboBoxEmploye.DataSource = employees;
            _comboBoxEmploye.DisplayMember = "Fio";
            _comboBoxEmploye.SelectedIndex = -1;
        }

        public Employee SelectedEmployee
        {
            get { return _comboBoxEmploye.SelectedItem as Employee; }
            set
            {
                if (value == null)
                    _comboBoxEmploye.SelectedIndex = -1;
            }
        }

        public List<Expense> Expenses
        {
            get { return objectListView1.Objects as List<Expense>; }
            set { objectListView1.Objects = value; }
        }

        public string Comment
        {
            get { return _textBoxComment.Text; }
            set
            {
                _textBoxComment.Text =
                    value;
            }
        }

        public Expense SelectedExpense
        {
            get { return objectListView1.SelectedObject as Expense; }
            set { objectListView1.SelectedObject = value; }
        }

        public event EventHandler<EventArgs> Save;
        public event EventHandler<EventArgs> Delete;
    }
}
