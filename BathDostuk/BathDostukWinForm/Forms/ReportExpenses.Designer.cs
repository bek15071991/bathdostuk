﻿namespace BathDostukWinForm.Forms
{
    partial class ReportExpenses
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.objectListView1 = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn11 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this._textBoxTotal2 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._buttonSearch = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this._dtpTo = new System.Windows.Forms.DateTimePicker();
            this._dtpFrom = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Employee.Fio";
            this.olvColumn1.CellPadding = null;
            this.olvColumn1.DisplayIndex = 0;
            this.olvColumn1.HeaderTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Text = "Сотрудник";
            this.olvColumn1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.olvColumn1.Width = 253;
            // 
            // objectListView1
            // 
            this.objectListView1.AllColumns.Add(this.olvColumn3);
            this.objectListView1.AllColumns.Add(this.olvColumn4);
            this.objectListView1.AllColumns.Add(this.olvColumn11);
            this.objectListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn11});
            this.objectListView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.objectListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objectListView1.FullRowSelect = true;
            this.objectListView1.GridLines = true;
            this.objectListView1.Location = new System.Drawing.Point(256, 0);
            this.objectListView1.MultiSelect = false;
            this.objectListView1.Name = "objectListView1";
            this.objectListView1.ShowGroups = false;
            this.objectListView1.Size = new System.Drawing.Size(764, 461);
            this.objectListView1.TabIndex = 20;
            this.objectListView1.UseCompatibleStateImageBehavior = false;
            this.objectListView1.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "Salary";
            this.olvColumn3.CellPadding = null;
            this.olvColumn3.Text = "Выдано зарплаты";
            this.olvColumn3.Width = 107;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "Comment";
            this.olvColumn4.CellPadding = null;
            this.olvColumn4.Text = "Комментарий";
            this.olvColumn4.Width = 462;
            // 
            // olvColumn11
            // 
            this.olvColumn11.AspectName = "Date";
            this.olvColumn11.CellPadding = null;
            this.olvColumn11.Text = "Дата";
            this.olvColumn11.Width = 153;
            // 
            // _textBoxTotal2
            // 
            this._textBoxTotal2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this._textBoxTotal2.Location = new System.Drawing.Point(256, 461);
            this._textBoxTotal2.Name = "_textBoxTotal2";
            this._textBoxTotal2.ReadOnly = true;
            this._textBoxTotal2.Size = new System.Drawing.Size(764, 20);
            this._textBoxTotal2.TabIndex = 21;
            this._textBoxTotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this._buttonSearch);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this._dtpTo);
            this.groupBox2.Controls.Add(this._dtpFrom);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 481);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Поиск";
            // 
            // _buttonSearch
            // 
            this._buttonSearch.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSearch.Location = new System.Drawing.Point(64, 122);
            this._buttonSearch.Name = "_buttonSearch";
            this._buttonSearch.Size = new System.Drawing.Size(117, 32);
            this._buttonSearch.TabIndex = 2;
            this._buttonSearch.Text = "Поиск";
            this._buttonSearch.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "по";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "C";
            // 
            // _dtpTo
            // 
            this._dtpTo.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpTo.Location = new System.Drawing.Point(53, 68);
            this._dtpTo.Name = "_dtpTo";
            this._dtpTo.Size = new System.Drawing.Size(153, 20);
            this._dtpTo.TabIndex = 0;
            // 
            // _dtpFrom
            // 
            this._dtpFrom.Cursor = System.Windows.Forms.Cursors.Hand;
            this._dtpFrom.Location = new System.Drawing.Point(53, 24);
            this._dtpFrom.Name = "_dtpFrom";
            this._dtpFrom.Size = new System.Drawing.Size(153, 20);
            this._dtpFrom.TabIndex = 0;
            // 
            // ReportExpenses
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 481);
            this.Controls.Add(this.objectListView1);
            this.Controls.Add(this._textBoxTotal2);
            this.Controls.Add(this.groupBox2);
            this.Name = "ReportExpenses";
            this.Text = "Отчет по расходам";
            ((System.ComponentModel.ISupportInitialize)(this.objectListView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.ObjectListView objectListView1;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn11;
        private System.Windows.Forms.TextBox _textBoxTotal2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button _buttonSearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker _dtpTo;
        private System.Windows.Forms.DateTimePicker _dtpFrom;
    }
}