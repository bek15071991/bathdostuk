﻿using System.Windows.Forms;
using BathDostukDAL.Connection;

namespace BathDostukWinForm
{
    public partial class AppSetting : Form
    {
        private readonly ConnectionManager _manager;

        public AppSetting()
        {
            InitializeComponent();
            _manager = new ConnectionManager();
            Setup();
        }

        private void Setup()
        {
            _textBoxNameDataBase.Text = _manager.DataBase;
            _textBoxSubd.Text = _manager.DataSource;

            _buttonSave.Click += (sender, args) =>
            {
                if (string.IsNullOrEmpty(_textBoxSubd.Text) ||
                    string.IsNullOrEmpty(_textBoxNameDataBase.Text)) return;

                _manager.SetDataBase(_textBoxNameDataBase.Text.Trim());
                _manager.SetDataSource(_textBoxSubd.Text.Trim());
                _manager.SaveChanges();
                MessageBox.Show("Настройка сохранена!!!");
                Application.Restart();
            }  ;
        }
    }
}
