﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;

namespace BathDostukWinForm
{
    public class Printer
    {
        private readonly PrinterSettings _settings;

        public Printer(string printerName)
        {
            _settings = new PrinterSettings();
            _settings.PrintFileName = printerName;
        }


        public void PrintOrders(List<Order> list)
        {
            Print(list.Where(o => o.Ticket.Gender == Gender.Female).ToList(), "Женский отдел.");
            Print(list.Where(o => o.Ticket.Gender == Gender.Male).ToList(), "Мужской отдел.");
            Print(list.Where(o => o.Ticket.Gender == Gender.Family).ToList(), "Семейный отдел.");
        }

        private void Print(ICollection<Order> list, string gender)
        {
            var document = new PrintDocument();
            if (list.Count == 0) return;
            document.PrintPage += (sender, args) =>
            {
                Graphics g = args.Graphics;
                int y = 0;
                g.DrawString(@"Баня ""Достук""", new Font("Times New Roman", 20), new SolidBrush(Color.Black), 45, y);
                y += 30;
                g.DrawString(gender, new Font("Times New Roman", 15), new SolidBrush(Color.Black), 40, y);
                y += 40;
                var count = 1;

                foreach (var order in list)
                {
                    var countSpace = 40 - (order.Ticket.Name.Length + 5 + count.ToString().Length + order.Ticket.Cost.ToString().Length);

                    var line = order.Ticket.Name + new string(' ', countSpace) + order.Ticket.Cost + "сом.";
                    g.DrawString(count + "." + line, new Font("Times New Roman", 12), new SolidBrush(Color.Black), 0, y);
                    y += 20;
                    count++;
                }
                y += 25;

                g.DrawString(DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString(),
                    new Font("Times New Roman", 15), new SolidBrush(Color.Black), 35, y);
            };

            document.PrinterSettings = _settings;
            document.Print();
            document.Dispose();
        }
    }
}
