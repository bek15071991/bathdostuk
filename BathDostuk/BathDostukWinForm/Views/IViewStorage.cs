﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukWinForm.Views
{
   public interface IViewStorage
   {
       void ShowProducts(List<Product> products);

       Product SelectedProduct{ get; set; }

       float Cost { get; set; }

       float Price { get; set; }

       float Count { get; set; }

       List<BuyProduct> BuyProducts { get; set; }

       BuyProduct SelectedBuyProduct { get; set; }

       List<BuyProduct> BuyProductsInStorage { get; set; }

       DateTime From { get; set; }

       DateTime To { get; set; }

       event EventHandler<EventArgs> Add;

       event EventHandler<EventArgs> Delete;

       event EventHandler<EventArgs> Save;

       event EventHandler<EventArgs> Searche;

       event EventHandler<EventArgs> ShowAll;
   }
}
