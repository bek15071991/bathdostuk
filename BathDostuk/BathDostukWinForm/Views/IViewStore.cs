﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukWinForm.Views
{
   public interface IViewStore
    { 

        void ShowBuyProducts(List<BuyProduct> buyProducts);

        BuyProduct SelectedBuyProduct { get; set; }

        BuyProduct InStorageBuyProduct { get; set; }

        float CountInStorage { get;  set; }
        
        float Price { get; set; }

        float Count { get; set; }

        List<SaleProduct> SaleProducts { get; set; }

        SaleProduct SelectedSaleProduct { get; set; }

        List<SaleProduct> SaleProductsInStorage { get; set; }

        DateTime From { get; set; }

        DateTime To { get; set; }

        event EventHandler<EventArgs> Add;

        event EventHandler<EventArgs> Delete;

        event EventHandler<EventArgs> Save;

        event EventHandler<EventArgs> Searche;

        event EventHandler<EventArgs> ShowAll;

       event EventHandler<EventArgs> GetInStorage;
    }
}
