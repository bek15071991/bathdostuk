﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
    public interface IViewReportOnDay
    {  

        List<Order> Orders { get; set; }
        List<SaleProduct> SaleProducts { get; set; }
        List<Expense> Expenses { get; set; }
        List<Expense> Salaries { get; set; }

        List<Employee> Users { get; set; }
        void ShowUsers(List<Employee> users);
         
        double TotalExpense { get; set; } 

        double TotalSalary{ get; set; }

        double TotalBath { get; set; }

        double TotalStore { get; set; }

        double Total { get; set; }

        event EventHandler<EventArgs> Save;
         

        void Close(); 
    }
}
