﻿using System;
using System.Collections.Generic; 
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
  public  interface IViewGeneralReport
    {
        DateTime From { get; }
        DateTime To { get; } 
        void ShowUsers(List<Employee> employees);

       
        Employee SeletcteEmployee { get; set; }

        event EventHandler<EventArgs> Search;

       
        void ShowReport(Employee user, double bath, double storeProfit, double store,     double profit, double? expense, double total);

        void ClearList();
    }
}
