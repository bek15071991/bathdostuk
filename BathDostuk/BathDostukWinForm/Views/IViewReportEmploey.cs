﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
    public interface IViewReportEmploey
    {
        DateTime From { get; }
        DateTime To { get; }
        void ShowUsers(List<Employee> employees);
         
        Employee SeletcteEmployee { get; set; }

        event EventHandler<EventArgs> Search;
         
        void ShowReport(Employee user, double days, double salary, double planSalary, double staySalary);

        void ClearList();
    }
}
