﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
    public interface IViewReportProduct
    {
        DateTime StoreFrom { get; }
        DateTime StoreTo { get; }

        DateTime StorageFrom { get; }
        DateTime StorageTo { get; }


        Employee SelectedStoreEmployee { get;  }
        Employee SelectedStoreageEmployee { get;   }

        void ShowStorage(List<BuyProduct> buyProducts);
        void ShowStore(List<SaleProduct> saleProducts);

        void ShowUsers(List<Employee> employees);

        event EventHandler<EventArgs> SearchStore;

        event EventHandler<EventArgs> SearchStorage;

        void TotalStore(double t, double pribl);
        void TotalStorage(double t);

    }
}
