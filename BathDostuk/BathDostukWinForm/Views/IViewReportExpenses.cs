﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukWinForm.Views
{
   public interface IViewReportExpenses
    {
        DateTime From { get; }
        DateTime To { get; }
       void ShowTotal(double total);
        event EventHandler<EventArgs> Search;
       void ShowExpense(IEnumerable<Expense> list);
    }
}
