﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;

namespace BathDostukWinForm.Views
{
    public interface IViewTicketSetting
    {
        int Cost { get; set; }
        string TicketName { get; set; }

        event EventHandler<EventArgs> Save;
        event EventHandler<EventArgs> Delete;
        Ticket SelectedTicket { get; set; }

        Gender ViewGender { get; set; }

        List<Ticket> Tickets { get; set; }

    }
}
