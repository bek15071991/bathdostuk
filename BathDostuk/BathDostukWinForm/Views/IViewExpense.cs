﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities; 

namespace BathDostukWinForm.Views
{
   public interface IViewExpense
    {
        double Count { get; set; }
        
         

        List<Expense> Expenses { get; set; }

        string Comment { get; set; }

        Expense SelectedExpense { get; set; }

        event EventHandler<EventArgs> Save;
        event EventHandler<EventArgs> Delete;
    }
}
