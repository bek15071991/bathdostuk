﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
    public interface IViewUserSetting
    {
        string Fio { get; set; }
        string Address { get; set; }
        string NumberPhone { get; set; }
        int Salary { get; set; } 
        string Login { get; set; }
        string Password { get; set; }
        Rule Rule { get; set; }
        bool IsDelete { get; set; }
        int CountDays { get; set; }

        List<Employee> Users { get; set; }

        Employee SelectedUser { get; set; }

        event EventHandler<EventArgs> Save;
        event EventHandler<EventArgs> Delete;


    }
}
