﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
    public interface IViewReportSalary
    {
        DateTime From { get; }
        DateTime To { get; }
        void ShowExpenses(List<Expense> orders);
        void ShowUsers(List<Employee> employees);

        void ShowSalaryDay(double salary, double day);
         
        Employee SeletcteEmployee { get; set; }

        event EventHandler<EventArgs> Search;
    }
}
