﻿using System;

namespace NewBath.Views
{
  public  interface IViewLogin
  {
      event EventHandler<EventArgs> LogIn;

      string Login { get;  }
      string Password { get;   } 
      void CloseForm();
  }
}
