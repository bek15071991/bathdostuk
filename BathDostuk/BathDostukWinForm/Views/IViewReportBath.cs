﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
   public interface IViewReportBath
    {
       DateTime From { get;  }
       DateTime To { get;   }
       void ShowOrders(List<Order> orders);
       void ShowUsers(List<Employee> employees);
         

       Employee SeletcteEmployee { get; set; }

       event EventHandler<EventArgs> Search;


    }
}
