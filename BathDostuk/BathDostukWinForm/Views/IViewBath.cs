﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukWinForm.Views
{
   public interface IViewBath
   {
       List<Ticket> Tickets { get; set; }
       Ticket SelectedTicket { get; set; }
       Order SelectedOrder { get; set; }
        

       List<Order> Orders { get; set; }

       List<Order> PaydOrders { get; set; } 

       event EventHandler<EventArgs> Add;
       event EventHandler<EventArgs> Save;
       event EventHandler<EventArgs> Delete; 

    }
}
