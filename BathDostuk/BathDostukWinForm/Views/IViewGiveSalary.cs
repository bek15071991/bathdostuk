﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;

namespace BathDostukWinForm.Views
{
  public  interface IViewGiveSalary
    {
      double Count { get; set; }

      void ShowEmployee(List<Employee> employees);
       

      Employee SelectedEmployee { get; set; }

      List<Expense> Expenses { get; set; }

        string Comment { get; set; }

      Expense SelectedExpense { get; set; }

      event EventHandler<EventArgs> Save;
      event EventHandler<EventArgs> Delete;

    }
}
