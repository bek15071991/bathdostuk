﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukWinForm.Views
{
   public interface IViewProductSetting
    {
       string BarCode { get; set; }
       string NameProduct { get; set; }
       string Ed { get; set; }

       event EventHandler<EventArgs> Save;

       event EventHandler<EventArgs> Delete;

       List<Product> Products { get; set; }

       Product SelectedProduct { get; set; }

    }
}
