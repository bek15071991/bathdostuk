﻿namespace BathDostukWinForm
{
    partial class AppSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._textBoxSubd = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this._buttonSave = new System.Windows.Forms.Button();
            this._textBoxNameDataBase = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _textBoxSubd
            // 
            this._textBoxSubd.Location = new System.Drawing.Point(12, 42);
            this._textBoxSubd.Name = "_textBoxSubd";
            this._textBoxSubd.Size = new System.Drawing.Size(187, 20);
            this._textBoxSubd.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Имя экземпляра СУБД";
            // 
            // _buttonSave
            // 
            this._buttonSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this._buttonSave.Location = new System.Drawing.Point(236, 57);
            this._buttonSave.Name = "_buttonSave";
            this._buttonSave.Size = new System.Drawing.Size(123, 36);
            this._buttonSave.TabIndex = 2;
            this._buttonSave.Text = "Сохранить";
            this._buttonSave.UseVisualStyleBackColor = true;
            // 
            // _textBoxNameDataBase
            // 
            this._textBoxNameDataBase.Location = new System.Drawing.Point(12, 96);
            this._textBoxNameDataBase.Name = "_textBoxNameDataBase";
            this._textBoxNameDataBase.Size = new System.Drawing.Size(187, 20);
            this._textBoxNameDataBase.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя базы данных";
            // 
            // AppSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 133);
            this.Controls.Add(this._buttonSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._textBoxNameDataBase);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._textBoxSubd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AppSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройка подключения";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _textBoxSubd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button _buttonSave;
        private System.Windows.Forms.TextBox _textBoxNameDataBase;
        private System.Windows.Forms.Label label2;
    }
}