﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using BathDostukDAL.Repositories;
using BathDostukWinForm.Forms;
using NewBath.Presenters;
using Application = System.Windows.Forms.Application;
using MessageBox = System.Windows.Forms.MessageBox;

namespace BathDostukWinForm
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var form = new Login();
            new PresenterLogin(form, new EmployeeRepository());

            try
            {
                Application.Run(form);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.InnerException.Message + Environment.NewLine + ex.InnerException.TargetSite, "Ошибка подключения!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "Ошибка системы!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
