﻿using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterReportSalary
    {
        private readonly IViewReportSalary _view;
        private readonly IExpenseRepository _expenseRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRepositoryReportOnDay _repositoryReport;

        public PresenterReportSalary(IViewReportSalary view, IExpenseRepository expenseRepository, IEmployeeRepository employeeRepository, IRepositoryReportOnDay repositoryReport)
        {
            _view = view;
            _employeeRepository = employeeRepository;
            _repositoryReport = repositoryReport;
            _expenseRepository = expenseRepository;
            Init();
        }

        private void Init()
        {
            var users = _employeeRepository.GetAllNotDeleted();

            _view.ShowUsers(users.ToList());
            _view.Search += (sender, args) =>
            {
                var list = new List<Expense>();

                var to = _view.To.Date.AddDays(1);

                if (_view.SeletcteEmployee == null ||
                    _view.SeletcteEmployee.Id == 0)
                {
                    list = _expenseRepository.GetOnPeriodSalary(_view.From, _view.To).ToList();
                    double workedDay = 0;

                    foreach (var employee in users)
                    {
                        workedDay += _repositoryReport.GetWorkedDayOnPeriodWithUser(_view.From, _view.To, employee);
                    }

                    _view.ShowSalaryDay(list.Sum(l => l.Salary), workedDay);
                }
                else
                { list =
                        _expenseRepository.GetOnPeriodWithUserSalary(_view.From, _view.To, _view.SeletcteEmployee).ToList();

                _view.ShowSalaryDay(list.Sum(l => l.Salary), _repositoryReport.GetWorkedDayOnPeriodWithUser(_view.From, _view.To, _view.SeletcteEmployee));}
                _view.ShowExpenses(list);
            };
        }
    }
}
 