﻿using System;
using System.Linq;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterGeneralReport
    {
        private readonly IViewGeneralReport _view;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ISaleProductRepository _saleProductRepository;
        private readonly IRepositoryReportOnDay _reportOnDay;
        private readonly IExpenseRepository _reportSalary;


        public PresenterGeneralReport(IViewGeneralReport view,
            IEmployeeRepository employeeRepository, 
            ISaleProductRepository saleProductRepository,
            IRepositoryReportOnDay reportOnDay,
            IExpenseRepository reportSalary)
        {
            _view = view;
            _employeeRepository = employeeRepository;

            _saleProductRepository = saleProductRepository;
            _reportOnDay = reportOnDay;
            _reportSalary = reportSalary;
            Init();
        }

        private void Init()
        {
            _view.ShowUsers(_employeeRepository.GetAll().ToList());
            _view.Search += (sender, args) =>
            {
                _view.ClearList();
                 
                if (_view.SeletcteEmployee.Id != 0)
                {
                    var reportDays = _reportOnDay.GetOnPeriodWithUser(_view.From, _view.To, _view.SeletcteEmployee).ToList();

                    var bath = reportDays.Sum(rd => rd.TotalBath);

                    var store = reportDays.Sum(rd => rd.TotalStore);

                    var storeProfit = store == 0 ? 0 : _saleProductRepository.GetOnPeriodWithUser(_view.From, _view.To, _view.SeletcteEmployee).Sum(st => st.Profit);

                     
                    var expense = reportDays.Sum(r => r.TotalExpense);
                    var salaries = reportDays.Sum(r => r.TotalSalary);


                    var total = reportDays.Sum(t => t.Total);

                    _view.ShowReport(_view.SeletcteEmployee, bath, storeProfit, store,  bath+storeProfit  , expense + salaries, total);
                }
                else
                {
                    double totalBath = 0,
                        totalStoreProfit = 0,
                        totalStore = 0,
                        totalTotal = 0,
                        totalExpensies =0,
                        totalSalaries = 0;

                    var totalDays = 0;


                    foreach (var user in _employeeRepository.GetAllNotDeleted().ToList())
                    {

                        var reportDays = _reportOnDay.GetOnPeriodWithUser(_view.From, _view.To, user).ToList();

                        var bath = reportDays.Sum(rd => rd.TotalBath);

                        var store = reportDays.Sum(rd => rd.TotalStore);

                        var storeProfit = store == 0 ? 0 : _saleProductRepository.GetOnPeriodWithUser(_view.From, _view.To, user).Sum(st => st.Profit);

                        double salaries = Convert.ToDouble(reportDays.Sum(r => r.TotalSalary));

                        var expense = Convert.ToDouble(reportDays.Sum(r => r.TotalExpense));
                        var total = reportDays.Sum(t => t.Total);


                        totalExpensies += expense;
                        totalSalaries += salaries;
                        totalDays += totalDays;
                        totalBath += bath;
                        totalStoreProfit += storeProfit;
                        totalStore += store;
                        totalTotal += total;
                        _view.ShowReport(user, bath, storeProfit, store, (bath + storeProfit  ), expense + salaries, total);
                    }

                    _view.ShowReport(new Employee { Fio = "Всего :" }, totalBath, totalStoreProfit, totalStore, (totalBath + totalStoreProfit), totalExpensies + totalSalaries, totalTotal);


                }


            };
        }
    }
}
