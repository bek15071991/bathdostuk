﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterBath
    {
        private readonly IViewBath _view;
        private readonly IOrderRepository _orderRepository;
        private readonly ITicketRepository _ticketRepository;

        public PresenterBath(IViewBath view, IOrderRepository orderRepository, ITicketRepository ticketRepository)
        {
            _view = view;
            _orderRepository = orderRepository;
            _ticketRepository = ticketRepository;
            _view.PaydOrders = _orderRepository.GetIsNotReportOrders().ToList();
            _view.Tickets = _ticketRepository.GetNotDeleted().ToList();
            Init();
        }

        private void Init()
        {
            _view.Add += (sender, args) =>
            {
                if (_view.SelectedTicket == null) return;


                if (_view.Orders == null)
                {
                    _view.Orders = new List<Order>
                {
                     new Order
                    { 
                        Ticket = _view.SelectedTicket,
                        Cassier = User.CurrentUser as Cassier,  
                    }
                };
                }
                else
                {
                    var orders = _view.Orders;
                    orders.Add(new Order
                    {
                        Ticket = _view.SelectedTicket,
                        Cassier = User.CurrentUser as Cassier,
                    });
                    _view.Orders = orders;
                }
                _view.SelectedTicket = null;

            };

            _view.Delete += (sender, args) =>
            {
                if (_view.SelectedOrder == null) return;

                var orders = _view.Orders;
                orders.Remove(_view.SelectedOrder);
                _view.Orders = orders;
            };

            _view.Save += (sender, args) =>
            {

                if (!(User.CurrentUser is Cassier))
                {
                    MessageBox.Show("Выдавать билеты могут только кассиры!!", "Ошибка");
                    return;

                }
                if (_view.Orders == null ||
                    _view.Orders.Count == 0 ||
                    MessageBox.Show("Сохранить данные?" + Environment.NewLine + "Итого: " + _view.Orders.Sum(r => r.Ticket.Cost) + " сом.", "Продажа...", MessageBoxButtons.YesNo)
                    != DialogResult.Yes)
                    return;

                using (var transact = new TransactionScope())
                {
                    foreach (var order in _view.Orders)
                    {
                        order.Cassier = (Cassier)User.CurrentUser;
                        _orderRepository.Add(order);
                    }
                    transact.Complete();
                }

                var printer = new Printer("PP6900Enterprise");
                printer.PrintOrders(_view.Orders);
                MessageBox.Show("Билеты сохранены!", "Внимание...");

                _view.Orders = null;
                _view.PaydOrders =
                    _orderRepository.GetIsNotReportOrders().ToList();
            };

        }
    }
}
