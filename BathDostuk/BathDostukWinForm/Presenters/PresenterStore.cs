﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterStore
    {
        private readonly IViewStore _view;
        private readonly ISaleProductRepository _saleProductRepository;
        private readonly IBuyProductRepository _buyProductRepository;

        public PresenterStore(IViewStore view, ISaleProductRepository saleProductRepository, IBuyProductRepository buyProductRepository)
        {
            _view = view;
            _saleProductRepository = saleProductRepository;
            _buyProductRepository = buyProductRepository;
            Init();
        }

        private void Init()
        {
            _view.ShowBuyProducts(_buyProductRepository.GetProductsInStorageAsNoTracking().ToList());
            _view.SaleProductsInStorage = _saleProductRepository.GetProductsWithUserTop100(User.CurrentUser).ToList();


            _view.GetInStorage += (sender, args) =>
            {
                GetBuyProductInStorage();
            };

            _view.Add += (sender, args) =>
            {
                if (_view.SelectedBuyProduct == null ||
                    _view.Price < 1 ||
                    _view.Count < 1
                    ) return;


                if (_view.CountInStorage < _view.Count)
                    return;

                var saleProducts = _view.SaleProducts;

                saleProducts.Add(new SaleProduct
                {
                    BuyProduct = _buyProductRepository.GetById(_view.SelectedBuyProduct.Id),
                    Count = _view.Count,
                    Cost = _view.Price,
                    Employee = User.CurrentUser
                });

                _view.SaleProducts = saleProducts;
                _view.Price =
                    _view.Count = 0;
                _view.SelectedBuyProduct = null;

            };


            _view.Delete += (sender, args) =>
            {
                if (_view.SelectedSaleProduct == null ||
                    MessageBox.Show("Удалить выбранный товар?", "Внимание...", MessageBoxButtons.YesNo) != DialogResult.Yes) return;
                var saleProducts = _view.SaleProducts;
                saleProducts.Remove(_view.SelectedSaleProduct);
                _view.SaleProducts = saleProducts;
                _view.SelectedBuyProduct = null;
            };

            _view.Save += (sender, args) =>
            {
                if (_view.SaleProducts == null ||
                    _view.SaleProducts.Count == 0 ||
                    MessageBox.Show("Сохранить данные?" + Environment.NewLine +
                                    "Итого: " + _view.SaleProducts.Sum(bp => bp.Total) + " сом.",
                        "Внимание...", MessageBoxButtons.YesNo) != DialogResult.Yes) return;

                using (var trans = new TransactionScope())
                {
                    foreach (var saleProduct in _view.SaleProducts)
                    {
                        _saleProductRepository.Add(saleProduct);
                    }
                    trans.Complete();
                }

                _view.SaleProducts = new List<SaleProduct>();
                _view.SaleProductsInStorage = _saleProductRepository.GetProductsWithUserTop100(User.CurrentUser).ToList();
                _view.ShowBuyProducts(_buyProductRepository.GetProductsInStorageAsNoTracking().ToList());
                _view.Price =
                    _view.CountInStorage =
                    _view.Count = 0;
                MessageBox.Show("Данные успешно сохраненны!", "Внимание...");
            };

            _view.Searche += (sender, args) =>
            {
                _view.SaleProductsInStorage =
                    _saleProductRepository.GetOnPeriodWithUser(_view.From, _view.To,User.CurrentUser).
                        OrderByDescending(bp => bp.Date).ToList();
            };

            _view.ShowAll += (sender, args) =>
            {
                _view.SaleProductsInStorage = _saleProductRepository.GetAllWithUser(User.CurrentUser).ToList();
            };
        }

        private void GetBuyProductInStorage()
        {
            if (_view.InStorageBuyProduct == null) return;

            var amountInTable =
                _view.SaleProducts.Where(sp => sp.BuyProduct.Id == _view.InStorageBuyProduct.Id).Sum(s => s.Count);

            _view.CountInStorage =
                (float)(_buyProductRepository.GetProductsInStorageAsNoTracking()
                    .Where(bp => bp.Id == _view.InStorageBuyProduct.Id).Sum(bp => bp.Count) - amountInTable);

            _view.Price = _view.InStorageBuyProduct.Price;
        }
    }
}
