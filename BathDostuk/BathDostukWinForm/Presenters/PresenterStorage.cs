﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterStorage
    {
        private readonly IViewStorage _view;
        private readonly IProductRepository _productRepository;
        private readonly IBuyProductRepository _buyProductRepository;

        public PresenterStorage(IViewStorage view, IProductRepository productRepository, IBuyProductRepository buyProductRepository)
        {
            _view = view;
            _productRepository = productRepository;
            _buyProductRepository = buyProductRepository;
            Init();
        }

        private void Init()
        {
            _view.ShowProducts(_productRepository.GetAll().OrderBy(d=>d.Name).ToList());
            _view.BuyProductsInStorage = _buyProductRepository.GetProductsTop100().ToList();

            _view.Add += (sender, args) =>
            {
                if (_view.SelectedProduct == null ||
                    _view.Cost < 1 ||
                    _view.Price < 1 ||
                    _view.Count < 1 ||
                    (_view.Price < _view.Cost)
                    ) return;

                var buyProducts = _view.BuyProducts;

                buyProducts.Add(new BuyProduct
                {
                    Product = _view.SelectedProduct,
                    Count = _view.Count,
                    Price = _view.Price,
                    Cost = _view.Cost,
                    Employee = User.CurrentUser
                });

                _view.BuyProducts = buyProducts;
                _view.Cost =
                    _view.Price =
                        _view.Count = 0;
                _view.SelectedProduct = null;

            };


            _view.Delete += (sender, args) =>
            {
                if (_view.SelectedBuyProduct == null ||
                    MessageBox.Show("Удалить выбранный товар?", "Внимание...", MessageBoxButtons.YesNo) != DialogResult.Yes) return;
                var buyProducts = _view.BuyProducts;
                buyProducts.Remove(_view.SelectedBuyProduct);
                _view.BuyProducts = buyProducts;
                _view.SelectedBuyProduct = null;
            };

            _view.Save += (sender, args) =>
            {
                if (_view.BuyProducts == null ||
                    _view.BuyProducts.Count == 0 ||
                    MessageBox.Show("Сохранить данные?" + Environment.NewLine +
                                    "Итого: " + _view.BuyProducts.Sum(bp => bp.Total) + " сом.",
                        "Внимание...", MessageBoxButtons.YesNo) != DialogResult.Yes) return;

                using (var trans = new TransactionScope())
                {
                    foreach (var buyProduct in _view.BuyProducts)
                    {
                        _buyProductRepository.Add(buyProduct);
                    }
                    trans.Complete();
                }

                _view.BuyProducts = new List<BuyProduct>();
                _view.BuyProductsInStorage = _buyProductRepository.GetProductsTop100().ToList();
                MessageBox.Show("Данные успешно сохраненны!", "Внимание...");
            };

            _view.Searche += (sender, args) =>
            {
                _view.BuyProductsInStorage =
                    _buyProductRepository.GetAll().Where(bp => bp.Date > _view.From & bp.Date < _view.To).
                    OrderByDescending(bp => bp.Date).Include("Product").ToList();
            };

            _view.ShowAll += (sender, args) =>
            {
                _view.BuyProductsInStorage = _buyProductRepository.GetAll().Include("Product").ToList();
            };
        }
    }
}
