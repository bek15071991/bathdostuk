﻿using System;
using System.Linq;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterReportEmploye
    {
        private readonly IViewReportEmploey _view;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRepositoryReportOnDay _reportOnDay;
        private readonly IExpenseRepository _expenseRepository;

        public PresenterReportEmploye(IViewReportEmploey view,
              IEmployeeRepository employeeRepository,
              IRepositoryReportOnDay reportOnDay, IExpenseRepository expenseRepository)
        {
            _view = view;
            _employeeRepository = employeeRepository;

            _reportOnDay = reportOnDay;
            _expenseRepository = expenseRepository;
            Init();
        }

        private void Init()
        {
            _view.ShowUsers(_employeeRepository.GetAll().ToList());

            _view.Search += (sender, args) =>
            {
                _view.ClearList();


                if (_view.SeletcteEmployee.Id != 0)
                {
                    var reportDays = _reportOnDay.GetOnPeriodWithUser(_view.From, _view.To, _view.SeletcteEmployee).ToList();

                    var salaries = _expenseRepository.GetOnPeriodWithUserSalary(_view.From, _view.To,
                        _view.SeletcteEmployee).Sum(expense => expense.Salary);

                    var planSalary = Math.Round(reportDays.Count * _view.SeletcteEmployee.SalaryForDay, 0);

                    _view.ShowReport(_view.SeletcteEmployee, reportDays.Count, salaries, planSalary, planSalary - salaries);
                }
                else
                {

                    foreach (var user in _employeeRepository.GetAllNotDeleted().ToList())
                    {

                        var reportDays = _reportOnDay.GetOnPeriodWithUser(_view.From, _view.To, user).ToList();

                        var salaries = _expenseRepository.GetOnPeriodWithUserSalary(_view.From, _view.To,
                        user).Sum(expense => expense.Salary);


                        var planSalary = Math.Round(reportDays.Count * user.SalaryForDay, 0);
                        //totalSalaries += salaries;
                        //totalDays += totalDays;
                        //totalBath += bath;
                        //totalStoreProfit += storeProfit;
                        //totalStore += store;
                        //totalTotal += total;

                        _view.ShowReport(user, reportDays.Count, salaries, planSalary, planSalary - salaries);
                    }

                    //_view.ShowReport(new Employee { Fio = "Всего :" }, totalBath, totalStoreProfit, totalStore, (totalBath + totalStoreProfit  ), expenseSum + totalSalaries, totalTotal);


                }


            };
        }
    }
}

