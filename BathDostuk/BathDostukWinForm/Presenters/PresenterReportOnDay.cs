﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Transactions;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Forms;
using BathDostukWinForm.Views;
using ReportOnDay = BathDostukDomain.Entities.ReportOnDay;

namespace BathDostukWinForm.Presenters
{
    public class PresenterReportOnDay
    {
        private readonly IViewReportOnDay _view;
        private readonly ISaleProductRepository _saleProductRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IRepositoryReportOnDay _repositoryReport;
        private readonly IExpenseRepository _expenseRepository;
        private readonly IEmployeeRepository _employeeRepository;

        private List<Employee> _users; 

        public PresenterReportOnDay(IViewReportOnDay view, ISaleProductRepository saleProductRepository, IOrderRepository orderRepository, IRepositoryReportOnDay repositoryReport, IExpenseRepository expenseRepository, IEmployeeRepository employeeRepository)
        {
            _view = view;
            _saleProductRepository = saleProductRepository;
            _orderRepository = orderRepository;
            _repositoryReport = repositoryReport;
            _expenseRepository = expenseRepository;
            _employeeRepository = employeeRepository;
            Init();
        }

        private void Init()
        { 
            var saleProducts = _saleProductRepository.GetIsNotReport().ToList();
            var orders = _orderRepository.GetIsNotReportOrders().ToList();
            var expenses = _expenseRepository.GetIsNotReportExpense().ToList();
            var salaries = _expenseRepository.GetIsNotReportSalaries().ToList();
            _view.TotalBath = orders.Sum(o => o.Ticket.Cost);
            _view.TotalStore = saleProducts.Sum(sp => sp.Total);
            _view.TotalSalary = salaries.Sum(s => s.Salary);
            _view.Expenses = expenses;
            _view.TotalExpense = expenses.Sum(s => s.Salary);
            _view.Total = _view.TotalBath + _view.TotalStore - _view.TotalExpense -_view.TotalSalary;
            _view.SaleProducts = saleProducts;
            _view.Orders = orders;
            _view.Salaries = salaries;
            _users = _employeeRepository.GetEmployesNotDeleted().Where(u => u.Id != User.CurrentUser.Id).ToList();
            _view.ShowUsers(_users);
             
            _view.Users = new List<Employee> { User.CurrentUser };

             

            _view.Save += (sender, args) =>
            {
                if (_view.Users.Count == 0)
                {
                    MessageBox.Show("Пожалуйста укажите работающих сотрудников!!!", "Внимание...", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }

                if (_view.Users.Count == 1)
                {
                    MessageBox.Show("Количество работающих сотрудников очень мало!!!", "Внимание...", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }

                var form = new Security();
                 
                show:
                if (_view.Total < 1 ||
                    form.ShowDialog() != DialogResult.Yes) return; 
                
                if (!User.CurrentUser.Password.Equals(form.Password))
                    goto show;

                form.Close();
                form.Dispose();

                using (var transact = new TransactionScope())
                {
                    foreach (var order in orders)
                    {
                        order.IsReport = true;
                        _orderRepository.Update(order);
                    }

                    foreach (var saleProduct in saleProducts)
                    {
                        saleProduct.IsReport = true;
                        _saleProductRepository.Update(saleProduct);
                    }

                    var expenseSalry = expenses; // Расходы и зарплаты
                    expenseSalry.AddRange(salaries);

                    foreach (var expense in expenseSalry)
                    {
                        expense.IsReport = true; 
                        _expenseRepository.Update(expense);
                    }

                     
                    foreach (var employee in _view.Users)
                    {
                        if(employee.Id!= User.CurrentUser.Id)
                        _repositoryReport.Add(new ReportOnDay
                        {
                            Total = 0,
                            TotalBath = 0,
                            TotalStore = 0,
                            User = employee,
                            TotalExpense = 0,
                            TotalSalary = 0
                        });
                    }

                    var report = new ReportOnDay
                    {
                        Total = _view.Total,
                        TotalBath = _view.TotalBath,
                        TotalStore = _view.TotalStore,
                        User = User.CurrentUser,
                        TotalExpense = _view.TotalExpense,
                        TotalSalary = _view.TotalSalary
                    };

                    _repositoryReport.Add(report);

                    transact.Complete();

                }
                  
                MessageBox.Show("Отчет успешно сохранен!", "внимание..."); 
               
                _view.Close();
            };
        }
    }
}
