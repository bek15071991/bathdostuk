﻿using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterReportProduct
    {
        private readonly IViewReportProduct _view;
        private readonly IBuyProductRepository _buyProductRepository;
        private readonly ISaleProductRepository _saleProductRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public PresenterReportProduct(IViewReportProduct view, IBuyProductRepository buyProductRepository, ISaleProductRepository saleProductRepository, IEmployeeRepository employeeRepository)
        {
            _view = view;
            _buyProductRepository = buyProductRepository;
            _saleProductRepository = saleProductRepository;
            _employeeRepository = employeeRepository;
            Init();
        }

        private void Init()
        {
            _view.ShowUsers(_employeeRepository.GetAll().Where(u=>u.Rule == Rule.Cassier || u.Rule==Rule.Barman).ToList());
            _view.SearchStorage += (sender, args) =>
            {
                var list = new List<BuyProduct>();
                if (_view.SelectedStoreageEmployee == null ||
                    _view.SelectedStoreageEmployee.Id == 0)
                    list = _buyProductRepository.GetOnPeriod(_view.StorageFrom, _view.StorageTo).OrderByDescending(bp => bp.Date).ToList();
                else
                    list = _buyProductRepository.GetOnPeriodWithUser(_view.StorageFrom, _view.StorageTo, _view.SelectedStoreageEmployee).OrderByDescending(bp => bp.Date).ToList();
                
                _view.TotalStorage(list.Sum(bp=>bp.Total));
                _view.ShowStorage(list);

            };

            _view.SearchStore += (sender, args) =>
            { 
                var list = new List<SaleProduct>();
                if (_view.SelectedStoreEmployee == null ||
                    _view.SelectedStoreEmployee.Id == 0)
                    list = _saleProductRepository.GetOnPeriod(_view.StorageFrom, _view.StorageTo).OrderByDescending(bp => bp.Date).ToList();
                else
                    list = _saleProductRepository.GetOnPeriodWithUser(_view.StorageFrom, _view.StorageTo, _view.SelectedStoreageEmployee).OrderByDescending(bp => bp.Date).ToList();
                 
                double total = 0, pribal =0;

                foreach (var saleProduct in list)
                {
                    total += saleProduct.Total;
                    pribal += (saleProduct.BuyProduct.Price - saleProduct.BuyProduct.Cost)*saleProduct.Count;
                }
                 
                _view.TotalStore(total,pribal);
                _view.ShowStore(list);
            };
        }
    }
}
