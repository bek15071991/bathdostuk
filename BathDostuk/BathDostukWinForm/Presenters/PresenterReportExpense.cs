﻿using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterReportExpense
    {
         private readonly IViewReportExpenses _view;
        private readonly IExpenseRepository _expenseRepository;  

        public PresenterReportExpense(IViewReportExpenses view, IExpenseRepository expenseRepository )
        {
            _view = view;  
            _expenseRepository = expenseRepository;
            Init();
        }

        private void Init()
        { 
            _view.Search += (sender, args) =>
            {
                var list = new List<Expense>();

                var to = _view.To.Date.AddDays(1);
                 
                    list = _expenseRepository.GetOnPeriodExpense(_view.From, _view.To).ToList();
                 
                _view.ShowTotal(list.Sum(l => l.Salary));
                _view.ShowExpense(list);
            };
        }
    }
}
