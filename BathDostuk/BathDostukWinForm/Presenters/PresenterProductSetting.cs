﻿using System.Linq;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterProductSetting
    {
        private readonly IViewProductSetting _view;
        private readonly IProductRepository _repository;
        private readonly IBuyProductRepository _buyProduct;

        public PresenterProductSetting(IViewProductSetting view, IProductRepository repository, IBuyProductRepository buyProduct)
        {
            _view = view;
            _repository = repository;
            _buyProduct = buyProduct;
            Init();
        }

        private void Init()
        {
            _view.Products = _repository.GetAll().ToList();

            _view.Save += (sender, args) =>
            {

                if (string.IsNullOrEmpty(_view.Ed) ||
                    string.IsNullOrEmpty(_view.NameProduct) ||
                     _view.BarCode.ToString().Length < 2) return;

                if (_view.Products.Any(p => p.BarCode == _view.BarCode))
                {
                    MessageBox.Show("Такой баркод уже существует!!", "Ошибка...", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }

                _repository.Add(new Product
                {
                    ED = _view.Ed,
                    Name = _view.NameProduct,
                    BarCode = _view.BarCode
                });
                _view.Products = _repository.GetAll().ToList();
                _view.BarCode =
                    _view.Ed =
                        _view.NameProduct = string.Empty;

            };

            _view.Delete += (sender, args) =>
            {
                if (_view.SelectedProduct == null) return;
                
                
                    if (!_buyProduct.CanDeleteProduct(_view.SelectedProduct))
                    {
                        MessageBox.Show("Данный товар связан и не может быть удален!!", "внимание...",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return; 
                    }

                    if (MessageBox.Show("Удалить выбранные данные?", "внимание...", MessageBoxButtons.YesNo) == DialogResult.No) return;
                 
                _repository.Remove(_view.SelectedProduct);
                MessageBox.Show("Успешно удален!", "Внимание....");
                _view.SelectedProduct = null;
                _view.Products = _repository.GetAll().ToList();
            };
        }
    }
}
