﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterTicketSetting
    {
        private readonly IViewTicketSetting _view;
        private readonly ITicketRepository _repository;
        private readonly IOrderRepository _orderRepository;

        public PresenterTicketSetting(ITicketRepository repository, IViewTicketSetting view, IOrderRepository orderRepository)
        {
            _repository = repository;
            _view = view;
            _orderRepository = orderRepository;
            Init();
        }

        private void Init()
        {
            _view.Tickets = _repository.GetAll().ToList();

            _view.Save += (sender, args) =>
            {
                int countGender = Enum.GetValues(typeof (Gender)).Length;

                if (_view.Cost == 0 ||
                    (_view.ViewGender < 0 | _view.ViewGender > (Gender)countGender)||
                    string.IsNullOrEmpty(_view.TicketName))
                    return;

                if (_view.TicketName.Length > 25)
                { MessageBox.Show("Тип билета может быть не более 25 символов!!!", "внимание...", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                _repository.Add(new Ticket
                {
                    Name = _view.TicketName, Cost = _view.Cost ,
                    Gender = _view.ViewGender
                });
                _view.Tickets = _repository.GetAll().ToList();
                ClearValues(); 
            };

            _view.Delete += (sender, args) =>
            {
                if(_view.SelectedTicket==null ||
                    MessageBox.Show("Удалить выбранный билет?", "Внимание...", MessageBoxButtons.YesNo)==DialogResult.No)
                    return;
                
                _view.SelectedTicket.Delete = true;
                _repository.Update(_view.SelectedTicket);
                MessageBox.Show("Успешно удален!!", "Внимание..");
                _view.Tickets = _repository.GetAll().ToList();
                _view.SelectedTicket = null;
             
            };
        }


        private void ClearValues()
        {
            _view.Cost = 0;
            _view.TicketName = string.Empty;
            _view.ViewGender = (Gender)2;
        }
    }
}
