﻿
using System.Configuration;
using System.Windows;
using BathDostukDAL.Repositories;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Forms;
using BathDostukWinForm.Properties;
using NewBath.Views;

namespace NewBath.Presenters
{
    public class PresenterLogin
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IViewLogin _view;

        public PresenterLogin(IViewLogin view, IEmployeeRepository employeeRepository)
        {
            _view = view;
            _employeeRepository = employeeRepository;
            Init();
        }

        private void Init()
        {
             

            _view.LogIn += (sender, args) =>
            {
                if (string.IsNullOrEmpty(_view.Login) ||
                    string.IsNullOrEmpty(_view.Password))
                    return;


                var dd = new BuyProductRepository().GetProductsInStorageAsNoTracking();

                var user = _employeeRepository.GetByLoginPassword(login: _view.Login, password: _view.Password); //_employeeRepository.GetAll().First(); //



                if (user == null ||
                     user.Rule == Rule.Attendant ||
                    user.Rule == Rule.Fireman ||
                    user.Rule == Rule.Other )
                {

                    MessageBox.Show("Логин или пароль введен неправильно!", "Ошибка",MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }



                User.SetUser(user); 

                var form = new Main();
                form.Show();
                _view.CloseForm();

            };
        }
    }
}
