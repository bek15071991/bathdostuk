﻿using System.Collections.Generic; 
using System.Data.Entity;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;
using Rule = BathDostukDomain.Entities.Enum.Rule;

namespace BathDostukWinForm.Presenters
{
    public class PresenterReportBath
    {
        private readonly IViewReportBath _view;
        private readonly IOrderRepository _orderRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public PresenterReportBath(IViewReportBath view, IOrderRepository orderRepository, IEmployeeRepository employeeRepository)
        {
            _view = view;
            _orderRepository = orderRepository;
            _employeeRepository = employeeRepository;
            Init();
        }

        private void Init()
        {
            _view.ShowUsers(_employeeRepository.GetWithRole(Rule.Cassier).ToList());
            _view.Search += (sender, args) =>
            {
                var list = new List<Order>();

                var to = _view.To.Date.AddDays(1);

                if (_view.SeletcteEmployee == null ||
                    _view.SeletcteEmployee.Id == 0)
                { 
                    list = _orderRepository.GetAll()
                            .Where(or => or.Date > _view.From.Date & or.Date < to)
                            .OrderByDescending(or => or.Date).Include("Ticket").Include("Cassier")
                            .ToList();
                }
                else
                    list = _orderRepository.GetAll()
                        .Where(or => or.Date > _view.From.Date & or.Date < to).Where(or => or.Cassier.Id == _view.SeletcteEmployee.Id)
                        .OrderByDescending(or => or.Date).Include("Ticket").Include("Cassier")
                        .ToList();
                 
                _view.ShowOrders(list);
            };
        }


    }
}
