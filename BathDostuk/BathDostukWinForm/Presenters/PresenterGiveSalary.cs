﻿using System.Linq;
using System.Windows;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterGiveSalary
    {
        private readonly IViewGiveSalary _view;
        private readonly IExpenseRepository _expenseRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public PresenterGiveSalary(IViewGiveSalary view, IExpenseRepository expenseRepository, IEmployeeRepository employeeRepository)
        {
            _view = view;
            _expenseRepository = expenseRepository;
            _employeeRepository = employeeRepository;
            Init();
        }

        private void Init()
        {
            _view.ShowEmployee(_employeeRepository.GetAllNotDeleted().ToList());
            _view.Expenses = _expenseRepository.GetIsNotReportSalaries().ToList();

            _view.Delete += (sender, args) =>
            {
                if (_view.SelectedExpense == null ||
                     MessageBox.Show("Удалить выбраный расход?", "Внимание...", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                    return;
                _expenseRepository.Remove(_view.SelectedExpense);
                _view.Expenses = _expenseRepository.GetIsNotReportSalaries().ToList();
                _view.SelectedExpense = null;
            };

            _view.Save += (sender, args) =>
            {
                if (_view.Count < 1 ||
                    _view.SelectedEmployee == null)
                    return;
                _expenseRepository.Add(new Expense
                {
                    Employee = _view.SelectedEmployee,
                    Salary = _view.Count,
                    Comment = _view.Comment,
                    IsReport = false
                });

                _view.Expenses = _expenseRepository.GetIsNotReportSalaries().ToList();
                _view.Count = 0;
                _view.SelectedEmployee = null;
                _view.Comment = string.Empty;
            };
        }
    }
}
