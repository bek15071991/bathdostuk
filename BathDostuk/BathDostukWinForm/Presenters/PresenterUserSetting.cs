﻿using System;
using System.Linq;
using System.Windows.Forms;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
    public class PresenterUserSetting
    {
        private readonly IViewUserSetting _view;
        private readonly IEmployeeRepository _repository;

        public PresenterUserSetting(IViewUserSetting view, IEmployeeRepository repository)
        {
            _view = view;
            _repository = repository;
            Init();
        }

        private void Init()
        {
            _view.Users = _repository.GetAll().ToList();

            _view.Save += (sender, args) =>
            {
                if (string.IsNullOrEmpty(_view.Fio) ||
                    (string.IsNullOrEmpty(_view.Address) |
                    string.IsNullOrEmpty(_view.NumberPhone)) ||
                    _view.Salary == 0 ||
                    _view.CountDays < 1) return;

                if ((string.IsNullOrEmpty(_view.Login) &
                     string.IsNullOrEmpty(_view.Password)) | _view.Rule == Rule.Attendant || _view.Rule == Rule.Fireman ||
                    _view.Rule == Rule.Other)
                {
                    _view.Login = string.Format("l{0}", _view.Fio.GetHashCode());
                    _view.Password = string.Format("p{0}", _view.Fio.GetHashCode());
                }

                if (_view.SelectedUser == null & (_view.Rule < 0 | _view.Rule == (Rule)10))
                    return;



                if (_view.SelectedUser == null)
                {
                    if (_repository.LoginEqual(_view.Login))
                    {
                        MessageBox.Show("Такой логин занят! " + Environment.NewLine + "Введите другой логин", "Информация",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if ((_view.Password.Equals(_view.Login)))
                    {
                        MessageBox.Show("Логин и пароль не могут быть идентичны!", "Информация",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    if (_view.Password.Length < 5 | _view.Password.Length > 20)
                    {
                        MessageBox.Show("Длина пароля может быть больше 5 символов и меньше 20 символов!", "Информация",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if ((_view.Login.Length < 5 | _view.Login.Length > 20))
                    {
                        MessageBox.Show("Длина логина может быть больше 5 символов и меньше 20 символов!", "Информация",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    Employee user;

                    switch (_view.Rule)
                    {
                        case Rule.Cassier:
                            {
                                user = new Cassier();
                                break;
                            }
                        case Rule.Barman:
                            {
                                user = new Barman();
                                break;
                            }
                        default:
                            {
                                user = new Employee();
                                break;
                            }
                    }

                    user.Fio = _view.Fio;
                    user.Address = _view.Address;
                    user.NumberPhone = _view.NumberPhone;
                    user.Salary = _view.Salary;
                    user.Login = _view.Login;
                    user.Password = _view.Password;
                    user.Rule = _view.Rule;
                    user.CountDays = _view.CountDays;

                    _repository.Add(user);
                }
                else
                {

                    _view.SelectedUser.Fio = _view.Fio;
                    _view.SelectedUser.Address = _view.Address;
                    _view.SelectedUser.NumberPhone = _view.NumberPhone;
                    _view.SelectedUser.Salary = _view.Salary;
                    _view.SelectedUser.Login = _view.Login;
                    _view.SelectedUser.Password = _view.Password;
                    _view.SelectedUser.Id = _view.SelectedUser.Id;
                    _view.SelectedUser.Delete = _view.IsDelete;
                    _view.SelectedUser.CountDays = _view.CountDays;
                    _repository.Update(_view.SelectedUser);

                    _view.SelectedUser = null;
                }

                SetDefault();



            };


            _view.Delete += (sender, args) =>
            {
                if (_view.SelectedUser != null & _view.SelectedUser.Id == User.CurrentUser.Id)
                {
                    MessageBox.Show("Под логином данного пользователя был сделан вход в систему! ", "Ошибка...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                       ; return;
                }

                if (_view.SelectedUser == null ||
                    MessageBox.Show("Удалить пользователя?", "Внимание...", MessageBoxButtons.YesNo) != DialogResult.Yes) return;

                var user = _view.SelectedUser;
                user.Delete = true;
                _repository.Update(user);
                _view.SelectedUser = null;
                SetDefault();
                MessageBox.Show("Пользователь успешно удален!", "Внимание...");
            };



        }

        private void SetDefault()
        {
            _view.Fio =
                   _view.Address
                       = _view.NumberPhone
                           = _view.Login
                               = _view.Password = string.Empty;
            _view.Salary = 0;

            _view.Rule = (Rule)10;
            _view.Users = _repository.GetAll().ToList();
            _view.CountDays = 0;
        }
    }
}
