﻿using System.Linq;
using System.Windows;
using BathDostukDomain.Entities;
using BathDostukDomain.Interfaces.Repositories;
using BathDostukWinForm.Views;

namespace BathDostukWinForm.Presenters
{
   public class PresenterExpense
    {
         private readonly IViewExpense _view;
        private readonly IExpenseRepository _expenseRepository;
        private readonly IEmployeeRepository _employeeRepository;

        public PresenterExpense(IViewExpense view, IExpenseRepository expenseRepository, IEmployeeRepository employeeRepository)
        {
            _view = view;
            _expenseRepository = expenseRepository;
            _employeeRepository = employeeRepository;
            Init();
        }

        private void Init()
        { 
            _view.Expenses = _expenseRepository.GetIsNotReportExpense().ToList();

            _view.Delete += (sender, args) =>
            {
                if (_view.SelectedExpense == null ||
                     MessageBox.Show("Удалить выбраный расход?", "Внимание...", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                    return;
                _expenseRepository.Remove(_view.SelectedExpense);
                _view.Expenses = _expenseRepository.GetIsNotReportExpense().ToList();

                _view.SelectedExpense = null;
            };

            _view.Save += (sender, args) =>
            {
                if (_view.Count < 1 ||
                    string.IsNullOrEmpty(_view.Comment))
                    return;
                _expenseRepository.Add(new Expense
                { 
                    Salary = _view.Count,
                    Comment = _view.Comment,
                    IsReport = false
                });

                _view.Expenses = _expenseRepository.GetIsNotReportExpense().ToList();
                _view.Count = 0;
                _view.Comment = string.Empty;
            };
        }
    }
}
