﻿using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;


namespace BathDostukDomain.Interfaces.Repositories
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
        IEnumerable<Order> GetIsNotReportOrders();

        IEnumerable<Order> GetOrdersOnPeriodWithUser(DateTime from, DateTime to, Employee user);
         
    }
}
