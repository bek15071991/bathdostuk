﻿using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukDomain.Interfaces.Repositories
{
    public interface IProductRepository : IRepositoryBase<Product>
    {
        
    }
}
