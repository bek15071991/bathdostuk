﻿using System;
using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Interfaces.Repositories
{
    public interface ISaleProductRepository : IRepositoryBase<SaleProduct>
    {
        IEnumerable<SaleProduct> GetProductsTop100();

        IEnumerable<SaleProduct> GetOnPeriod(DateTime from, DateTime to);

        IEnumerable<SaleProduct> GetOnPeriodWithUser(DateTime from, DateTime to, Employee user);

        IEnumerable<SaleProduct> GetIsNotReport();
          
        IEnumerable<SaleProduct> GetProductsWithUserTop100(Employee employee);

        IEnumerable<SaleProduct> GetAllWithUser(Employee employee);
    }
}
