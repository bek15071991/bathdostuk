﻿using System.Collections.Generic;
using BathDostukDomain.Entities.Enum;
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Interfaces.Repositories
{
    public interface IEmployeeRepository : IRepositoryBase<Employee>
    {
        Employee GetByLoginPassword(string login, string password);

        IEnumerable<Employee> GetAllNotDeleted();

        bool LoginEqual(string p);
        IEnumerable<Employee> GetWithRole(Rule cassier);

        IEnumerable<Employee> GetEmployesNotDeleted();
    }
}
