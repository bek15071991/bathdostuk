﻿using System.Collections.Generic;
using System.Linq;
using BathDostukDomain.Entities;

namespace BathDostukDomain.Interfaces.Repositories
{
    public interface IExpenseRepository : IRepositoryBase<Expense>
    {
        IQueryable<Expense> GetAllExpense();
        IQueryable<Expense> GetAllSalary();
         
         List<Expense> GetOnPeriodSalary(System.DateTime dateTime1, System.DateTime dateTime2);

        List<Expense> GetOnPeriodWithUserSalary(System.DateTime dateTime1, System.DateTime dateTime2, Entities.People.Employee employee);

        List<Expense> GetOnPeriodExpense(System.DateTime dateTime1, System.DateTime dateTime2);

        IEnumerable<Expense> GetIsNotReportExpense();
        IEnumerable<Expense> GetIsNotReportSalaries();
    }
}
