﻿using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukDomain.Interfaces.Repositories
{
    public interface IRepositoryReportOnDay : IRepositoryBase<ReportOnDay>
    { 
        int GetWorkedDayOnPeriodWithUser(System.DateTime dateTime1, System.DateTime dateTime2, Entities.People.Employee employee);

        IEnumerable<ReportOnDay> GetOnPeriodWithUser(System.DateTime dateTime1, System.DateTime dateTime2, Entities.People.Employee employee);
    }
}
