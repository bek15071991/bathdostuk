﻿using System.Collections.Generic;
using BathDostukDomain.Entities;

namespace BathDostukDomain.Interfaces.Repositories
{
    public interface ITicketRepository : IRepositoryBase<Ticket>
    {
        IEnumerable<Ticket> GetNotDeleted();
    }
}
