﻿ 
using System;
using System.Collections.Generic;
using BathDostukDomain.Entities;
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Interfaces.Repositories
{

    public interface IBuyProductRepository : IRepositoryBase<BuyProduct>
    {
        IEnumerable<BuyProduct> GetProductsInStorageAsNoTracking();

        IEnumerable<BuyProduct> GetProductsTop100();

        IEnumerable<BuyProduct> GetOnPeriod(DateTime from, DateTime to);

        IEnumerable<BuyProduct> GetOnPeriodWithUser(DateTime from, DateTime to, Employee user);

        bool CanDeleteProduct(Product selectedProduct);
    }
}
