﻿ 
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Entities
{
    public class User
    {
        private static Employee _currentUser;

        public static void SetUser(Employee person)
        {  
            _currentUser = person;
        }


        public static Employee CurrentUser { get { return _currentUser; } }
    }
}
