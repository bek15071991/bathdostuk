﻿using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Entities
{
    public class ExpenseSalary : Expense
    {
        public Employee Employee { get; set; }
    }
}
