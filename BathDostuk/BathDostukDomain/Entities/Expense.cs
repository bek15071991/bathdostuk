﻿using System;
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Entities
{
   public class Expense
    {
       public int Id { get; set; }
       public double Salary { get; set; }
       public string Comment { get; set; }
       public DateTime Date { get; set; }
       public bool IsReport { get; set; }
       public Employee Employee { get; set; }
    }
}
