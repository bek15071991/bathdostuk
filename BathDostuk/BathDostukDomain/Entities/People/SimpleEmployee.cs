﻿using System.Collections.Generic;

namespace BathDostukDomain.Entities.People
{
    public class SimpleEmployee : Employee
    {
        public virtual ICollection<BuyProduct> BuyProducts { get; set; } 
        public virtual ICollection<SaleProduct> SaleProducts { get; set; }

        public SimpleEmployee()
        {
            SaleProducts = new HashSet<SaleProduct>();
            BuyProducts = new HashSet<BuyProduct>(); 
        }
         
  
    }
}
