﻿using System;
using BathDostukDomain.Entities.Enum;

namespace BathDostukDomain.Entities.People
{
    public class Employee
    {
        public int Id { get; set; }
        public string Fio { get; set; }
        public string Address { get; set; }
        public string NumberPhone { get; set; }
        public float Salary { get; set; }
        public int CountDays { get; set; }
        public byte[] Photo { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool Delete { get; set; }
        public Rule Rule { get; set; }

        public double
            SalaryForDay
        {
            get { return (Salary == 0 || CountDays == 0) ? 0 : Convert.ToDouble(Salary / CountDays); }
        }
    }
}
