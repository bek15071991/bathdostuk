﻿using BathDostukDomain.Entities.Enum;

namespace BathDostukDomain.Entities.People
{
    public class Barman : SimpleEmployee
    {
        public Barman()
        {
            Rule = Rule.Barman;
        }

    }
}
