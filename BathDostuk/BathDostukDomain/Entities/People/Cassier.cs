﻿
using System.Collections.Generic;
using BathDostukDomain.Entities.Enum;

namespace BathDostukDomain.Entities.People
{
    public class Cassier : SimpleEmployee
    {
        public virtual ICollection<Order> Orders { get; set; }

        public Cassier()
        { 
            Orders = new HashSet<Order>();
            Rule = Rule.Cassier;
        }


        /// <summary>
        /// Продажа билетов
        /// </summary>
        /// <param name="tickets"></param>
        public void SaleTickets(IEnumerable<Order> tickets)
        {
            foreach (var ticket in tickets)
            {
                Orders.Add(ticket);
            }
        }
       
    }
}
