﻿using System;
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Entities
{
    public abstract class BuySaleAbastract
    {
        public int Id { get; set; }
        public float Cost { get; set; }
        public double Count { get; set; }
        public DateTime Date { get; set; }
        public Employee Employee { get; set; }
        public bool IsReport { get; set; }

        public   float Total
        {
            get { return (float)Count * Cost; }
        }
      

    }
}
