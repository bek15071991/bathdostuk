﻿

using BathDostukDomain.Entities.Enum;

namespace BathDostukDomain.Entities
{
    public class Ticket
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public float Cost { get; set; }
        public Gender Gender { get; set; }
        public bool Delete { get; set; }
    }

    
}
