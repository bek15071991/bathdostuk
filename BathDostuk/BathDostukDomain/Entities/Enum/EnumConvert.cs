namespace BathDostukDomain.Entities.Enum
{
    public static  class EnumConvert
    {
        public static string ToString(this System.Enum code)
        {
            if (code is Gender)
            {
                var gender = (Gender)code;
                if (gender == Gender.Male)
                    return "�������";
                if (gender == Gender.Female)
                    return "�������";
                if (gender == Gender.Family)
                    return "��������";
            }

            if (code is Rule)
            {
                var gender = (Rule)code;
                if (gender == Rule.Admin)
                    return "�������������";
                if (gender == Rule.Attendant)
                    return "�������";
                if (gender == Rule.Barman)
                    return "������";
                if (gender == Rule.Director)
                    return "��������";
                if (gender == Rule.Fireman)
                    return "�������";
                if (gender == Rule.Other)
                    return "������";
                if (gender == Rule.Cassier)
                    return "������";

            }

            return string.Empty;
        }
    }
}