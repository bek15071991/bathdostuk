﻿

namespace BathDostukDomain.Entities.Enum
{
    public enum Rule : int
    {
        Admin,
        Director,
        Cassier,
        Barman,
        Attendant,
        Fireman,
        Other
    }
}
