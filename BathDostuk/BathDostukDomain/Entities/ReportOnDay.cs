﻿using System;
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Entities
{
   public class ReportOnDay
    {
       public int Id { get; set; }
       public Employee User { get; set; }
       public double Total { get; set; }
       public double TotalBath { get; set; }
       public double TotalStore { get; set; }
       public double TotalExpense { get; set; }
       public double TotalSalary { get; set; }
       public DateTime Date { get; set; }

    }
}
