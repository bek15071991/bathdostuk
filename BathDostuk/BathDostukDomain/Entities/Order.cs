﻿using System;
using BathDostukDomain.Entities.People;

namespace BathDostukDomain.Entities
{

    public class Order
    {
        public int Id { get; set; }
        public Ticket Ticket { get; set; } 
        public DateTime Date { get; set; }

        public string DateTostring {
            get { return Date.ToString("dd/MM/yyyy HH:mm:ss"); }
        }

        public virtual Cassier Cassier { get; set; }
        public bool  IsReport { get; set; }

    }
}
