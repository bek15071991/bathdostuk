﻿ 

namespace BathDostukDomain.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BarCode { get; set; }
        public string ED { get; set; } 
    }
}
