﻿namespace BathDostukDomain.Entities
{
    public class SaleProduct : BuySaleAbastract
    {
        public virtual BuyProduct BuyProduct { get; set; }

        public double Profit
        {
            get
            {
                return (BuyProduct.Price - BuyProduct.Cost) *  Count;
            }
        }
         
    }
}
